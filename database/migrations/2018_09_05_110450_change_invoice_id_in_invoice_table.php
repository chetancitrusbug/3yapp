<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeInvoiceIdInInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('ALTER TABLE `invoice` CHANGE `invoice_id` `invoice_id` VARCHAR(191) NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice', function (Blueprint $table) {
            \DB::statement('ALTER TABLE `invoice` CHANGE `invoice_id` `invoice_id` VARCHAR(191) NOT NULL');
        });
    }
}
