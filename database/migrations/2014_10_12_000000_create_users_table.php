<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('user_type',['user','guest']);
            $table->enum('lgn_type',['simple','google','facebook']);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('contact_no');
            $table->integer('country');
            $table->string('company_name')->nullable();
            $table->string('profile_pic')->nullable();
            $table->string('company_tax_no')->nullable();
            $table->string('company_vat_no')->nullable();
            $table->text('insurance_certificates')->nullable();
            $table->string('website')->nullable();
            $table->tinyInteger('is_trial')->default(0)->nullable();
            $table->date('birth_date');
            $table->text('lgn_id')->nullable();
            $table->string('stripe_id')->nullable();
            $table->string('stripe_token_id')->nullable();
            $table->string('api_token')->nullable();
            $table->string('activation_token')->nullable();
            $table->integer('vat')->nullable();
            $table->rememberToken();
            $table->string('referral_code')->nullable();
            $table->text('bio')->nullable();
            $table->tinyInteger('status');
            $table->timestamp('expire_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
