<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsEligibleInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('is_eligible')->after('status');
            $table->text('address')->after('status')->nullable();
            $table->text('address_line1')->after('status')->nullable();
            $table->text('address_line2')->after('status')->nullable();
            $table->text('city')->after('status')->nullable();
            $table->text('zipcode')->after('status')->nullable();
            $table->varchar('company_logo')->after('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_eligible');
            $table->dropColumn('address');
            $table->dropColumn('company_logo');
        });
    }
}
