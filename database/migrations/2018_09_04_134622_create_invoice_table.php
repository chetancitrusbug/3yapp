<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id')->unique();
            $table->integer('user_id');
            $table->integer('client_id');
            $table->string('subject');
            $table->text('description');
            $table->float('net_amount');
            $table->float('vat');
            $table->float('total_amount');
            $table->string('invoice_image')->nullable()->default(null);
            $table->dateTime('date_time');
            $table->date('due_date');
            $table->enum('status',['in_progress','confirm']);
            $table->enum('is_invoice',['0','1'])->default(0);
            $table->enum('payment_mode',['0','1']);
            $table->enum('receipt_payment_mode',['cash','card','bank-transfer']);
			$table->integer('payment_terms_id');
            $table->enum('invoice_status',['Draft','Sent','Cancel','Paid'])->default('Draft');
            $table->integer('payment_instruction_id')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice');
    }
}
