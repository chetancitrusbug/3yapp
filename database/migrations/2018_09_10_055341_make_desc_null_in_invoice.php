<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeDescNullInInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `invoice` CHANGE `description` `description` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL");
        \DB::statement("ALTER TABLE `mileages` ADD COLUMN `is_perpose` ENUM('commercial','personel') NULL AFTER `is_start`");
        \DB::statement("ALTER TABLE `mileages` ADD COLUMN `total_distance` FLOAT NULL AFTER `is_perpose`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `invoice` DROP COLUMN `is_perpose`");
        \DB::statement("ALTER TABLE `mileages` DROP COLUMN `is_perpose`");
        \DB::statement("ALTER TABLE `mileages` DROP COLUMN `total_distance`");
    }
}
