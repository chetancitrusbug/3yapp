<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMileagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mileages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_title');
            $table->string('client_name');
            $table->integer('client_id');
            $table->integer('user_id');
            $table->text('purspose');
            $table->date('date');
            $table->time('time');
            $table->string('start_point_lat');
            $table->string('start_point_long');
            $table->string('dest_point_lat');
            $table->string('dest_point_long');
            $table->string('vehical_type');
            $table->string('ret_per_mile');
            $table->string('start_point_name')->nullable()->default(null);
            $table->string('end_point_name')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mileages');
    }
}
