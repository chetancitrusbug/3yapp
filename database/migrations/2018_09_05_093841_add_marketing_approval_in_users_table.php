<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMarketingApprovalInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('marketing_approval')->after('referral_code');
            $table->tinyInteger('address_line1')->after('referral_code');
            $table->tinyInteger('address_line2')->after('referral_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('marketing_approval');
            $table->dropColumn('address_line1');
            $table->dropColumn('address_line2');
        });
    }
}
