<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\HasRoles;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;
use Auth;
use Carbon\Carbon;



class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_type','lgn_type','first_name','last_name', 'email', 'password', 'contact_no','activation_token','status', 'lgn_type','country','company_name','profile_pic','company_tax_no','company_vat_no','insurance_certificates','website','is_trial','birth_date','lgn_id','stripe_id','stripe_token_id','expire_at','bio','vat','address','company_logo','address_line1','address_line2','city','zipcode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    // protected $dates = ['deleted_at'];

    public function getBirthDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
        return $value;
    }
	
	public function role()
    {
        return $this->belongsToMany('App\Role','role_user','user_id','role_id');
    }
}
