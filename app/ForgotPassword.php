<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForgotPassword extends Model
{
    protected $guarded = ['id'];
    protected $table = 'password_resets';

    public function user() {
        return $this->belongsTo('App\User');
    }
}
