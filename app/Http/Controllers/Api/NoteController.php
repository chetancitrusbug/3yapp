<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Note;
use App\User;

class NoteController extends Controller
{
    //
    public function list(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        }
        else
        {
            $user = User::where('id', request()->user_id)->where('api_token', request()->api_token)->first();

            if (!$user) {
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result' => $data, 'status' => $status, 'message' => $message, 'code' => $code]);
            }
            $notes=array();
            if (isset($request->client_id)) 
            {
                if($request->client_id != '')
                {
                    $notes= Note::where('user_id',$request->user_id)->where('client_id',$request->client_id)->get();
                }
                else
                {
                    $notes = Note::where('user_id', $request->user_id)->where('client_id', 0)->get();
                }
            }
            else
            {
                $notes = Note::where('user_id', $request->user_id)->where('client_id', 0)->get();
            }

            if (count($notes) <= 0) {
                $code = 200;
                $messages = 'Data not found';
                $status = true;
            }
            else
            {
                $data=$notes;
            }
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;

    }
    public function store(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required',
        );
       
            $rules['title'] = 'required';
            $rules['description'] = 'required';
      

        $validator = \Validator::make($request->all(), $rules, []);
      
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        }
        else
        {
            $user = User::where('id', request()->user_id)->where('api_token', request()->api_token)->first();

            if (!$user) {
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result' => $data, 'status' => $status, 'message' => $message, 'code' => $code]);
            }
            if(isset($request->client_id))
            {
                if($request->client_id != '')
                {
                    $note = new Note();
                    $note->user_id = $request->user_id;
                    $note->client_id = $request->client_id;
                    $note->title = $request->title;
                    $note->description = $request->description;
                    $note->save();
                    $data=$note;
                }
                else
                {
                    $note = new Note();
                    $note->user_id = $request->user_id;
                    $note->client_id = 0;
                    $note->title = $request->title;
                    $note->description = $request->description;
                    $note->save();
                    $data = $note;
                }
               
            }
            else
            {
                $note=new Note();
                $note->user_id=$request->user_id;
                $note->client_id=0;
                $note->title=$request->title;
                $note->description=$request->description;
                $note->save();
                $data = $note;

            }
        }

        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }
    public function edit(Request $request)
    {
       
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required',
            'note_id' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } 
        else {
            $user = User::where('id', request()->user_id)->where('api_token', request()->api_token)->first();

            if (!$user) {
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result' => $data, 'status' => $status, 'message' => $message, 'code' => $code]);
            }
            $note=Note::find($request->note_id);
            if($note == null)
            {
               
                    $code = 200;
                    $messages = 'Data not found';
                    $status = true;
              
            }
            else
            {
                $data[] = $note;
            } 
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;

    }
    public function update(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required',
            'note_id' => 'required',
            'title'=> 'required',
            'description'=> 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::where('id', request()->user_id)->where('api_token', request()->api_token)->first();

            if (!$user) {
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result' => $data, 'status' => $status, 'message' => $message, 'code' => $code]);
            }
            $note = Note::find($request->note_id);
            if ($note == null) {

                $code = 200;
                $messages = 'Data not found';
                $status = true;
            } else {
                $note->title=$request->title;
                $note->description=$request->description;
                $note->save();
            }
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit; 
    }
    public function delete(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required',
            'note_id' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } 
        else
        {
            $note = Note::find($request->note_id);
            if ($note == null) {

                $code = 200;
                $messages = 'Data not found';
                $status = true;
            }
            else
            {
                $note->delete();
            }
        }

        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit; 
    }
}
