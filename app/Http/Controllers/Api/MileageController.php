<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Mileage;
use App\User;

class MileageController extends Controller
{
    private $key = 'AIzaSyBS1S2BSDnzObBL8xel9k7QM10fJFQvdDw';
    public function store(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Mileage added Successfully';
        $status = true;
        $error = '';
        $user_id = $request->user_id;
        $rules = array(
            'user_id' => 'required',
            'job_title' => 'required',
            //'client_name' => 'required',
            //'client_id' => 'required',
            'purspose' => 'required',
            'date' => 'required',
            'time' => 'required',
            'start_point_lat' => 'required',
            'start_point_long' => 'required',
            'vehical_type' => 'required',
            'ret_per_mile' => 'required',
            'start_point_name' => 'nullable',
			
        );

        $validator = \Validator::make($request->all(), $rules, ['job_title.required' => 'Mileage Title is required.','purspose.required' => 'The purpose field is required.']);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
			if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }elseif($request->is_start){
				$mileages = Mileage::where('user_id',request()->user_id)->where('is_start','1')->get()->count();
				if($mileages > 0){
					$message = 'Mileage can not be added, because already one is running!';
					$code = 400;
					$status = false;
					return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
				}
			}

            $mileage = new Mileage();
            $mileage->user_id = $request->user_id;
            $mileage->job_title = $request->job_title;
            $mileage->client_name = (isset($request->client_name)) ? $request->client_name: '' ;
            $mileage->client_id =(isset($request->client_id)) ?  $request->client_id : '';
            $mileage->user_id = $request->user_id;
            $mileage->purspose = $request->purspose;
            $mileage->date = $request->date;
            $mileage->time = $request->time;
            $mileage->start_point_lat = $request->start_point_lat;
            $mileage->start_point_long = $request->start_point_long;
            $mileage->vehical_type = $request->vehical_type;
            $mileage->ret_per_mile = $request->ret_per_mile;
            $mileage->is_start = $request->is_start;
            $mileage->is_perpose = $request->is_perpose;
            if(request()->has('start_point_name')  && !empty($request->start_point_name)  ){
                $mileage->start_point_name = $request->start_point_name;
            } else {
                $mileage->start_point_name = $this->GetAddressByGoogleApi($request->start_point_lat,$request->start_point_long);
            }
            $mileage->save();
            $mileage->mileage_id = $mileage->id; // '#'.str_pad($paymentInstruction->id, 10, "0", STR_PAD_LEFT);
            $data = $mileage;
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;

    }
	
	public function vehicalRate(Request $request){
		$data = array();
        $code = 200;
        $messages = 'Mileage added Successfully';
        $status = true;
        $error = '';
        $data = \App\RateVehical::all();
		return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
	}
    public function update(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Mileage updated Successfully';
        $status = true;
        $error = '';

        $rules = array(
            'mileage_id' => 'required',
            'user_id' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            $mileage = Mileage::where('id',$request->mileage_id)->where('user_id',$request->user_id)->first();
            if(!$mileage){
                $message = 'Mileage not found';
                $code = 200;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $mileage->job_title = ($request->job_title)?$request->job_title:$mileage->job_title;
            $mileage->client_name = ($request->client_name)?$request->client_name:$mileage->client_name;
            $mileage->client_id = ($request->client_id)?$request->client_id:$mileage->client_id;
            $mileage->purspose = ($request->purspose)?$request->purspose:$mileage->purspose;
            $mileage->date = ($request->date)?$request->date:$mileage->date;
            $mileage->time = ($request->time)?$request->time:$mileage->time;
            $mileage->start_point_lat = ($request->start_point_lat)?$request->start_point_lat:$mileage->start_point_lat;
            $mileage->start_point_long = ($request->start_point_long)?$request->start_point_long:$mileage->start_point_long;
            $mileage->dest_point_lat = ($request->dest_point_lat)?$request->dest_point_lat:$mileage->dest_point_lat;
            $mileage->dest_point_long = ($request->dest_point_long)?$request->dest_point_long:$mileage->dest_point_long;
            $mileage->vehical_type = ($request->vehical_type)?$request->vehical_type:$mileage->vehical_type;
            $mileage->ret_per_mile = ($request->ret_per_mile)?$request->ret_per_mile:$mileage->ret_per_mile;
            $mileage->start_point_name = ($request->start_point_name)?$request->start_point_name:$mileage->start_point_name;
            $mileage->end_point_name = ($request->end_point_name)?$request->end_point_name:$mileage->end_point_name;
			$mileage->is_start = $request->is_start;
			$mileage->total_distance = $request->total_distance;
            
            $mileage->save();

            $mileage->mileage_id = $mileage->id;
            $data = $mileage;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;

    }
    public function list(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required'
        );

        if(request()->has('start_date') || request()->has('end_date')){
            $rules['start_date'] = 'required';
            $rules['end_date'] = 'required';
        }

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

            if(!$user){
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $mileages = Mileage::where('user_id',request()->user_id);

            if(isset($request->client_id) && $request->client_id != ''){
                $mileages->whereIn('client_id',explode(',',request()->client_id));
            }

            if(request()->has('start_date') && request()->has('end_date')){
                $mileages->whereDate('date','>=',request()->start_date);
                $mileages->whereDate('date','<=',request()->end_date);
            }
            $mileages = $mileages->select('*',\DB::raw(' round((total_distance * ret_per_mile),2) as total_price'))->orderby('is_start','desc')->orderby('updated_at','desc')->get();

            if($mileages->count() <= 0){
                $code = 200;
                $messages = 'Mileage not found';
                $status = false;
            }
            $data = $mileages;
        }

        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;

    }
    public function delete(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Mileage Deleted Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'mileage_id' =>'required',
            'api_token' =>'required'
        );


        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {

            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $mileage = Mileage::where('id',$request->mileage_id)->where('user_id',$request->user_id)->first();
            if(empty($mileage)){
                $message = 'Mileage not found';
                $code = 200;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
                exit;
            }
            $mileage->delete();
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }
    public function GetAddressByGoogleApi($lat,$long)
    {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?key=".$this->key."&latlng=$lat,$lon&sensor=false";

        // Make the HTTP request
        $data = @file_get_contents($url);
        // Parse the json response
        $jsondata = json_decode($data,true);
        if($jsondata['status'] == 'REQUEST_DENIED'){
		//	var_dump($jsondata);
				return "Undefined Address Title";
		} else {
			return $jsondata['results'][0]['formatted_address'];
		}
        // If the json data is invalid, return empty array
       /* if (!check_status($jsondata))   return array();

        $address = array(
            'country' => google_getCountry($jsondata),
            'province' => google_getProvince($jsondata),
            'city' => google_getCity($jsondata),
            'street' => google_getStreet($jsondata),
            'postal_code' => google_getPostalCode($jsondata),
            'country_code' => google_getCountryCode($jsondata),
            'formatted_address' => google_getAddress($jsondata),
        );
        return $address; */
    }
}
