<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\User;
use App\ExpenseCategory;


class ExpenseCategoryController extends Controller
{

    public function index(Request $request)
    {

    }


    public function create(Request $request)
    {

    }


    public function store(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Expense Category added Successfully';
        $status = true;
        $error = '';
        $user_id = $request->user_id;
        $rules = array(
            'user_id' => 'required',
            'title' => [
                        'required',
                        Rule::unique('expense_categories')->where(function ($query) use ($user_id) {
                            return $query->where('user_id', $user_id);
                        }),
                        ]
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $category = new ExpenseCategory();
            $category->user_id = $request->user_id;
            $category->title = $request->title;
            $category->status = 'Active';

            $category->save();

            $category->category_id = $category->id; // '#'.str_pad($paymentInstruction->id, 10, "0", STR_PAD_LEFT);
            $data = $category;
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }


    public function list(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {

            $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

            if(!$user){
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
            if($user->id == 1){
                $categories = ExpenseCategory::all();
            } else {
                $categories = ExpenseCategory::where('user_id',request()->user_id)->orWhere('user_id',1)->orderby('title','asc');
                $categories = $categories->get();
            }

            if($categories->count() <= 0){
                $code = 200;
                $messages = 'Category not Found';
                $status = true;
            }
            $data = $categories;
        }

        return response()->json( ['code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }


    public function edit(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = true;
        $error = '';

        $rules = array(
            'user_id' => 'required',
            'expensecategory_id' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $category = ExpenseCategory::where('id',$request->expensecategory_id)->where('user_id',$request->user_id)->first();

            if(empty($category)){
                $code = 404;
                $messages = 'Expense Category not exist';
                $status = false;
            }
            $data= $category;
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }


    public function update(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Expense Category Updated Successfully';
        $status = true;
        $error = '';
        $user_id = $request->user_id;

        $rules = array(
            'user_id' => 'required',
            'status'=>'in:Active,Deactivate',
            'expensecategory_id'=> 'required',
            'title' => [ 'required',
                    Rule::unique('expense_categories')->ignore($request->expensecategory_id)->where(function ($query) use ($user_id) {
                            return $query->where('user_id', $user_id);
                    }),
            ]
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $category = ExpenseCategory::where('id',$request->expensecategory_id)->where('user_id',$request->user_id)->first();
            if(empty($category)){
                $message = 'Expense Category not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
            $data['title'] = ($request->title)?$request->title:$category->title;
            $data['status'] = ($request->status)?$request->status:$category->status;


            $category->update($data);

            $category->category_id =  $category->id;//'#'.str_pad($paymentInstruction->id, 10, "0", STR_PAD_LEFT);
            $data = $category;
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }


    public function delete(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Expense Category Deleted Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'expensecategory_id' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $category = ExpenseCategory::where('id',$request->expensecategory_id)->where('user_id',$request->user_id)->first();
            if(empty($category)){
                $message = 'Expense Category not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
                exit;
            }
            $category->delete();
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }
}
