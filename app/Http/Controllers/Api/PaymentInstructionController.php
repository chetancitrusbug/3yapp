<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Payment_Instraction;

class PaymentInstructionController extends Controller
{
    public function store(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Payment Instruction added Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'bank_name' => 'required',
            'account_number' => 'required',
            'ifsc_code' => 'required',
			'save_for_future' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
			$paymentInstructions = Payment_Instraction::where('user_id',request()->user_id)->where('account_number',$request->account_number);
			$paymentInstruction = $paymentInstructions->first();
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
			if(!empty($paymentInstruction)){
				if($request->save_for_future == 'Y'){
					$data['save_for_future'] = 'Y';
					$paymentInstruction->update($data);
				}
				$paymentInstruction->payment_instruction_id = $paymentInstruction->id;// '#'.str_pad($paymentInstruction->id, 10, "0", STR_PAD_LEFT);
				$data = $paymentInstruction;
				return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
				exit;
			}

            $paymentInstruction = new Payment_Instraction();
            $paymentInstruction->user_id = $request->user_id;
            $paymentInstruction->bank_name = $request->bank_name;
            $paymentInstruction->account_number = $request->account_number;
            $paymentInstruction->ifsc_code = $request->ifsc_code;
            $paymentInstruction->branch = $request->branch;
            $paymentInstruction->save_for_future = $request->save_for_future;

            $paymentInstruction->save();

            $paymentInstruction->payment_instruction_id = $paymentInstruction->id;// '#'.str_pad($paymentInstruction->id, 10, "0", STR_PAD_LEFT);
            $data = $paymentInstruction;
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;

    }
    public function getPaymentInstruction()
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

        if(!$user){
            $message = 'Not valid user';
            $code = 400;
            $status = false;
            return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
        }

        $paymentInstructions = Payment_Instraction::where('user_id',request()->user_id)->where('save_for_future','=','Y');

        $paymentInstruction = $paymentInstructions->get();

        if(empty($paymentInstruction)){
            $code = 404;
            $messages = 'Failed';
            $status = false;
        }

        return response()->json( ['code' => $code, 'message' => $messages, 'status' => $status,'result' => $paymentInstruction]);
        exit;

    }

    public function editPaymentInstruction(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = true;
        $error = '';

        $paymentInstruction = Payment_Instraction::where('id',$request->payment_instruction_id)->where('user_id',$request->user_id)->where('save_for_future','=','Y')->first();

        if(empty($paymentInstruction)){
            $code = 404;
            $messages = 'Payment Instruction not exist';
            $status = false;
        }

        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $paymentInstruction]);
        exit;
    }

    public function updatePaymentInstruction(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Payment Instruction Updated Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'payment_instruction_id'=>'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $paymentInstruction = Payment_Instraction::where('id',$request->payment_instruction_id)->where('user_id',$request->user_id)->where('save_for_future','=','Y')->first();
            if(empty($paymentInstruction)){
                $message = 'Payment Instruction not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
            $data['bank_name'] = ($request->bank_name)?$request->bank_name:$paymentInstruction->bank_name;
            $data['account_number'] = ($request->account_number)?$request->account_number:$paymentInstruction->account_number;
            $data['ifsc_code'] = ($request->ifsc_code)?$request->ifsc_code:$paymentInstruction->ifsc_code;
            $data['branch'] = ($request->branch)?$request->branch:$paymentInstruction->branch;
            $data['save_for_future'] = ($request->save_for_future)?$request->save_for_future:$paymentInstruction->save_for_future;

            $paymentInstruction->update($data);

            $paymentInstruction->payment_instruction_id =  $paymentInstruction->id;//'#'.str_pad($paymentInstruction->id, 10, "0", STR_PAD_LEFT);
            $data = $paymentInstruction;
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }
    public function deletePaymentInstruction(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Payment Instruction Deleted Successfully';
        $status = true;
        $error = '';

            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $paymentInstruction = Payment_Instraction::where('id',$request->payment_instruction_id)->where('user_id',$request->user_id)->where('save_for_future','=','Y')->first();
            if(empty($paymentInstruction)){
                $message = 'Payment Instruction not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
                exit;
            }
            $paymentInstruction->delete();
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;

    }
}
