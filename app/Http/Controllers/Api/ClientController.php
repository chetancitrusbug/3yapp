<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Validation\Rule;
use DB;
use App\Client;
use App\User;

class ClientController extends Controller{

    public function list(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';
        $notification_count=get_notification_count(request()->user_id);

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required', 
            'guest' => 'numeric',

        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        }
        else
        {
            $notification_count=get_notification_count(request()->user_id);

            $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();
			$client = [];
            if(!$user){
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'notification_count' =>$notification_count]);
            }
            if(isset($request->guest) && request()->guest == 1)
            {
					$parent_id = $request->parent_id;
                    if(request()->onlyname){
                        $client = Client::where('user_id',request()->parent_id)->orderBy('first_name','asc')->select(DB::raw('INITCAP(first_name)'),DB::raw('INITCAP(last_name)'),'id')->get();
                    }else{
                        $client = Client::select([
                                'client.*','client.id as client_id',
                                DB::raw('(CASE WHEN image != "" THEN CONCAT("'.url('/').'","/",image) ELSE "" END) AS image'),
                                DB::raw('INITCAP(first_name) as first_name'),DB::raw('INITCAP(last_name) as last_name')
                            ])
                            ->where('user_id',request()->parent_id)->orderBy('first_name','asc')->with(['guestuser'=> function($query) use ($parent_id){
								$query->where('parent_id',$parent_id);
							}])->get();
                    }
            }
            else
            {
				$user_id = request()->user_id;
                if(request()->onlyname){
                    $client = Client::where('user_id',request()->user_id)->orderBy('first_name','asc')->select(DB::raw('INITCAP(first_name)'),DB::raw('INITCAP(last_name)'),'id')->get();
                }else{
                    $client = Client::select([
                            'client.*','client.id as client_id',
                            DB::raw('(CASE WHEN image != "" THEN CONCAT("'.url('/').'","/",image) ELSE "" END) AS image'),
                            DB::raw('INITCAP(first_name) as first_name'),DB::raw('INITCAP(last_name) as last_name')
                        ])
                        ->where('user_id',request()->user_id)->with(['guestuser'=> function($query) use ($user_id){
								$query->where('parentId',$user_id);
							}])->orderBy('first_name','asc')->get();
                }
            }

            if(empty($client)){
                $code = 404;
                $messages = 'Failed';
                $status = false;
            }

            return response()->json(['result' => $client, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' =>0]);

        }


        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' =>$notification_count]);
        exit;
    }

    public function store(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Client added Successfully';
        $status = true;
        $error = '';
        $user_id = $request->user_id;
        $rules = array(
            'email' => [
                        'required','email',
                        Rule::unique('client')->where(function ($query) use ($user_id) {
                            return $query->where('user_id', $user_id);
                        }),
                    ],
            'user_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
     //     'address'=>'required',
	//		'city'=>'required',
	//		'notes'=>'required',
	//		'company_name'=>'required',
	//		'postcode'=>'required|numeric',
            'phone' => 'required_without:mobile',
        );
		
        if($request->get('image')){
            $rules['image'] = 'mimes:jpg,jpeg,png';
        }

        $validator = \Validator::make($request->all(), $rules, ['phone.required_without'=> 'Either phone number or landline number necessary.']);
		
        if ($validator->fails()) {
            $validation = $validator;
			$status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
			//dd($request->company_name);
            $result = Client::where('email',$request->email)->where('user_id',$request->user_id)->first();
            $user = User::find($request->user_id);

            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            if($result){
                $message = 'Client already exist';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $client = new Client();
            $client->user_id = $request->user_id;
            $client->first_name = $request->first_name;
            $client->last_name = $request->last_name;
            $client->email = $request->email;
            $client->phone = $request->phone;
            $client->mobile = $request->mobile;
            $client->address = ($request->address)?$request->address:'';
            $client->company_name = ($request->company_name)?$request->company_name:'';
            $client->city = ($request->city)?$request->city:'';
            $client->notes = ($request->notes)?$request->notes:'';
            $client->postcode = ($request->postcode)?$request->postcode:'';
            $client->address_line1 = ($request->address_line1)?$request->address_line1:'';
            $client->address_line2 = ($request->address_line2)?$request->address_line2:'';
            $client->status = 1;
			
            $name = $request->first_name.'_'.$request->last_name;//str_replace(' ','_',$request->client_name);

            if($request->file('image')){
                $image = $request->file('image');

                $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

                $image->move('uploads/client', $filename);

                $client->image = 'uploads/client/'.$filename;
            }else{
				$client->image = 'uploads/user/avatar.jpg';
			}

            $client->save();
            $client->image = url('/').'/'.$client->image;
            $client->client_id = $client->id;
            $data = $client;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function edit(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = true;
        $error = '';

        $client = Client::where('id',$request->client_id)->where('user_id',$request->user_id)->first();

        if(empty($client)){
            $code = 404;
            $messages = 'Client not exist';
            $status = false;
        }

        return response()->json(['result' => $client, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function update(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Client Details Updated Successfully';
        $status = true;
        $error = '';
		$user_id = $request->user_id;
		$client_id = $request->client_id;
        $rules = array(
            'phone' => 'required_without:mobile',
			'email' => [
                        'required','email',
                        Rule::unique('client')->where(function ($query) use ($user_id,$client_id) {
                            return $query->where('user_id', $user_id)->where('id','!=',$client_id);
                        }),
                    ],//'required|unique:client,email,' . $request->client_id,
        );

        if($request->get('image')){
            $rules['image'] = 'mimes:jpg,jpeg,png';
        }

        $validator = \Validator::make($request->all(), $rules, ['phone.required_without'=> 'Either phone number or landline number necessary.']);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            $client = Client::where('id',$request->client_id)->where('user_id',$request->user_id)->first();
            if(!$client){
                $message = 'Client not found';
                $code = 400;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $data['first_name'] = ($request->first_name)?$request->first_name:$client->first_name;
            $data['last_name'] = ($request->last_name)?$request->last_name:$client->last_name;
            $data['email'] = ($request->email)?$request->email:$client->email;
            $data['phone'] = (isset($request->phone))?$request->phone:$client->phone;
            $data['mobile'] = (isset($request->mobile))?$request->mobile:$client->mobile;
            /*$data['address'] = ($request->address)?$request->address:$client->address;
            $data['company_name'] = ($request->has('company_name'))?$request->company_name:$client->company_name;
			$data['city'] = ($request->city)?$request->city:$client->city;
            $data['notes'] = ($request->notes)?$request->notes:$client->notes;
            $data['postcode'] = ($request->postcode)?$request->postcode:$client->postcode ;*/
			$data['address'] = ($request->address)?$request->address:'';
            $data['company_name'] = ($request->company_name)?$request->company_name:'';
            $data['city'] = ($request->city)?$request->city:'';
            $data['notes'] = ($request->notes)?$request->notes:'';
            $data['postcode'] = ($request->postcode)?$request->postcode:'';
			$data['address_line1'] = ($request->address_line1)?$request->address_line1:'';
            $data['address_line2'] = ($request->address_line2)?$request->address_line2:'';
			
            $name = $data['first_name'].'_'.$data['last_name'];//str_replace(' ','_',$request->client_name);

            if($request->file('image')){

                if(file_exists($client->image)){
					if($client->image != 'uploads/user/avatar.jpg')
						unlink($client->image); //delete previously uploaded image
                }

                $image = $request->file('image');

                $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

                $image->move('uploads/client', $filename);

                $data['image'] = 'uploads/client/'.$filename;
            }
			
            $client->update($data);
            $client->image = url('/').'/'.$client->image;
            $client->client_id = $client->id;
            $data = $client;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }
	public function delete(Request $request){
		$data = array();
        $code = 200;
        $messages = 'Client Deleted Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'client_id' => 'required',
            'user_id' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);

            if($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $client = Client::where('id',$request->client_id)->where('user_id',$request->user_id)->first();
            if($client){
                if(file_exists($client->image) && $client->image != 'uploads/client/avatar.jpg'){
                    unlink($client->image); //delete previously uploaded image
                }
                $client->delete();
            }else{
                $messages = 'Client not found';
                $status = true;
            }
        }
		return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function ClientExists(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Client Email Address not registered.';
        $status = true;
        $error = '';

        $rules = array(
            'email' => 'required|email',
            'user_id' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $user = User::find($request->user_id);

            if($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
             $rules = array(
                    'email' => [
                                'required','email',
                                Rule::unique('client')->where(function ($query) use ($user_id) {
                                    return $query->where('user_id', $user_id);
                                }),
                            ],
                );
                    $true = Rule::unique('client')->where(function ($query) use ($user_id) {
                                    return $query->where('user_id', $user_id);
                                });
                                $validator = \Validator::make($request->all(), $rules, []);

                if ($validator->fails()) {
                    $validation = $validator;
                    $status = false;
                    $code = 400;
                    $msgArr = $validator->messages()->toArray();
                    $messages = "You are already registered this email as a client.";
                }
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    
    public function details(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';
        $notification_count=get_notification_count(request()->user_id);

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required', 
            'guest' => 'numeric|min:1|max:1',
            'client_id' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        }
        else
        {
            $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

            if(!$user){
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'notification_count' =>$notification_count]);
            }
            if(isset($request->guest))
            {
                if($request->guest == 1)
                {
                    $client = Client::where('id',$request->client_id)->where('user_id',$request->parent_id)->first();

                }
            }
            else
            {
                $client = Client::where('id',$request->client_id)->where('user_id',$request->user_id)->first();

            }
          
            if(empty($client)){
                $code = 404;
                $messages = 'Client not exist';
                $status = false;
            }
            return response()->json(['result' => $client, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' =>$notification_count]);

        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' =>$notification_count]);

        exit;
    }

}