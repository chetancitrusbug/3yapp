<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Api\PaymentInstructionController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use DB;
use App\Invoice;
use App\User;
use App\Client;
use App\Expense;
use App\Payment_Instraction;
use PDF;
use App\InvoiceItem;
class InvoiceController extends Controller{

    public function list(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';
        $notification_count=get_notification_count(request()->user_id);

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required',
            'guest' => 'numeric',
        );

        if(request()->has('start_date') || request()->has('end_date')){
            $rules['start_date'] = 'required';
            $rules['end_date'] = 'required';
        }


        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {

            $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

            if(!$user){
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'notification_count' =>$notification_count]);
            }

            $invoices = Invoice::select([
                    'invoice.*','client.first_name as client_first_name','client.last_name as client_last_name','client.company_name as client_company_name','client.email as client_email',
                    DB::raw('CONCAT("'.url('/').'","/",invoice_image) AS invoice_image'),
                    'invoice.id as inv_id',
                ])
				->with('invoiceItem','paymentTerm')
                ->leftJoin('client','client.id','invoice.client_id');
                if(isset($request->guest) && request()->guest)
                {
                    $invoices->where('invoice.user_id',request()->parent_id);
                }
                else
                {
                    $invoices->where('invoice.user_id',request()->user_id);
                }

            if(request()->has('client_id') && request()->client_id != ''){
                $invoices->whereIn('client_id',explode(',',request()->client_id));
            }

            if(request()->has('start_date') && request()->has('end_date')){
                $invoices->whereDate('date_time','>=',request()->start_date);
                $invoices->whereDate('date_time','<=',request()->end_date);
            }
			//$invoices;
            $invoices = $invoices->orderby('invoice.id','desc')->get();

            if($invoices->count() <= 0){
                $code = 200;
                $messages = 'Data not found';
                $status = true;
            } else {
                foreach ($invoices as $invoice) {
                    /*$client = Client::where('id',$invoice->client_id)->first();
                    if($client){
                        $invoice->client_first_name = $client->first_name;
                        $invoice->client_last_name = $client->last_name;
                        $invoice->client_company_name = $client->company_name;
                        $invoice->client_email = $client->email;
                    } else {
                        $invoice->client_first_name = '';
                        $invoice->client_last_name = '';
                        $invoice->client_company_name = '';
                        $invoice->client_email = '';
                    } */

                    $invoice->payment_instruction = array();
                    if($invoice->payment_mode == 1){
                        $payment_instruction = Payment_Instraction::where('id',$invoice->payment_instruction_id)->first();
                        $invoice->payment_instruction =($payment_instruction)?$payment_instruction:array();
                    }
                    $data[] = $invoice;
                }
            }
         //   $data = $invoice;
        }

        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' =>$notification_count]);
        exit;
    }

	public function InvoicePdf(Request $request){
		$data = array();
        $code = 200;
        $messages = 'Successfully';
        $status = true;
        $error = '';
		if($request->api_token && $request->invoice_id){
			$user = User::find($request->user_id);
			if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
			 $invoice = Invoice::select([
                    'invoice.*','client.first_name as client_fname','client.last_name as client_lname','client.company_name as client_company_name','client.email as client_email','client.address_line1 as client_address','client.address_line2 as client_address_line2','client.city as client_city','client.postcode as client_postcode','users.website','users.company_vat_no','client.phone as clien_phone','bank_name','account_number','ifsc_code','branch','users.contact_no','users.email','users.first_name','users.last_name','users.company_logo','users.address','users.country','users.vat as users_vat','users.company_name as company_name',
                    DB::raw('CONCAT("'.url('/').'","/",invoice_image) AS invoice_image'),
                    'invoice.id as inv_id',
                ])
				->with('invoiceItem','paymentTerm')
				->leftJoin('payment__instractions','payment__instractions.id','invoice.payment_instruction_id')
				->leftJoin('client','client.id','invoice.client_id')
                ->leftJoin('users','users.id','client.user_id')
                ->where('invoice.user_id',request()->user_id)->where('invoice.id',$request->invoice_id)->first();
			
			//$invoice = Invoice::where('invoice.id',$request->invoice_id)->where('invoice.user_id',$request->user_id)->leftJoin('client','client.id','invoice.client_id')->leftJoin('payment__instractions','payment__instractions.id','invoice.payment_instruction_id')->with('invoiceItem')->first();
			if(!$invoice){
				$message = 'Invalid Invoice';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
				exit;
            } 

			
			//PDF::loadView('invoice.pdffile',['invoice' => $invoice],[],['format' => 'A4','display_mode' => 'fullpage','default_font'=>'Poppins'])->stream('result.pdf')->save(public_path().'/uploads/invoice/pdf/'.str_replace("#","",$invoice->invoice_id).".pdf");
			PDF::loadView('invoice.pdffile',['invoice' => $invoice],[],['format' => 'A4','display_mode' => 'fullpage'])->save(public_path().'/uploads/invoice/pdf/'.str_replace("#","",$invoice->invoice_id).".pdf");
			
			$data['url'] =  \URL::to('').'/uploads/invoice/pdf/'.str_replace("#","",$invoice->invoice_id).".pdf";
			
		}else{
			$messages = "Something went wrong!";
			$code = 400;
		}
		return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);	
		exit;
		 
	}
	
    public function store(Request $request){
		//echo json_encode(array('0'=>array('item_title'=>'Test','qty'=>1,'item_price'=>10,'item_sub_total'=>10)));exit;
        $data = array();
        $code = 200;
        $messages = 'Invoice added Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'client_id' => 'required',
            'subject' => 'required',
            //'description' => 'required',
            'net_amount' => 'required',
            //'vat' => 'required',
            'invoice_image' => 'mimes:jpg,jpeg,png',
            'date_time' => 'required',
            //'payment_mode' => 'required',
			'invoice_item' => 'required',
			//'due_date' => 'required'
			//'currency_code' => 'required',
			
        );
        if($request->payment_mode == 1){
            if(!request()->has('payment_instruction_id') || empty(request()->payment_instruction_id)){
                $rules['bank_name'] = 'required';
                $rules['account_number'] = 'required';
                $rules['ifsc_code'] = 'required';
                $rules['branch'] = 'required';
                $rules['save_for_future'] = 'required';
            } else {
                $rules['payment_instruction_id'] = 'required';
            }
        }

        $validator = \Validator::make($request->all(), $rules, ['invoice_item.required' => 'Please Enter at list on item!']);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            if($request->payment_mode == 1){
                if(!request()->has('payment_instruction_id') || empty(request()->payment_instruction_id)){
                    $paymentInstructionObj = new PaymentInstructionController;
                    $paymentinstructiondata = $paymentInstructionObj->store($request);
                    $paymentinstructiondata =  json_decode(json_encode($paymentinstructiondata, true));
                    $request->payment_instruction_id =  $paymentinstructiondata->original->result->payment_instruction_id;
                }
            }
            $user = User::find($request->user_id);
            $client = Client::where('id',$request->client_id)->where('user_id',$request->user_id)->first();

            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            if(!$client){
                $message = 'Client not exist';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
			$item_lists = json_decode($request->invoice_item);
			if(count($item_lists) > 0){
				if(count($item_lists) == 1){
					if($item_lists[0]->qty == 0){
						$message = 'Item does not have 0 qty.';
						$code = 400;
						$status = false;
						return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
						exit;
					}
				}
				if(isset($request->company_vat_no) && $request->company_vat_no > 0){
					$user->company_vat_no = $request->company_vat_no;
					$user->save();
				}
				$invoice = new Invoice();
				$invoice->user_id = $request->user_id;
				$invoice->client_id = $request->client_id;
				$invoice->subject = $request->subject;
				$invoice->description = $request->description;
				$invoice->net_amount = $request->net_amount;
				$invoice->vat = (isset($request->vat))?$request->vat : 0;
				$vat = round(($request->net_amount*$request->vat)/100,2);
				$invoice->total_amount = $request->net_amount+$vat;
				$invoice->date_time = date('Y-m-d h:i:s',strtotime(str_replace('/', '-', $request->date_time)));
				$invoice->payment_mode = (isset($request->payment_mode))? $request->payment_mode : 2;
				$invoice->due_date = (isset($request->due_date))?date('Y-m-d',strtotime(str_replace('/', '-', $request->due_date))) : '';
				$invoice->payment_instruction_id = ($request->payment_mode == 1)?$request->payment_instruction_id:'';
				$invoice->is_invoice = $request->is_invoice;
				$invoice->receipt_payment_mode = $request->receipt_payment_mode;
				$invoice->payment_terms_id = $request->payment_terms_id;
				$invoice->currency_code = ($request->currency_code)?$request->currency_code:'GBP';
				if($request->invoice_status == '')
					$invoice->invoice_status = 'Draft';
				else	
					$invoice->invoice_status = $request->invoice_status;
				
				if($request->is_invoice){
					$invoice->invoice_status = 'Paid';
				}
				$name = str_replace(' ','_',$request->subject);

				if($request->file('invoice_image')){
					$image = $request->file('invoice_image');

					$filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

					$image->move('uploads/invoice', $filename);

					$invoice->invoice_image = 'uploads/invoice/'.$filename;
				}
				
				#00000000053
				#che53
				$invoices = array();   
				if(isset($request->invoice_refrence_no) && $request->invoice_refrence_no != ''){
					$invoices = Invoice::where('invoice_id',$request->invoice_refrence_no)->where('user_id',$request->user_id)->get();
				}else{
					$invoices = array();   
				}
				
				if(count($invoices) == 0){
					$invoice->save();
				}else{
					$message = 'Invoice Reference number is already added, Please use difference number.';
					$code = 400;
					$status = false;
					return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
					exit;
				}
				if(!($request->invoice_refrence_no) && $request->invoice_refrence_no == ''){
					
					//$invoiceId = '#'.str_pad($invoice->id, 10, "0", STR_PAD_LEFT);
					$invoiceId = '#'.substr($client->first_name, 0, 3).$invoice->id;
				}else{
					$invoiceId = $request->invoice_refrence_no;
				}
				
				Invoice::where('id',$invoice->id)->update(['invoice_id'=>$invoiceId]);
				$invoice->invoice_id = $invoiceId;//'#'.str_pad($invoice->id, 10, "0", STR_PAD_LEFT);
				$ItemData = array();
				foreach($item_lists as $key => $item){
					$ItemData[$key]['invoice_item'] = $item->item_title;
					$ItemData[$key]['qty'] = $item->qty;
					$ItemData[$key]['unit'] = (isset($item->unit))? $item->unit : '';
					$ItemData[$key]['item_price'] = $item->item_price;
					$ItemData[$key]['item_sub_price'] = $item->item_sub_total;
					$ItemData[$key]['invoice_id'] = $invoice->id;
					$ItemData[$key]['created_at'] = date('y-m-d h:i:s');
				}
				InvoiceItem::insert($ItemData);
				$data = $invoice;
				$data['company_vat_no'] = $request->company_vat_no;
			}else{
				$message = 'Invoice not have any Item, Please add atleast one item.';
				$code = 400;
				$status = false;
				return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
				exit;
			}
			
            
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function edit(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = true;
        $error = '';

        $rules = array(
            'user_id' => 'required',
            'client_id' => 'required',
        );


        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $invoice = Invoice::where('id',$request->invoice_id)->with('invoiceItem','paymentTerm')->where('user_id',$request->user_id)->where('client_id',$request->client_id)->first();

            if($invoice->count() <= 0){
                $code = 404;
                $messages = 'Invoice not exist';
                $status = false;
            }
            $data = $invoice;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function update(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Invoice updated Successfully';
        $status = true;
        $error = '';
		
        $rules = array(
            'invoice_id' => 'required',
            'user_id' => 'required',
            'invoice_image' => 'mimes:jpg,jpeg,png',
            //'payment_mode' => 'required',
            'invoice_status'=>'in:Draft,Sent,Cancel,Paid',
            //'currency_code'=>'required',
        );
        if($request->payment_mode == 1){
            if(!request()->has('payment_instruction_id') || empty(request()->payment_instruction_id)){
                $rules['bank_name'] = 'required';
                $rules['account_number'] = 'required';
                $rules['ifsc_code'] = 'required';
                $rules['branch'] = 'required';
                $rules['save_for_future'] = 'required';
            } else {
                $rules['payment_instruction_id'] = 'required';
            }
        }

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            $invoice = Invoice::where('id',$request->invoice_id)->where('user_id',$request->user_id)->where('client_id',$request->client_id)->first();

            if(!$invoice){
                $message = 'Invoice not found';
                $code = 400;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
				exit;
            }
			
			/*
            if($request->payment_mode == 1){
                if(!request()->has('payment_instruction_id') || empty(request()->payment_instruction_id)){
                    $paymentInstructionObj = new PaymentInstructionController;
                    $paymentinstructiondata = $paymentInstructionObj->store($request);
                    $paymentinstructiondata =  json_decode(json_encode($paymentinstructiondata, true));
                    $request->payment_instruction_id =  $paymentinstructiondata->original->result->payment_instruction_id;
				}else if(request()->has('payment_instruction_id') || ( request()->has('bank_name') || request()->has('account_number') || request()->has('ifsc_code') || request()->has('branch'))  ){
					$paymentInstructionObj = new PaymentInstructionController;
					$paymentinstructiondata = $paymentInstructionObj->updatePaymentInstruction($request);
					dd($paymentinstructiondata);
					$paymentinstructiondata =  json_decode(json_encode($paymentinstructiondata, true));
					$request->payment_instruction_id =  $paymentinstructiondata->original->result->payment_instruction_id;
				}
            }
			*/
			$item_lists = json_decode($request->invoice_item);
			if(count($item_lists) > 0){
				if(count($item_lists) == 1){
					if($item_lists[0]->qty == 0){
						$message = 'Item does not have 0 qty.';
						$code = 400;
						$status = false;
						return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
						exit;
					}
				} 
				
				$invoice->subject = ($request->subject)?$request->subject:$invoice->subject;
				$invoice->description = ($request->description)?$request->description:$invoice->description;
				$invoice->net_amount = ($request->net_amount)?$request->net_amount:$invoice->net_amount;
				$invoice->vat = (isset($request->vat))?$request->vat:$invoice->vat;
				$vat = round(($invoice->net_amount*$invoice->vat)/100,2);
				$invoice->total_amount = $vat+$invoice->net_amount;
				$invoice->date_time = ($request->date_time)?date('Y-m-d h:i:s',strtotime(str_replace('/', '-', $request->date_time))):$invoice->date_time;
				$invoice->due_date = ($request->due_date)?date('Y-m-d',strtotime(str_replace('/', '-', $request->due_date))):$invoice->due_date;
				$invoice->status = ($request->status)?$request->status:$invoice->status;
				$invoice->invoice_status = ($request->invoice_status)?$request->invoice_status:$invoice->invoice_status;
				$invoice->payment_mode = (request()->has('payment_mode'))?$request->payment_mode:$invoice->payment_mode;
				$invoice->payment_instruction_id = ($request->payment_instruction_id && $request->payment_mode == 1)? $request->payment_instruction_id:'';
				$invoice->payment_terms_id = ($request->payment_terms_id)? $request->payment_terms_id:$invoice->due_date;
				$invoice->currency_code = ($request->currency_code)?$request->currency_code:'GBP';
				$invoice->is_invoice = (isset($request->is_invoice))? $request->is_invoice : $invoice->is_invoice;
				$invoice->receipt_payment_mode = (isset($request->receipt_payment_mode))? $request->receipt_payment_mode : $invoice->receipt_payment_mode;
				$name = str_replace(' ','_',($request->subject)?$request->subject:$invoice->subject);

				if($request->file('invoice_image')){
					if(file_exists($invoice->invoice_image)){
						unlink($invoice->invoice_image); //delete previously uploaded image
					}
					$image = $request->file('invoice_image');

					$filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

					$image->move('uploads/invoice', $filename);

					$invoice->invoice_image = 'uploads/invoice/'.$filename;
				}
				if($request->is_invoice){
					$invoice->invoice_status = 'Paid';
				}
				$invoice->save();
				$invoice->invoice_image = url('/').'/'.$invoice->invoice_image;
				InvoiceItem::where('invoice_id',$request->invoice_id)->delete();
				$ItemData = array();
				foreach($item_lists as $key => $item){
					$ItemData[$key]['invoice_item'] = $item->item_title;
					$ItemData[$key]['qty'] = $item->qty;
					$ItemData[$key]['unit'] = (isset($item->unit))? $item->unit : '';
					$ItemData[$key]['item_price'] = $item->item_price;
					$ItemData[$key]['item_sub_price'] = $item->item_sub_total;
					$ItemData[$key]['invoice_id'] = $request->invoice_id;
					$ItemData[$key]['created_at'] = date('y-m-d h:i:s');
				}
				InvoiceItem::insert($ItemData);
				$data = $invoice;
			}else{
				$message = 'Please add Atleast one item.';
                $code = 400;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
			}
			
            
			
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }
	
	public function updatePayment(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Invoice Status Update Successfully';
        $status = true;
        $error = '';

        $rules = array(
            'invoice_id' => 'required',
            'user_id' => 'required',
            'invoice_status'=>'in:Draft,Sent,Cancel,Paid'
        );
        
        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
			$invoice = Invoice::where('invoice.id',$request->invoice_id)->where('invoice.user_id',$request->user_id)->first();
            //$invoice = Invoice::where('id',$request->invoice_id)->where('user_id',$request->user_id)->where('client_id',$request->client_id)->first();
            if(!$invoice){
                $message = 'Invoice not found';
                $code = 400; 
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
			if($request->invoice_status == "Sent"){
				//$invoiceForPdf = Invoice::where('invoice.id',$request->invoice_id)->join('client','client.id','invoice.client_id')->where('invoice.user_id',$request->user_id)->leftJoin('payment__instractions','payment__instractions.id','invoice.payment_instruction_id')->with('invoiceItem')->where('client_id',$request->client_id)->first();
				$invoice = Invoice::select([
                    'invoice.*','client.first_name as client_fname','client.last_name as client_lname','client.company_name as client_company_name','client.email as client_email','client.address_line1 as client_address','client.address_line2 as client_address_line2','client.city as client_city','client.postcode as client_postcode','users.website','users.company_vat_no','client.phone as clien_phone','bank_name','account_number','ifsc_code','branch','users.contact_no','users.email','users.first_name','users.last_name','users.company_logo','users.address','users.country','users.vat as users_vat','users.company_name as company_name',
                    DB::raw('CONCAT("'.url('/').'","/",invoice_image) AS invoice_image'),
                    'invoice.id as inv_id',
                ])
				->with('invoiceItem','paymentTerm')
				->leftJoin('payment__instractions','payment__instractions.id','invoice.payment_instruction_id')
				->leftJoin('client','client.id','invoice.client_id')
                ->leftJoin('users','users.id','client.user_id')
                ->where('invoice.user_id',request()->user_id)->where('invoice.id',$request->invoice_id)->first();
				PDF::loadView('invoice.pdffile',['invoice' => $invoice],[],['format' => 'A4','display_mode' => 'fullpage'])->save(public_path().'/uploads/invoice/pdf/'.str_replace("#","",$invoice->invoice_id).".pdf");
				$url =  \URL::to('').'/uploads/invoice/pdf/'.str_replace("#","",$invoice->invoice_id).".pdf";
				
				$text = 'This is sample Invoice sent Mail. Please Find attachement For same';
				$template_data = array('invoice' => $invoice,'text'=>$text);
				 \Mail::send(['html' => 'invoice.sentEmail'], $template_data,
					function ($message) use ($invoice,$url) {
						$message->to($invoice->client_email)
						->to('chetan@citrusbug.com')
						->from('user.citrusbug@gmail.com') //not sure why I have to add this
						->subject('Invoice');
						$message->attach($url);
				});
				
			}
            $invoice->invoice_status = ($request->invoice_status)?$request->invoice_status:$invoice->invoice_status;
            $invoice->save();
            $data = $invoice;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function delete(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Invoice Deleted Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'invoice_id' => 'required',
            'api_token' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $invoice = Invoice::where('id',$request->invoice_id)->where('user_id',$request->user_id)->first();
            if(empty($invoice)){
                $message = 'Invoice not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
                exit;
            }
            $invoice->delete();
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }

    public function bussnessprofit(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'api_token' => 'required',
            'user_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        );
        if(request()->has('client_id')){
            $rules['client_id'] = 'required';
        }

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
			$total_invoice_cost = 0;
			$total_expense_cost = 0;
			$data['userExpenceList'] = array();
			$data['client'] = array();
			if(request()->userFilter){
				$users_expense = Expense::where('user_id',$request->user_id)
								->where('client_id',0)
								->whereDate('date_time','>=',date('Y-m-d h:i:s',strtotime(str_replace('/', '-', request()->start_date))))
								->whereDate('date_time','<=',date('Y-m-d h:i:s',strtotime(str_replace('/', '-', request()->end_date))))
								//->whereDate('date_time','>=',date('Y-m-d',strtotime(request()->start_date)))
								//->whereDate('date_time','<=',date('Y-m-d',strtotime(request()->end_date)))
								->where('currency_code',request()->currency)
								->get();
				$data['userExpenceList'] = $users_expense;
			}else{
				if(request()->has('client_id') && !empty(request()->client_id)){
					$clients = Client::select([
								'client.*','client.id as client_id',
								DB::raw('(CASE WHEN image != "" THEN CONCAT("'.url('/').'","/",image) ELSE "" END) AS image'),
								DB::raw('INITCAP(first_name) as first_name'),DB::raw('INITCAP(last_name) as last_name')
							])->whereIn('id',explode(',',request()->client_id))->get();
				} else {
					$clients = Client::select([
								'client.*','client.id as client_id',
								DB::raw('(CASE WHEN image != "" THEN CONCAT("'.url('/').'","/",image) ELSE "" END) AS image'),
								DB::raw('INITCAP(first_name) as first_name'),DB::raw('INITCAP(last_name) as last_name')
							])->where('user_id',$request->user_id)->get();
				}
				
				foreach ($clients as $client) {
					$client_invoice_cost = Invoice::where('user_id',$request->user_id)
													->where('client_id',$client->id)
													->where('invoice_status','Paid')
													->whereDate('date_time','>=',date('Y-m-d h:i:s',strtotime(str_replace('/', '-', request()->start_date))))
													->whereDate('date_time','<=',date('Y-m-d h:i:s',strtotime(str_replace('/', '-', request()->end_date))))
													->where('currency_code',request()->currency)
													->sum('total_amount');
					//$client_invoice_cost =  $client_invoice_cost->get();

					$client_expense_cost = Expense::where('user_id',$request->user_id)
													->where('client_id',$client->id)
													->whereDate('date_time','>=',date('Y-m-d h:i:s',strtotime(str_replace('/', '-', request()->start_date))))
													->whereDate('date_time','<=',date('Y-m-d h:i:s',strtotime(str_replace('/', '-', request()->end_date))))
													//->whereDate('date_time','>=',date('Y-m-d',strtotime(request()->start_date)))
													//->whereDate('date_time','<=',date('Y-m-d',strtotime(request()->end_date)))
													->where('currency_code',request()->currency)
													->sum('total_amount');
													//->get();
					$client->total_invoice_amount = $client_invoice_cost;
					$client->total_expense_amount = $client_expense_cost;
					$total_invoice_cost = $total_invoice_cost+$client_invoice_cost;
					$total_expense_cost = $total_expense_cost+ $client_expense_cost;
					$client->profit_status = (($client_invoice_cost - $client_expense_cost) < 0)?'Loss':'Profit';
					$client->accumulation_amount = ($client_invoice_cost - $client_expense_cost);
					$client->client_id = $client->id;
					if($client->image == ''){
						$client->image = url('/').'/uploads/user/avatar.jpg';
					}
					if($client_expense_cost > 0 || $client_invoice_cost > 0){
						$data['client'][]=$client;
					}
				}
			}
			$users_cost = Expense::where('user_id',$request->user_id)
								->where('client_id',0)
								->whereDate('date_time','>=',date('Y-m-d h:i:s',strtotime(str_replace('/', '-', request()->start_date))))
								->whereDate('date_time','<=',date('Y-m-d h:i:s',strtotime(str_replace('/', '-', request()->end_date))))
								//->whereDate('date_time','>=',date('Y-m-d',strtotime(request()->start_date)))
								//->whereDate('date_time','<=',date('Y-m-d',strtotime(request()->end_date)))
								->where('currency_code',request()->currency)
								->sum('total_amount');
			$total_expense_cost = $total_expense_cost + $users_cost;
			$data['users']['total_expense'] = $users_cost;
            $data['total']['total_invoice_amount'] = $total_invoice_cost;
            $data['total']['currency'] = request()->currency;
            $data['total']['total_expense_amount'] = $total_expense_cost;
            $data['total']['profit_status'] = (($total_invoice_cost - $total_expense_cost) < 0)?'Loss':'Profit';
            $data['total']['accumulation_amount'] = $total_invoice_cost - $total_expense_cost;
            $gain = $total_invoice_cost - $total_expense_cost;
            if($gain == 0){
                $data['total']['percentage'] = 00;
                 $data['total']['profit_status'] = 'Stable';
            } else if($gain < 0){
                $loss = $total_expense_cost - $total_invoice_cost;
                $data['total']['percentage'] = round(($loss/$data['total']['total_expense_amount'])* 100,2);
            } else {
                if($total_expense_cost != 0){
                    $gain = $total_invoice_cost - $total_expense_cost;
                    $data['total']['percentage'] = round(($gain/$total_invoice_cost)* 100,2);
                } else {
                    $data['total']['percentage'] = 100;
                }

            }
        }

        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
    }

    public function details(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';
        $notification_count=get_notification_count(request()->user_id);

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required', 
            'guest' => 'numeric|min:1|max:1',
            'invoice_id' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        }
        else
        {
            $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

            if(!$user){
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'notification_count' =>$notification_count]);
            }
            if(isset($request->guest) && $request->guest == 1)
            {
				$invoice = Invoice::where('id',$request->invoice_id)->where('user_id',$request->parent_id)->where('client_id',$request->client_id)->with('invoiceItem','paymentTerm')->first();

				if($invoice == null){
					$code = 404;
					$messages = 'Invoice not exist';
					$status = false;
				}
                
            }
            else
            {
                $invoice = Invoice::where('id',$request->invoice_id)->with('invoiceItem','paymentTerm')->where('user_id',$request->user_id)->where('client_id',$request->client_id)->first();

                if($invoice == null){
                    $code = 404;
                    $messages = 'Invoice not exist';
                    $status = false;
                }
            }
            return response()->json(['result' => $invoice, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' =>$notification_count]);

           
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' =>$notification_count]);

        exit;
    }
	

}