<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\User;
use App\Category;

class CategoryController extends Controller
{
    public function store(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Category added Successfully';
        $status = true;
        $error = '';
        $user_id = $request->user_id;
        $rules = array(
            'user_id' => 'required',
            'title' => [
                        'required',
                        Rule::unique('category')->where(function ($query) use ($user_id) {
                            return $query->where('user_id', $user_id);
                        }),
                        ]);


        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $category = new Category();
            $category->user_id = $request->user_id;
            $category->title = $request->title;
            $category->status = 1;

            $category->save();

            $category->category_id = $category->id; // '#'.str_pad($paymentInstruction->id, 10, "0", STR_PAD_LEFT);
            $data = $category;
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }
    public function update(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Category Updated Successfully';
        $status = true;
        $error = '';
        $user_id = $request->user_id;
        $rules = array(
            'user_id' => 'required',
            'category_id'=>'required',
            'status'=>'in:0,1',
            'title' => [ 'required',
                    Rule::unique('category')->ignore($request->category_id)->where(function ($query) use ($user_id) {
                            return $query->where('user_id', $user_id);
                    }),
            ]
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $category = Category::where('id',$request->category_id)->where('user_id',$request->user_id)->first();
            if($category->count() <= 0 ){
                $message = 'Category not found';
                $code = 200;
                $status = true;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
            $data['title'] = ($request->title)?$request->title:$category->title;
            $data['status'] = ($request->status)?$request->status:$category->status;


            $category->update($data);

            $category->category_id =  $category->id;//'#'.str_pad($paymentInstruction->id, 10, "0", STR_PAD_LEFT);
            $data = $category;
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;

    }
    public function edit(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'category_id' => 'required',
            );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $category = Category::where('id',$request->category_id)->where('user_id',$request->user_id)->first();

            if(empty($category)){
                $code = 404;
                $messages = 'Category not exist';
                $status = false;
            }
            $data = $category;
        }

        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;

    }
    public function list(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';
        $rules = array(
            'user_id' => 'required',
        );


        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {

            $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

            if(!$user){
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
            if($user->id == 1){
                $categories = Category::all();
            } else {
                $categories = Category::where('user_id',request()->user_id)->orWhere('user_id',1);
                $categories = $categories->get();
            }

            if(empty($categories)){
                $code = 404;
                $messages = 'Failed';
                $status = false;
            }
            $data = $categories;
        }

        return response()->json( ['code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }
    public function delete(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Category Deleted Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'category_id' =>'required'
        );


        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {

            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $category = Category::where('id',$request->category_id)->where('user_id',$request->user_id)->first();
            if(empty($category)){
                $message = 'Category not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
                exit;
            }
            $category->delete();
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;

    }
}
