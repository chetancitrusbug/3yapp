<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use App\User;
use App\GuestUser;
use App\Feedback;
use App\Certificate;
use Carbon\Carbon as Carbon;


class UsersController extends Controller
{
    //
	public function feedback(Request $request){
		$data = array();
        $code = 200;
        $messages = 'Thanks for your valuable feedback, we will get back to you shortly.';
        $status = true;
        if($request->api_token != '' && $request->user_id != '' ){
			$user = User::where('api_token', $request->api_token)->where('id', $request->user_id)->first();
			 if($user){
				$feedbackEmail = Feedback::create(['user_id' => $request->user_id,'description'=>$request->description]);
				$feedbackEmail->notify(new \App\Notifications\Feedback(array('email' => 'martin.syms@three-y.com','name'=>$user->first_name . ' '.$user->last_name)));
				return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => true]);
				exit;
			 }else{
				$code = 400;
				$status = false;
				$messages = 'User not found, Please try again.'; 
			 }
		}else{
			$code = 400;
			$status = false;
			$messages = 'Please try again';
		}
		return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
	}
	public function checkUser(Request $request){
		$data = array();
        $code = 200;
        $messages = 'Email not used';
        $status = true;
        if($request->email !=''){
			$user = User::where('email', $request->email)->where('lgn_type', $request->login_type)->first();
			 if($user){
				$messages = 'Email already used';
				return response()->json(['result' => $data, 'code' => 400, 'message' => $messages, 'status' => false]);
				exit;
			 }
		}
		return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
	}
	
	public function detail(Request $request){
		$data = array();
        $code = 200;
        $messages = 'User details';
        $status = true;
        if($request->user_id !=''){
			$user = User::where('id', $request->user_id)->first();
			if($user){
				$datework = Carbon::parse($user->expire_at);
				$now = Carbon::now();
				$day = $datework->diffInDays($now);
				$user->remaining_day = $day;
				$user->profile_pic = url('/').'/'.$user->profile_pic;
                $user->company_logo = url('/').'/'.$user->company_logo;
				$certificates = Certificate::select([
					'certificate.*',
					\DB::raw('(CASE WHEN name != "" THEN CONCAT("'.url('/').'","/",name) ELSE "" END) AS name'),
				])
				->where('user_id',$request->user_id)->get();
				$user->insurance_certificates = $certificates;
				return response()->json(['result' => $user, 'code' => 200, 'message' => $messages, 'status' => 1]);
				exit;
			}
		}else{
			return response()->json(['result' => $data, 'code' => 400, 'message' => 'User not found', 'status' => false]);
				exit;
		}
		return response()->json(['result' => $data, 'code' => 400, 'message' => 'User not found', 'status' => false]);
        exit;
	}

    public function login(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Login Successfull';
        $status = true;
        $rules = array(
            'user_type' => 'required',
            'login_type' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules, []);

        //dd($validator->passes(), $validator->messages()->toArray());
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            $user = User::where('email', $request->email)->where('lgn_type', $request->login_type)->first();

            if($user && $user->status == 1){
                if (Hash::check($request->password, $user->password)) {
                    $user->api_token = md5(uniqid());
                    $user->update();
                    $user->profile_pic = url('/').'/'.$user->profile_pic;
                    $user->user_id = $user->id;
                    //user certificates
                    $certificates = Certificate::select([
                            'certificate.*',
                            \DB::raw('(CASE WHEN name != "" THEN CONCAT("'.url('/').'","/",name) ELSE "" END) AS name'),
                        ])
                        ->where('user_id',$user->id)->get();

                    $user->insurance_certificates = $certificates;
					$datework = Carbon::parse($user->expire_at);
					$now = Carbon::now();
					$day = $datework->diffInDays($now);
					$user->remaining_day = $day;
                    $data = $user;
                    $messages = 'login successful';
                }else{
                    $messages = 'Password is wrong';
                    $code = 400;
                    $status = 'false';
                }
            }elseif($user && $user->status == 0){
                $code = 400;
                $messages = 'Account not activated. Check your email to activated it.';
                $status = false;
            }else{
                $code = 400;
                $messages = "Couldn't find your 3Y App Account.";
                $status = false;
            }
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }


    public function register(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Registration Successfull.';
        $status = true;
        $error = '';
        $rules = array(
            'user_type' => 'required|in:user,guest',
            'login_type' => 'required|in:simple',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            //'birth_date' => 'required',
            'contact_no' => 'required',
            'country'=> 'required|integer',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            $user = new User();
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->lgn_type = $request->login_type;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->contact_no = $request->contact_no;
            $user->country = $request->country;
            $user->company_name = $request->company_name;
            $user->birth_date = $request->birth_date;
            $user->website = $request->website;
            $user->address_line1 = $request->address_line1;
            $user->address_line2 = $request->address_line2;
            $user->city = $request->city;
            $user->zipcode = $request->zipcode;
            $user->company_vat_no = $request->company_vat_no;
            $user->bio = $request->bio;
            $date1 = Carbon::now();
            $date2 = Carbon::parse($user->birth_date);

            if($date1->diffInYears($date2) < 18){
                $user->is_eligible = 0;
            }else{
                $user->is_eligible = 1;
            }

            $user->referral_code = md5(uniqid());
            $user->profile_pic = 'uploads/user/avatar.jpg';

            if(isset($request->lgn_id) && $request->lgn_id != ''){
                $user->lgn_id  = $request->lgn_id;
				//$user->api_token = md5(uniqid());
            }else{
                $user->lgn_id = '';
                //$user->api_token = '';
                //$user->activation_token = sha1(time() . uniqid() . $user['email']);
            }
			$user->api_token = md5(uniqid());
            $user->status = 1;
            
			$user->expire_at = Carbon::now()->addMonths(1);
            $user->vat = (isset($request->vat) && $request->vat > 0) ? $request->vat : 0;
            $user->save();
			//$user->profile_pic = url('/').'/'.$user->profile_pic;
			if($request->file('profile_pic')){
                $image = $request->file('profile_pic');

                $filename = $request->first_name.'_'.$request->last_name.'_'.uniqid(time()) . '.jpg';// . $image->getClientOriginalExtension();

                $image->move('uploads/user', $filename);

                if(file_exists($user->profile_pic)){
					if($user->profile_pic != 'uploads/user/avatar.jpg')
						unlink($user->profile_pic); //delete previously uploaded profilepic
                }

                $input['profile_pic'] = 'uploads/user/'.$filename;
            }
			
			if($request->file('company_logo')){
                $image = $request->file('company_logo');

                $filename = $request->company_name.'_'.uniqid(time()) . '.jpg';// . $image->getClientOriginalExtension();

                $image->move('uploads/user', $filename);

                if(file_exists($user->company_logo)){
					if($user->company_logo != 'uploads/user/avatar.jpg')
						unlink($user->company_logo); //delete previously uploaded profilepic
                }

                $input['company_logo'] = 'uploads/user/'.$filename;
            }

            if($request->file('insurance_certificates')){
				/*
                $oldCertificate = Certificate::where('user_id',$user->id)->get();

                if(count($oldCertificate)){
                    foreach ($oldCertificate as $key => $value) {
                        if(file_exists($value->name)){
                            unlink($value->name); //delete previously uploaded certificate
                        }
                    }
                }

                Certificate::where('user_id',$user->id)->delete(); */

                //foreach ($request->file('insurance_certificates') as $key => $value) {
					$value = $request->file('insurance_certificates');
                    # code...
                    $file = [];
                    $filename = $request->first_name.'_'.$request->last_name.'_'. uniqid(time()) . '.' . $value->getClientOriginalExtension();
                    $value->move('uploads/user/certificates', $filename);
                    $file['name'] = 'uploads/user/certificates/'.$filename;
                    $file['status'] = 1;
                    $file['user_id'] = $user->id;
                    $certificate = Certificate::create($file);
                //}
            }
			$certificates = Certificate::select([
					'certificate.*',
					\DB::raw('(CASE WHEN name != "" THEN CONCAT("'.url('/').'","/",name) ELSE "" END) AS name'),
				])
				->where('user_id',$user->id)->get();
			if(isset($input) && count($input) > 0 )
				$user->update($input);	
			$user->profile_pic = url('/').'/'.$user->profile_pic;
            $user->company_logo = url('/').'/'.$user->company_logo;
			$user->insurance_certificates = $certificates;
			
            $data = $user;
			$guestUserUpdateChild = GuestUser::where('child_email',$request->email)->update(['childId'=>$user->id]);
            if ($request->user_type == 'user'){
                $user->assignRole('USER');
            }
            else{
                $user->assignRole('GUEST');
            }
            $user->user_id = $user->id;
			 
			$datework = Carbon::parse(Carbon::now()->addMonths(1));
			$now = Carbon::now();
			$day = $datework->diffInDays($now);
			$user->remaining_day = $day;
			$user->expire_at = $user->expire_at->toDateTimeString();;
            $data = $user;
            $user->notify(new \App\Notifications\ActivationLink($user));
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function forgotPassword(Request $request){
		
        $data = array();
        $code = 200;
        $messages = '';
        $status = 'true';
        $error = '';
        $email = $request->get('email');

        $user = User::where('email',$email)
                    ->first();

        if($user)
        {
            if($user->status == 0){
                $status = false;
                $messages = 'User is not active, Please contact your admin';
                $code = 400;
            }else{
				$token = str_random(5);
				
                $response = Password::sendResetLink($request->only('email'), function ($message) {
                    $message->subject($this->getEmailSubject());
                },compact('token'));

                if ($response == "passwords.sent") {
                    $messages = 'Password reset link has been sent to your registered email';
                } else if ($response == "passwords.user") {
                    $status = false;
                    $messages = 'User not found';
                    $code = 400;
                } else {
                    $status = false;
                    $messages = 'Something went wrong ! Please try again later';
                    $code = 400;
                } 
            }
        }
        else{
            $messages = 'Email/User does not exists. Please register.';
            $code = 400;
            $status = 'false';
        }

        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function profileUpdate(Request $request){
        $data = [];
        $flag = 1;
        $message = "Profile Updated";
        $status = true;
        $code = 200;

        $rules = array(
            //'contact_no' => 'numeric',
        ); 
		
        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        }
        else {
			$user = User::where('id',$request->user_id)->first();

            if(!$user){
                $message = 'user not found';
                $code = 400;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Token not valid';
                $code = 400;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $input['first_name'] = ($request->first_name)?$request->first_name:$user->first_name;
            $input['last_name'] = ($request->last_name)?$request->last_name:$user->last_name;
            $input['contact_no'] = ($request->contact_no)?$request->contact_no:$user->contact_no;
            $input['company_name'] = ($request->company_name)?$request->company_name:''; //Optional
            $input['company_tax_no'] = ($request->company_tax_no)?$request->company_tax_no:$user->company_tax_no;
            $input['company_vat_no'] = ($request->company_vat_no)?$request->company_vat_no:$user->company_vat_no;
            $input['website'] = ($request->website)?$request->website:''; //optional
            $input['bio'] = ($request->bio)?$request->bio:''; //optional
			$input['vat'] = (isset($request->vat) && $request->vat > 0) ? $request->vat : 0; 
			$input['address'] = ($request->address)?$request->address:''; //optional
			$input['address_line1'] = ($request->address_line1)?$request->address_line1 : ''; //optional
            $input['address_line2'] = ($request->address_line2)?$request->address_line2: ''; //optional
            $input['city'] = ($request->city)?$request->city : ''; //optional
            $input['zipcode'] = ($request->zipcode)?$request->zipcode : ''; //optional
            $input['country'] = $request->country; //optional
            //$input['company_logo'] = $request->company_logo;
            //$name = str_replace(' ','_',($request->name)?$request->name:$user->name);
			$name = $input['first_name'].'_'.$input['last_name'];
			
            if($request->file('profile_pic')){
                $image = $request->file('profile_pic');

                $filename = $name.'_'.uniqid(time()) . '.jpg';// . $image->getClientOriginalExtension();

                $image->move('uploads/user', $filename);

                if(file_exists($user->profile_pic)){
					if($user->profile_pic != 'uploads/user/avatar.jpg')
						unlink($user->profile_pic); //delete previously uploaded profilepic
                }

                $input['profile_pic'] = 'uploads/user/'.$filename;
            }
			
			if($request->file('company_logo')){
                $image = $request->file('company_logo');

                $filename = $input['company_name'].'_'.uniqid(time()) . '.jpg';// . $image->getClientOriginalExtension();

                $image->move('uploads/user', $filename);

                if(file_exists($user->company_logo)){
					if($user->company_logo != 'uploads/user/avatar.jpg')
						unlink($user->company_logo); //delete previously uploaded profilepic
                }

                $input['company_logo'] = 'uploads/user/'.$filename;
            }

            if($request->file('insurance_certificates')){

               /* $oldCertificate = Certificate::where('user_id',$user->id)->get();

                if(count($oldCertificate)){
                    foreach ($oldCertificate as $key => $value) {
                        if(file_exists($value->name)){
                            unlink($value->name); //delete previously uploaded certificate
                        }
                    }
                }

                Certificate::where('user_id',$user->id)->delete(); */

               // foreach ($request->file('insurance_certificates') as $key => $value) {
				   $value = $request->file('insurance_certificates') ;
                    # code...
                    $file = [];
                    $filename = $name.'_'. uniqid(time()) . '.' . $value->getClientOriginalExtension();
                    $value->move('uploads/user/certificates', $filename);
                    $file['name'] = 'uploads/user/certificates/'.$filename;
                    $file['status'] = 1;
                    $file['user_id'] = $user->id;
                    $certificate = Certificate::create($file);
               // }
            }

            if($user){
                if($user->status ==  '0')
                {
                    $message = 'User is inactive';
                    $code = 400;
                    $status = 'false';
                    return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
                }
				$user->update($input);
				
                $user->profile_pic = url('/').'/'.$user->profile_pic;
                $user->company_logo = url('/').'/'.$user->company_logo;
                $certificates = Certificate::select([
                        'certificate.*',
                        \DB::raw('(CASE WHEN name != "" THEN CONCAT("'.url('/').'","/",name) ELSE "" END) AS name'),
                    ])
                    ->where('user_id',$user->id)->get();

                $user->insurance_certificates = $certificates;
				$datework = Carbon::parse($user->expire_at);
				$now = Carbon::now();
				$day = $datework->diffInDays($now);
				$user->remaining_day = $day;
                $user->url = url('/');
                $user->user_id = $user->id;
                $data = $user;
            }
            else{
                $message = 'User Not Found';
                $status = false;
                $code=400;
            }
        }

        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function changePassword(Request $request){
        $data = [];
        $flag = 1;
        $message = "Password changed successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'password' => 'required|min:6|max:255',
            'old_password' => 'required|different:password',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        }
        else {
            $user = User::where("id", $request->user_id)->first();

            if($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Token not valid';
                $code = 400;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            if ($user == null) {
                $message = 'User not Found';
				$status = false;
				$code=400;
            }
            else{
                if (Hash::check($request->input('old_password'),$user->password)) {
                    $user->password = Hash::make($request->input('password'));
                    $user->save();
                    $message = 'Password changed successfully.';
                    $data = $user;
                }
				else{
                    $message = 'Old Password not correct';
                    $status = false;
                    $code=400;
                }

            }
        }

        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function socialMediaLogin(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Registration Successfull';
        $status = true;
        $error = '';

        if(!in_array($request->login_type,['google,facebook'])){
            $user = User::where('lgn_id',$request->lgn_id)->where('lgn_type',$request->login_type)->first();

            if($user){
                $user->api_token = md5(uniqid());
                $messages = 'Login Successfull';
                return response()->json(['result' => $user, 'code' => $code, 'message' => $messages, 'status' => $status]);
            }else{
                $message = array('email.unique' => 'User Already Registered. Please Login with Password');


                $rules = array(
                    'user_type' => 'required|in:user,guest',
                    'login_type' => 'required|in:google,facebook',
                    'email' => 'required|email|unique:users',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'contact_no' => 'required|numeric',
                    'country'=> 'required|integer',
                    'referral_code'=> 'required_if:user_type,guest',
                );

                $validator = \Validator::make($request->all(), $rules, $message);

                if ($validator->fails()) {
                    $validation = $validator;
                    $status = false;
                    $code = 400;
                    $msgArr = $validator->messages()->toArray();
                    $messages = reset($msgArr)[0];
                } else {
                    $user = new User();
                    $user->first_name = $request->first_name;
					$user->last_name = $request->last_name;
                    $user->lgn_type = $request->login_type;
                    $user->email = $request->email;
                    $user->contact_no = $request->contact_no;
                    $user->country = $request->country;

                    $user->lgn_id  = $request->lgn_id;
                    $user->api_token = md5(uniqid());

                    $user->referral_code  = md5(uniqid());
                    $user->profile_pic = $request->profile_pic;

                    $user->status = 1;

                    $user->save();

                    $data = $user;
                    if ($request->user_type == 'user'){
                        $user->assignRole('USER');
                    }
                    else{
                        $user->assignRole('GUEST');
                    }
                    $user->notify(new \App\Notifications\RegisterThankyou($user));
                }
            }
        }else{
            $status = false;
            $code = 400;
            $messages = 'This login is only for social accounts';
            return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        }

        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }
	
	
}
