<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\PaymentTerms;
use App\User;

class PaymentTermsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {

            $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

            if(!$user){
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
            if($user->id == 1){
                $PaymentTerms = PaymentTerms::all();
            } else {
                $PaymentTerms = PaymentTerms::where('user_id',request()->user_id)->orWhere('user_id',1)->orderby('title','asc');
                $PaymentTerms = $PaymentTerms->get();
            }

            if($PaymentTerms->count() <= 0){
                $code = 200;
                $messages = 'Payment Terms not Found';
                $status = true;
            }
            $data = $PaymentTerms;
        }

        return response()->json( ['code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Payment Terms added Successfully';
        $status = true;
        $error = '';
        $user_id = $request->user_id;
        $rules = array(
            'user_id' => 'required',
            'title' => [
                        'required',
                        Rule::unique('payment_terms')->where(function ($query) use ($user_id) {
                            return $query->where('user_id', $user_id)->orWhere('user_id', '=', 1);
                        }),
                        ]
        );
		

        $validator = \Validator::make($request->all(), $rules, ['title.unique' => 'This Payment Terms already exists.']);

        if ($validator->fails()) {
			$validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $PaymentTerms = new PaymentTerms();
            $PaymentTerms->user_id = $request->user_id;
            $PaymentTerms->title = $request->title;
            $PaymentTerms->status = 'Active';

            $PaymentTerms->save();

            $PaymentTerms->category_id = $PaymentTerms->id; // '#'.str_pad($paymentInstruction->id, 10, "0", STR_PAD_LEFT);
            $data = $PaymentTerms;
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = true;
        $error = '';

        $rules = array(
            'user_id' => 'required',
            'payment_terms_id' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $PaymentTerms = PaymentTerms::where('id',$request->payment_terms_id)->where('user_id',$request->user_id)->first();

            if(empty($PaymentTerms)){
                $code = 404;
                $messages = 'Payment Terms not exist';
                $status = false;
            }
            $data= $PaymentTerms;
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array();
        $code = 200;
        $messages = 'Payment Terms Updated Successfully';
        $status = true;
        $error = '';
        $user_id = $request->user_id;

        $rules = array(
            'user_id' => 'required',
            'status'=>'in:Active,Deactivate',
            'payment_terms_id'=> 'required',
            'title' => [ 'required',
                    Rule::unique('payment_terms')->ignore($request->payment_terms_id)->where(function ($query) use ($user_id) {
                            return $query->where('user_id', $user_id);
                    }),
            ]
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $PaymentTerms = PaymentTerms::where('id',$request->payment_terms_id)->where('user_id',$request->user_id)->first();
            if(empty($category)){
                $message = 'Payment Terms not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
            $data['title'] = ($request->title)?$request->title:$PaymentTerms->title;
            $data['status'] = ($request->status)?$request->status:$PaymentTerms->status;
            $PaymentTerms->update($data);

            $PaymentTerms->category_id =  $PaymentTerms->id;//'#'.str_pad($paymentInstruction->id, 10, "0", STR_PAD_LEFT);
            $data = $PaymentTerms;
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
