<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use DB;
use App\Schedule;
use App\User;
use App\Client;
use App\ScheduleTitle;


class ScheduleController extends Controller
{

    public function store(Request $request)
    {
        $code = 200;
        $messages = 'Event Created Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'client_id' => 'required',
            'title' => 'required',
            //'location' => 'required',
            'date' => 'required',
            'start_time' => 'required',
            'priority' => 'required',
			'end_date' => 'required',
			'all_day' => 'required'
        );
		
        $schedule = array();
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            $client = Client::where('id', $request->client_id)->where('user_id', $request->user_id)->first();

            if (!$user) {
                $messages = 'User Not found';
                $code = 400;
                $status = false;

            } elseif ($user->id != $request->user_id || $user->api_token != $request->api_token) {
                $messages = 'Invalid token';
                $code = 400;
                $status = false;

            } elseif (!$client) {
                $messages = 'Client not exist';
                $code = 400;
                $status = false;

            }elseif(!(strtotime(str_replace('/', '-',$request->date)) >= strtotime(date('d-m-Y')))){
				$messages = 'You can not schedule event for past dates';
                $code = 400;
                $status = false;
			}elseif((strtotime(str_replace('/', '-',$request->end_date))) < (strtotime(str_replace('/', '-',$request->date)))){
				$messages = 'Please select different end date';
                $code = 400;
                $status = false;
			} else {
				$data = $request->except('api_token');
                $start_time = $data['start_time'];
                $end_time = $data['end_time'];
				if($data['end_time'] == ''){
					$end_time = date('H:i',strtotime($start_time) + 60*60);
				}
				$data['end_time'] =$end_time;
				$data['date'] = $date = date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $data['date'].' '.$start_time)));
				$data['end_date'] = $end_date = date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $data['end_date'].' '.$end_time)));
				$data['status'] = 1;
				/*$scheduled = DB::select("SELECT * FROM `schedule` WHERE `user_id` = ".$user->id." AND (( `date` BETWEEN  '".$data['date']."'  AND '".$data['end_date']."' ) OR (`end_date` BETWEEN  '".$data['date']."'  AND '".$data['end_date']."' ) ) AND `status` = 1");
                if(count($scheduled) > 0){
				    $messages = 'You can not schedule on this time, because You have already scheduled anothere task at this time!';
                    $code = 400;
                    $status = false;
                } else {*/
					$schedule = Schedule::create($data);
                /*}*/
            }
        }
        return response()->json(['result' => $schedule, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }
	
	public function checkSchedule(Request $request){
		$code = 200;
        $messages = 'Event is already add!';
        $status = true;
        $error = '';
		$date_format = 'Y-m-d';
		$datas['is_already'] = 1; 
		$data = $request->except('api_token');
		$start_time = $data['start_time'];
		$end_time = $data['end_time'];
		if($data['end_time'] == ''){ 
			$end_time = date('H:i',strtotime($start_time) + 60*60);
		}
		$data['end_time'] =$end_time;
		$data['date'] = $date = date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $data['start_date'].' '.$start_time)));
		$data['end_date'] = $end_date = date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $data['end_date'].' '.$end_time)));
		$data['status'] = 1;
		$scheduled = Schedule::where('schedule.user_id', $request->user_id)->where('schedule.status',1)->where('schedule.id','!=',$data['schedule_id']);
		if($data['date'] && $data['end_date']   && $data['end_date'] !="" && $data['date'] !=""){
			$start_date = $data['date'];
			$end_date = $data['end_date'];
			$w_date1 = " ((schedule.`date` > '$start_date' AND schedule.`date` < '$end_date' ) OR (schedule.`end_date` > '$start_date' AND schedule.`end_date` < '$end_date' ))";
			$w_date2 = " (( schedule.`date` < '$start_date' AND schedule.`end_date` > '$start_date'  ) OR (schedule.`date` < '$end_date' AND schedule.`end_date` > '$end_date' ))";
			$w_date = "(".$w_date1." OR ".$w_date2.")";
			$scheduled->whereRaw($w_date);
		}
		$schedule = $scheduled->get();
		//$scheduled = DB::select("SELECT * FROM `schedule` WHERE `user_id` = ".$request->user_id." AND (( `date` BETWEEN  '".$data['date']."'  AND '".$data['end_date']."' ) OR (`end_date` BETWEEN  '".$data['date']."'  AND '".$data['end_date']."' ) OR  ( `date` = '".$data['date']."' ) OR (`end_date` = '".$data['end_date']."' ) ) AND `status` = 1 AND schedule.id != '".$data['schedule_id']."'");
		if(count($schedule) == 0){
			$messages = 'You can add event!';
			$status = true;
			$datas['is_already'] = 0;
		}
		return response()->json(['result' => $datas, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
	}

	public function getScheduleTitleList(Request $request){
		$code = 200;
        $messages = 'Event Title List';
        $status = true;
        $error = '';
		$data = [];
		if($request->api_token){
			$user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

            if(!$user){
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
            if($user->id == 1){
                $ScheduleTitle = ScheduleTitle::all();
            } else {
                $ScheduleTitle = ScheduleTitle::where('user_id',request()->user_id)->orWhere('user_id',1)->orderby('title','asc');
                $ScheduleTitle = $ScheduleTitle->get();
            }

            if($ScheduleTitle->count() <= 0){
                $code = 200;
                $messages = 'Event Title list not Found';
                $status = true;
            }
            $data = $ScheduleTitle;
		}
		return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
	}
	
	
	public function storeScheduleTitle(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Event Title added Successfully';
        $status = true;
        $error = '';
        $user_id = $request->user_id;
        $rules = array(
            'user_id' => 'required',
            'title' => 'required'
                       
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
				exit;
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
				exit;
            }
			$scheduleTitlSelect = ScheduleTitle::where('title',$request->title)->whereRaw("( `user_id` = '".$request->user_id."' OR `user_id` = 1 )")->first();
			if($scheduleTitlSelect){
				return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $scheduleTitlSelect]);
				exit;
			}
            $ScheduleTitle = new ScheduleTitle();
            $ScheduleTitle->user_id = $request->user_id;
            $ScheduleTitle->title = $request->title;
            $ScheduleTitle->status = 'Active';

            $ScheduleTitle->save();

            $ScheduleTitle->category_id = $ScheduleTitle->id; // '#'.str_pad($paymentInstruction->id, 10, "0", STR_PAD_LEFT);
            $data = $ScheduleTitle;
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }

    public function getSchedule(Request $request)
    {
        $code = 200;
        $messages = 'Event List';
        $status = true;
        $error = '';
        $client_id = $request->client_id;
        $notification_count=get_notification_count(request()->user_id);
        if(isset($request->guest))
        {
            if($request->guest == 1)
            {
                $user_id = $request->parent_id;
            }
            else
            {
                $user_id = $request->user_id;
            }
        }
        else
        {
            $user_id = $request->user_id;
        }
     
        $priority = $request->priority;
		$fromdate = $request->fromdate;
		$todate = $request->todate;
        $scheduled = Schedule::where('schedule.user_id', $user_id);
        if ($priority > 0) {
            $scheduled->where('schedule.priority', $priority);
        }
        if ($client_id > 0) {
            $scheduled->whereRaw('schedule.client_id In ('.$client_id.')');
        }
		

        $scheduled->leftJoin('client', 'client.id', 'schedule.client_id')->where('schedule.status',1);
        $scheduled->leftJoin('schedule_titles', 'schedule_titles.id', 'schedule.title');
        $scheduled->select(DB::raw('schedule.*,client.first_name,client.last_name,schedule.id as schedule_id,schedule_titles.title,schedule_titles.id as titleId'));
		$date_format = 'd-m-Y';

		if($todate != '' && $fromdate != ''){
			$start_time = '00:00:00';
			$end_time = '23:59:00';
			//$scheduled->whereBetween('date',[date('Y-m-d',strtotime($fromdate)),date('Y-m-d',strtotime($todate))]);
			 if($todate == $fromdate){
				$scheduled->whereRaw("(( '".date('Y-m-d',strtotime($fromdate))."' BETWEEN schedule.`date` AND schedule.`end_date` ) OR ( '".date('Y-m-d',strtotime($todate))."' = date(schedule.`date`) ) OR ( '".date('Y-m-d',strtotime($todate))."' =  date(schedule.`end_date`) ))");
			}else{
				if($request->has('todate') && $request->has('fromdate')  && $request->todate !="" && $request->fromdate !=""){
					$start_date = \Carbon\Carbon::createFromFormat($date_format, $request->fromdate)->format('Y-m-d');
					$end_date = \Carbon\Carbon::createFromFormat($date_format, $request->todate)->format('Y-m-d');

					$w_date1 = " ((schedule.`date` > '$start_date' AND schedule.`date` < '$end_date' ) OR (schedule.`end_date` > '$start_date' AND schedule.`end_date` < '$end_date' ))";
					$w_date2 = " (( schedule.`date` < '$start_date' AND schedule.`end_date` > '$start_date'  ) OR (schedule.`date` < '$end_date' AND schedule.`end_date` > '$end_date' ))";
					$w_date = "(".$w_date1." OR ".$w_date2.")";
					$scheduled->whereRaw($w_date);
				}
				 //$scheduled->whereRaw("(( schedule.`date` BETWEEN  '".date('Y-m-d',strtotime($fromdate))." ".$start_time."'  AND '".date('Y-m-d',strtotime($todate)) ." ".$end_time."' ) OR (schedule.`end_date` BETWEEN  '".date('Y-m-d',strtotime($fromdate))." ".$start_time."'  AND '".date('Y-m-d',strtotime($todate)) ." ".$end_time."' ))");
				//$scheduled->whereRaw("schedule.`date` >= '".date('Y-m-d',strtotime($fromdate))."' AND schedule.`end_date` <= '".date('Y-m-d',strtotime($todate))."'");
			}
			//$scheduled->whereRaw("(`schedule`.`date` between '".date('Y-m-d',strtotime($fromdate))."' AND '".date('Y-m-d',strtotime($todate))."' )");
			
		}
		$scheduled->orderby('schedule.date','asc');
        $schedule = $scheduled->get();
        if(count($schedule) == 0){
            $messages = "Data not found";
        }
		$datesArray = array();
		$dateArray = Schedule::where('schedule.user_id', $user_id)->where('schedule.status',1);
		if($client_id){
			$dateArray->whereRaw('schedule.client_id In ('.$client_id.')');
		}
		$dateArray = $dateArray->get();
		
		foreach($dateArray as $date){
			$datesArray[]['date'] = date('Y-m-d',strtotime($date->date));
			if($date->all_day){
				$current = strtotime($date->date);
				$last = strtotime($date->end_date);

				while( $current <= $last ) {
					$datesArray[]['date'] = date('Y-m-d', $current);
					$current = strtotime('+1 day', $current);
				}
				$datesArray[]['date'] = date('Y-m-d', $last);
			}
		}
        return response()->json(['result' => array('schedule' => $schedule,'dates' => $datesArray), 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' =>$notification_count]);
        exit;
    }

	public function updateSchedule(Request $request){
        $code = 200;
        $messages = 'Event Updated Successfully';
        $status = true;
        $error = '';
		$client_id = $request->client_id;
        $user_id = $request->user_id;
        $date = $request->date;
        $end_date = $request->end_date;
		$schedule_id = $request->schedule_id;
		$start_time = $request->start_time;
		$end_time = $request->end_time;
        $date = date('Y-m-d h:i:s',strtotime(str_replace('/', '-', $request->date)));
		$end_date = date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $end_date.' '.$end_time)));
        $user = User::find($request->user_id);

        if(!$user){
            $message = 'User Not found';
            $code = 400;
            $status = false;
            return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
        }

		$scheduled = Schedule::where('schedule.user_id', $user_id)->where('schedule.client_id',$client_id)->where('schedule.id',$schedule_id)->where('schedule.status',1)->get();
		if(count($scheduled) > 0){
            // Changes remaining to convert in laravel
			
			
			$sql = "SELECT * FROM `schedule` WHERE `user_id` = ".$user->id." AND (( `date` BETWEEN  '".$date."'  AND '".$end_date."' ) OR (`end_date` BETWEEN  '".$date."'  AND '".$end_date."' ) ) AND `status` = 1 and id != $schedule_id";
            //$sql = "select * from schedule where user_id = $user_id and date='$date' and id != $schedule_id and status = 1 AND (start_time >= '$start_time' or end_time <= '$start_time' or end_time >= '$end_time' or end_time <= '$end_time')";
            $checkAlreadyScheduled  = DB::select($sql);
            /*
            Schedule::where('user_id', $user->id)->where('date', $date)
                    ->where('status',1)->where('id','!=',$schedule_id)
					->where(function($q) use ($start_time,$end_time) {
							$q->orwhere( 'start_time', '<=' , $start_time)->orwhere( 'end_time' ,'>=', $start_time );
					})->orwhere(function($q) use ($start_time,$end_time) {
						$q->orwhere( 'start_time', '>=' , $end_time)->orwhere( 'end_time' ,'<=', $end_time );
                    })->get();
                    dump($checkAlreadyScheduled); exit;

		/*	$checkAlreadyScheduled = Schedule::where('user_id', $user_id)->where('schedule.status',1)->where('date', $date)
					->where(function($q) use ($start_time,$end_time) {
							$q->where( 'start_time', '<=' , $start_time)->where( 'end_time' ,'>=', $start_time );
					})->orwhere(function($q) use ($start_time,$end_time) {
						$q->where( 'start_time', '>=' , $end_time)->where( 'end_time' ,'<=', $end_time );
					})->where('schedule.id','!=',$schedule_id)->get(); */
			/*if(count($checkAlreadyScheduled) > 0){
				$messages = 'Event can not be update, Please select different time. You have already scheduled at this time.';
				$status = false;
				$code = 400;
			}else{ */
				$scheduled = Schedule::find($schedule_id);
				$data['title'] = ($request->title)?$request->title:$scheduled->title;
				$data['location'] = ($request->location)?$request->location:'';
				$data['date'] = ($request->date)?date('Y-m-d h:i:s',strtotime(str_replace('/', '-', $request->date))):$scheduled->date;
				$data['start_time'] = ($request->start_time)?$request->start_time:$scheduled->start_time;
				$data['end_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $end_date)));
				$data['end_time'] = ($request->end_time)?$request->end_time:$scheduled->end_time;
				$data['priority'] = $request->priority;
				$data['all_day'] = $request->all_day;
				$scheduled->update($data);
			//}
		}else{
			$messages = 'Schedule not found';
			$status = false;
			$code = 400;
		}
		return response()->json(['result' => $scheduled, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
	}

	public function deleteSchedule(Request $request){
        
		$code = 200;
        $messages = 'Event Deleted Successfully';
        $status = true;
        $error = '';
		$client_id = $request->client_id;
        $user_id = $request->user_id;
        $schedule_id = $request->schedule_id;
		$scheduled = Schedule::where('schedule.user_id', $user_id)->where('schedule.client_id',$client_id)->where('schedule.id',$schedule_id)->where('schedule.status',1)->first();
		if($scheduled){
			$scheduled->delete();
		}else{
			$messages = 'Schedule not found';
			$status = false;
			$code = 400;
		}
		return response()->json(['result' => [], 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }
    public function getDetails(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Event details';
        $status = true;
        $notification_count=get_notification_count(request()->user_id);
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'schedule_id' => 'required',
            'api_token' => 'required',
            'guest' => 'numeric|min:1|max:1',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        }
        else
        {
            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data,'notification_count' =>$notification_count]);
            }
            elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'notification_count' =>$notification_count]);
            }
            if(isset($request->guest))
            {
                if($request->guest == 1)
                {
                    $user_id = $request->parent_id;
                }
            }
            else
            {
                $user_id = $request->user_id;
            }
            $scheduled = Schedule::where('schedule.user_id', $user_id)->where('schedule.id',$request->schedule_id);
            $scheduled->join('client', 'client.id', 'schedule.client_id')->where('schedule.status',1);
            $scheduled->leftJoin('schedule_titles', 'schedule_titles.id', 'schedule.title');
            $scheduled->select(DB::raw('schedule.*,client.first_name,client.last_name,schedule.id as schedule_id,schedule_titles.title,schedule_titles.id as titleId'));
            $scheduled->orderby('schedule.date','asc');
            $schedule = $scheduled->first();
          
            if($schedule == null){
                $messages = "Data not found";
                $schedule = [];
            }
            return response()->json(['result' => $schedule, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' =>$notification_count]);

        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' => $notification_count]);

        exit;
    }
}