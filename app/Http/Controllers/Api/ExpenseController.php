<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use DB;
use App\Invoice;
use App\User;
use App\Client;
use App\Expense;
use App\ExpenseCategory;
use App\ExpenceItem;
use PDF;

class ExpenseController extends Controller{

    public function list(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required',
            'guest' => 'numeric',

        );

        if(request()->has('start_date') || request()->has('end_date')){
            $rules['start_date'] = 'required';
            $rules['end_date'] = 'required';
        }

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

            if(!$user){
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
            if(isset($request->guest) && request()->guest == 1)
            {
                $expense = Expense::select([
					'client_expense.*',
					'client_expense.id as client_expense_id',
					DB::raw('(CASE WHEN upload != "" THEN CONCAT("'.url('/').'","/",upload) ELSE "" END) AS upload'),
				])
				->where('user_id',request()->parent_id);
               
            }
            else
            {
                $expense = Expense::select([
                    'client_expense.*',
                    'client_expense.id as client_expense_id',
                    DB::raw('(CASE WHEN upload != "" THEN CONCAT("'.url('/').'","/",upload) ELSE "" END) AS upload'),
                ])
                ->where('user_id',request()->user_id);
            }

            if(request()->has('client_id') && request()->client_id != ''){
                $expense->whereIn('client_id',explode(',',request()->client_id));
            }

            if(request()->has('invoice_id')){
                $expense->whereIn('invoice_id',explode(',',request()->invoice_id));
            }
            if(request()->has('start_date') && request()->has('end_date')){
                $expense->whereDate('date_time','>=',date('Y-m-d h:i:s',strtotime(str_replace('/', '-', request()->start_date))));
                $expense->whereDate('date_time','<=',date('Y-m-d h:i:s',strtotime(str_replace('/', '-', request()->end_date))));
            }
			$expense->orderby('client_expense.id','desc');
            $expense = $expense->with('expenseItem')->get();

            if($expense->count() <= 0){
                $code = 200;
                $messages = 'Expense Not Found';
                $status = true;
            } else {
                foreach ($expense as $exp) {
                    $category = ExpenseCategory::where('id',$exp->category_id)->first();
                    $exp->category_name = $category->title;
                    if($exp->client_id != 0){
                        $client = Client::where('id',$exp->client_id)->first();
                        if($client){
                            $exp->client_first_name = $client->first_name;
                            $exp->client_last_name = $client->last_name;
                            $exp->client_company_name = $client->company_name;
                            $exp->client_email = $client->email;
                        } else {
                            $exp->client_first_name = '';
                            $exp->client_last_name = '';
                            $exp->client_company_name = '';
                            $exp->client_email = '';
                        }
                    } else {
                        $exp->client_first_name = $user->first_name;
                        $exp->client_last_name = $user->last_name;
                        $exp->client_company_name = "Business Cost";
                        $exp->client_email = $user->email;
                    }

                    $data[] = $exp;
                }
            }

        }

        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

	public function viewPdf(Request $request){
		$data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';
        $notification_count=get_notification_count(request()->user_id);
        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required', 
            'guest' => 'numeric',
            'expense_id' => 'required',
           
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) 
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        }
        else
        {
            $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();
            if(!$user)
            {
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
            if(isset($request->guest) && $request->guest == 1)
            {
                $expense = Expense::where('client_expense.id',$request->expense_id)->where('client_expense.user_id',$request->parent_id);
            }
            else
            {
                $expense = Expense::where('client_expense.id',$request->expense_id)->where('client_expense.user_id',$request->user_id);
            }
			$expense = $expense->select(['client_expense.*','users.email','users.first_name','users.last_name','users.company_logo','users.address','users.country','users.vat as users_vat','users.company_name as company_name','users.company_vat_no as  company_vat_no','users.address_line1','users.address_line2','users.city','users.zipcode','users.contact_no'])->leftJoin('users','users.id','client_expense.user_id')->with('expenseItem')->get();
			
            if($expense->count() <= 0){
                $code = 200;
                $messages = 'Expense Not Found';
                $status = true;
            } 
            else
            {
                foreach ($expense as $exp) {
                    $category = ExpenseCategory::where('id',$exp->category_id)->first();
                    $exp->category_name = $category->title;
                    if($exp->client_id != 0){
                        $client = Client::where('id',$exp->client_id)->first();
                        if($client){
                            $exp->client_first_name = $client->first_name;
                            $exp->client_last_name = $client->last_name;
                            $exp->client_company_name = $client->company_name;
                            $exp->client_email = $client->email;
                            $exp->client_city = $client->city;
                        } 
                        else {
                            $exp->client_first_name = '';
                            $exp->client_last_name = '';
                            $exp->client_company_name = '';
                            $exp->client_email = '';
							$exp->client_city = '';
                        }
                    } 
                    else {
                        $exp->client_first_name = $user->first_name;
                        $exp->client_last_name = $user->last_name;
                        $exp->client_company_name = "Personal Cost";
                        $exp->client_email = $user->email;
                    }
					
					//PDF::loadView('invoice.expensePdf',['invoice' => $exp],[],['format' => 'A4','display_mode' => 'fullpage','default_font'=>'Poppins'])->stream('result.pdf')->save(public_path().'/uploads/invoice/pdf/'.str_replace("#","",$request->expense_id).".pdf");
					
					PDF::loadView('invoice.expensePdf',['invoice' => $exp],[],['format' => 'A4','display_mode' => 'fullpage'])->save(public_path().'/uploads/invoice/pdf/'.str_replace("#","",$exp->id).".pdf");
					$exp->url =  \URL::to('').'/uploads/invoice/pdf/'.str_replace("#","",$exp->id).".pdf";
                    $data[] = $exp;
                }
                
            }
    
			
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' => $notification_count]);
        exit;
		
		
		//PDF::loadView('invoice.expensePdf',['invoice' => $invoice],[],['format' => 'A4','display_mode' => 'fullpage'])->save(public_path().'/uploads/invoice/pdf/'.str_replace("#","",$invoice->invoice_id).".pdf");
	}
    public function store(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Expense added Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'category_id' => 'required',
            'title' => 'required',
            //'desc' => 'required',
            'net_amount' => 'required',
            'vat' => 'required',
            'upload' => 'mimes:jpg,jpeg,png',
            'date_time' => 'required',
			'expence_item' => 'required',
			//'currency_code' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);


            $category = ExpenseCategory::where('id',$request->category_id)->first();

            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
            if(request()->has('client_id') && !empty(request()->client_id)){
                $client = Client::where('id',$request->client_id)->where('user_id',$request->user_id)->first();
                if(!$client){
                    $message = 'Client not exist';
                    $code = 400;
                    $status = false;
                    return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
                }
            }

            if(request()->has('invoice_id') && !empty(request()->invoice_id)){
                $invoice = Invoice::where('id',$request->invoice_id)->first();
                if(!$invoice){
                    $message = 'Invoice not exist';
                    $code = 400;
                    $status = false;
                    return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
                }
            }

            if(!$category){
                $message = 'Category not exist';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
			$item_lists = json_decode($request->expence_item);
			if(count($item_lists) > 0){
				if(count($item_lists) == 1){
					if($item_lists[0]->qty == 0){
						$message = 'Item does not have 0 qty.';
						$code = 400;
						$status = false;
						return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
						exit;
					}
				}
				$expense = new Expense();
				$expense->user_id = $request->user_id;
				$expense->client_id = (request()->has('client_id') && !empty(request()->client_id))?$request->client_id:0;
				$expense->invoice_id = (request()->has('invoice_id') && !empty(request()->invoice_id))?$request->invoice_id:0;
				$expense->category_id = $request->category_id;
				$expense->title = $request->title;
				$expense->desc = $request->desc;
				$expense->net_amount = $request->net_amount;
				$expense->vat = $request->vat;
				$expense->currency_code = ($request->currency_code)?$request->currency_code:'GBP';
				$vat = round(($request->net_amount*$request->vat)/100,2);
				$expense->total_amount = $request->net_amount+$vat;
				$expense->status = 1;
				$expense->date_time = date('Y-m-d h:i:s',strtotime(str_replace('/', '-', $request->date_time)));

				$name = str_replace(' ','_',$request->title);

				if($request->file('upload') && request()->has('upload')){
					$image = $request->file('upload');

					$filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

					$image->move('uploads/expense', $filename);

					$expense->upload = 'uploads/expense/'.$filename;
				}

				$expense->save();
				$expense->upload = ($request->file('upload'))?url('/').'/'.$expense->upload:null;
				$expense->expense_id = $expense->id;
				$ItemData = array();
				foreach($item_lists as $key => $item){
					$ItemData[$key]['expence_item'] = $item->item_title;
					$ItemData[$key]['qty'] = $item->qty;
					$ItemData[$key]['item_price'] = $item->item_price;
					$ItemData[$key]['item_sub_price'] = $item->item_sub_total;
					$ItemData[$key]['unit'] = (isset($item->unit))? $item->unit : '';
					$ItemData[$key]['expence_id'] = $expense->id;
					$ItemData[$key]['created_at'] = date('y-m-d h:i:s');
				}
				ExpenceItem::insert($ItemData);
				$data = $expense;
			}else{
				$message = 'Expense not have any Item, Please add atleast one item.';
				$code = 400;
				$status = false;
				return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
				exit;
			}
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function edit(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = true;
        $error = '';

        $expense = Expense::where('id',$request->expense_id)
                        ->where('user_id',$request->user_id)
                        ->where('client_id',$request->client_id)
                        ->where('invoice_id',$request->invoice_id)
                        ->first();

        if(empty($expense)){
            $code = 404;
            $messages = 'Expense not exist';
            $status = false;
        }

        return response()->json(['result' => $expense, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function update(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Expense updated Successfully';
        $status = true;
        $error = '';

        $rules = array(
            'invoice_image' => 'mimes:jpg,jpeg,png',
            'user_id' => 'required',
            'expense_id' => 'required'
			
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            $expense = Expense::where('id',$request->expense_id)
                            ->where('user_id',$request->user_id)
                            ->first();

            if(!$expense){
                $message = 'Expense not found';
                $code = 400;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
			$item_lists = json_decode($request->expence_item);
			if(isset($request->expence_item) && count($item_lists) > 0){
				if(count($item_lists) == 1){
					if($item_lists[0]->qty == 0){
						$message = 'Item does not have 0 qty.';
						$code = 400;
						$status = false;
						return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
						exit;
					}
				}
				$expense->client_id = (request()->has('client_id') && $expense->client_id != $request->client_id)?$request->client_id:$expense->client_id;
				$expense->category_id = ($request->category_id)?$request->category_id:$expense->category_id;
				$expense->title = ($request->title)?$request->title:$expense->title;
				$expense->desc = ($request->desc)?$request->desc:$expense->desc;
				$expense->net_amount = ($request->net_amount)?$request->net_amount:$expense->net_amount;
				$expense->vat = (isset($request->vat))?$request->vat:$expense->vat;
				$expense->currency_code = ($request->currency_code)?$request->currency_code:'GBP';
				$vat = round(($expense->net_amount*$expense->vat)/100,2);
				$expense->total_amount = $vat+$expense->net_amount;
				$expense->date_time = ($request->date_time)?date('Y-m-d h:i:s',strtotime(str_replace('/', '-', $request->date_time))):$expense->date_time;

				$name = str_replace(' ','_',($request->title)?$request->title:$expense->title);
				
				if($request->file('upload') != null){
					if(file_exists($expense->upload)){
						unlink($expense->upload); //delete previously uploaded image
					}
					$image = $request->file('upload');

					$filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

					$image->move('uploads/expense', $filename);

					$expense->upload = 'uploads/expense/'.$filename;
				}
				$expense->save();
				// $expense->update($data);
				$expense->upload = url('/').'/'.$expense->upload;
				$expense->expense_id = $expense->id;
				ExpenceItem::where('expence_id',$expense->id)->delete();
				$ItemData = array();
				foreach($item_lists as $key => $item){
					$ItemData[$key]['expence_item'] = $item->item_title;
					$ItemData[$key]['qty'] = $item->qty;
					$ItemData[$key]['unit'] = (isset($item->unit))? $item->unit : '';
					$ItemData[$key]['item_price'] = $item->item_price;
					$ItemData[$key]['item_sub_price'] = $item->item_sub_total;
					$ItemData[$key]['expence_id'] = $expense->id;
					$ItemData[$key]['created_at'] = date('y-m-d h:i:s');
				}
				ExpenceItem::insert($ItemData);
				$data = $expense;
			}else{
				$message = 'Please add Atleast one item.';
                $code = 400;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
			}
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function delete(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Expense Deleted Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'expense_id' => 'required',
            'api_token' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['status'=>$status,'message'=>$message,'code'=> $code,'result'=>$data]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $expense = Expense::where('id',$request->expense_id)->where('user_id',$request->user_id)->first();
            if(empty($expense)){
                $message = 'Expense not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
                exit;
            }
            $expense->delete();
        }
        return response()->json([ 'code' => $code, 'message' => $messages, 'status' => $status,'result' => $data]);
        exit;
    }

    public function details(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';
        $notification_count=get_notification_count(request()->user_id);
        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required', 
            'guest' => 'numeric',
            'expense_id' => 'required',
           
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) 
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        }
        else
        {
            $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();
            if(!$user)
            {
                $message = 'Not valid user';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }
            if(isset($request->guest) && $request->guest == 1)
            {
                $expense = Expense::where('id',$request->expense_id)->where('user_id',$request->parent_id);
            }
            else
            {
                $expense = Expense::where('id',$request->expense_id)->where('user_id',$request->user_id);
            }
			$expense = $expense->with('expenseItem')->get();

            if($expense->count() <= 0){
                $code = 200;
                $messages = 'Expense Not Found';
                $status = true;
            } 
            else
            {
                foreach ($expense as $exp) {
                    $category = ExpenseCategory::where('id',$exp->category_id)->first();
                    $exp->category_name = $category->title;
                    if($exp->client_id != 0){
                        $client = Client::where('id',$exp->client_id)->first();
                        if($client){
                            $exp->client_first_name = $client->first_name;
                            $exp->client_last_name = $client->last_name;
                            $exp->client_company_name = $client->company_name;
                            $exp->client_email = $client->email;
                        } 
                        else {
                            $exp->client_first_name = '';
                            $exp->client_last_name = '';
                            $exp->client_company_name = '';
                            $exp->client_email = '';
                        }
                    } 
                    else {
                        $exp->client_first_name = $user->first_name;
                        $exp->client_last_name = $user->last_name;
                        $exp->client_company_name = "Personal Cost";
                        $exp->client_email = $user->email;
                    }

                    $data[] = $exp;
                }
                

            }
           
            
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' => $notification_count]);
        exit;
        }


   }