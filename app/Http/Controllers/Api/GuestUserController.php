<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use App\User;
use App\GuestUser;
use App\Certificate;
use Carbon\Carbon as Carbon;
use DB;

class GuestUserController extends Controller{

	public function sendInvitation(Request $request){
		
		$data = array();
        $code = 200;
        $messages = 'Invitation sent successfully.';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'email' => 'required',
            'api_token' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
            $code = 200;
        }
        else {
            $user = User::where("id", $request->user_id)->first();

            if($user->id != $request->user_id || $user->api_token != $request->api_token){
                $messages = 'Token not valid';
                $code = 200;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$messages,'code'=> $code]);
            }

            if ($user == null) {
                $messages = 'User not Found';
				$status = false;
				$code=200;
            }
            else{
                $guestUser = User::where("email", $request->email)->first();
              
				$guestUserInvitation['parentId'] = $request->user_id;
				$guestUserInvitation['child_email'] = $request->email;
				if($guestUser){
                    if($guestUser->id == $request->user_id)
                    {
                        $messages = 'Do not send invitation your self.';
                        $code = 200;
                        $status = 'false';
                        return response()->json(['result'=>$data,'status'=>$status,'message'=>$messages,'code'=> $code]);
                    }
				   $guestUserInvitation['childId'] = $guestUser->id;
				}
				$guestAlreadyExists = GuestUser::where($guestUserInvitation)->first();
				if($guestAlreadyExists == null){
					$guest = GuestUser::create($guestUserInvitation);
					$guest->email = $request->email;
					$guest->name = $user->first_name.' '.$user->last_name;
					$guest->notify(new \App\Notifications\InviteGuestUser($guest));
					$data = $guest;
				}else{
					$guestAlreadyExists->notify(new \App\Notifications\InviteGuestUser($guestAlreadyExists));
					$data = $guestAlreadyExists;
				}
			}
        }
		return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
	}
	
	public function updateInvitationStatus(Request $request){
		
		$data = array();
        $code = 200;
        $messages = 'Invitation Status update successfully.';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'invitation_id' =>'required',
			'api_token'=>'required',
			'status' => 'required',
		);
		if($request->status != 'revoke'){
			$rules['email'] = 'required';
		}
	
        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
            $code = 200;
        }
        else {
            $user = User::where("id", $request->user_id)->first();

            if($user->id != $request->user_id || $user->api_token != $request->api_token){
                $messages = 'Token not valid';
                $code = 200;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$messages,'code'=> $code]);
            }

            if ($user == null) {
                $messages = 'User not Found';
				$status = false;
				$code=200;
            }
            else{
				$guestUserInvitation['id'] = $request->invitation_id;
				if($request->status != 'revoke'){
					$guestUserInvitation['child_email'] = $request->email;
					$guestUserCheck = GuestUser::where($guestUserInvitation)->with('parentUsers','childUser')->first();
					if($guestUserCheck == null){
						$messages = 'You can not accept this invitation!';
						$status = false;
						$code=200;
					}else{
						$guestUserCheck->status = $request->status;
						$guestUserCheck->childId = $request->user_id;
						$guestUserCheck->read_status = 'read';
						$guestUserCheck->update();
						if($request->status == 'accepted'){
							$guestUserCheck->email = $guestUserCheck->parentUsers->email;
							$guestUserCheck->notify(new \App\Notifications\UpdateGuestUser($guestUserCheck));
						}
					}
					
				}else{
					$guestUserInvitation['parentId'] = $request->user_id;
					$guestUserCheck = GuestUser::where($guestUserInvitation)->first();
					$guestUserCheck->status = $request->status;
					$guestUserCheck->update();
				}
			}	
		}
		return response()->json(['result' => $guestUserCheck, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }
    public function getList(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';
        $notification_count=get_notification_count(request()->user_id);

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required',
			
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
            $code = 200;
        }

        $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

        if(!$user){
            $message = 'Not valid user';
            $code = 200;
            $status = false;
            return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
        }
        
        $guest=GuestUser::leftjoin('users','users.id','guest_users.childId')
                ->where('guest_users.parentId',request()->user_id)
                ->where('guest_users.status','!=','revoke')
				->select('users.first_name','users.last_name','guest_users.child_email','guest_users.status','guest_users.id as invitation_id',DB::raw('(CASE WHEN users.profile_pic != "" THEN CONCAT("'.url('/').'","/",users.profile_pic) ELSE "'.url('/').'/uploads/user/avatar.jpg'.'" END) AS image'))->orderby('guest_users.id','desc')->get();
       
       
        if(empty($guest)){
            $code = 200;
            $messages = 'Failed';
            $status = false;
        }

        return response()->json(['result' => $guest, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' =>$notification_count]);
        exit;

    }

    public function acceptUserList(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';
        $notification_count=get_notification_count(request()->user_id);

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
            $code = 200;
        }

        $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

        if(!$user){
            $message = 'Not valid user';
            $code = 200;
            $status = false;
            return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
        }
        
        $guest=GuestUser::leftjoin('users','users.id','guest_users.parentId')
                ->where('guest_users.status','accepted')
                ->where('guest_users.childId',request()->user_id)
                ->select('users.first_name','users.last_name','users.email','guest_users.parentId',DB::raw('(CASE WHEN users.profile_pic != "" THEN CONCAT("'.url('/').'","/",users.profile_pic) ELSE "" END) AS image'),'guest_users.id as invitation_id')->get();

        if(empty($guest)){
            $code = 200;
            $messages = 'Failed';
            $status = false;
        }

        return response()->json(['result' => $guest, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' =>$notification_count]);
        exit;
    }

    public function notificationList(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = true;
        $error = '';
        $notification_count=get_notification_count(request()->user_id);

        $rules = array(
            'user_id' => 'required',
            'api_token' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
            $code = 200;
        }

        $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

        if(!$user){
            $message = 'Not valid user';
            $code = 200;
            $status = false;
            return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'notification_count' =>$notification_count]);
        }
        
        $notification=GuestUser::leftjoin('users','users.id','guest_users.parentId')
                ->where('guest_users.read_status','unread')
                ->where('guest_users.childId',request()->user_id)
                ->select('users.first_name','users.last_name','users.email',DB::raw('(CASE WHEN users.profile_pic != "" THEN CONCAT("'.url('/').'","/",users.profile_pic) ELSE "" END) AS image'),'guest_users.id as invitation_id')->get();

       

        if(empty($notification)){
            $code = 200;
            $messages = 'Failed';
            $status = false;
        }

        return response()->json(['result' => $notification, 'code' => $code, 'message' => $messages, 'status' => $status,'notification_count' =>$notification_count]);
        exit;
    }
	
}