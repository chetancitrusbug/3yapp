<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Client;
use App\User;
use DB;
use Illuminate\Http\Request;
use Session;

class ClientController extends Controller
{
    public function __construct(){
        view()->share('moduleName', 'Clients');
        view()->share('route', 'client');
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */

    public function index(Request $request)
    {
        return view('admin.client.index');
    }

    public function datatable(Request $request)
    {
        // $client = Client::with(['user'])->get();
        $client = Client::select(['users.name as user_name','client.*'])
                    ->leftJoin('users',function($join){
                        $join->on('users.id','=','client.user_id');
                    })
                    ->orderBy('client.id','DESC')
                    ->get();

        return datatables()->of($client)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $users = User::where('status','1')->pluck('name','id')->toArray();
        view()->share('users', $users);

        return view('admin.client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');

        $this->validateData($request);

        if($request->file('image')){
            $name = str_replace(' ','_',$request->client_name);

            $image = $request->file('image');

            $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

            $image->move('uploads/client', $filename);

            $input['image'] = 'uploads/client/'.$filename;
        }

        $client = Client::create($input);

        Session::flash('flash_success', 'Client added!');
        return redirect('admin/clients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $client = Client::select(['users.name as user_name','client.*'])
                    ->leftJoin('users',function($join){
                        $join->on('users.id','=','client.user_id');
                    })
                    ->where('client.id',$id)
                    ->first();

        if ($client) {
            return view('admin.client.show', compact('client'));
        } else {
            Session::flash('flash_message', 'User is not exist!');
            // return redirect('admin/users');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$client = Client::where('id',$id)->first();
        if ($client) {
            $users = User::where('status','1')->pluck('name','id')->toArray();
            view()->share('users', $users);

            return view('admin.client.edit', compact('client'));
        } else {
            Session::flash('flash_warning', 'User is not exist!');
            return redirect('admin/clients');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $input = $request->except(['_token','email','user_id']);

        $this->validateData($request,$id);

        $client = Client::where('id',$id)->first();

        if($request->file('image')){
            if(file_exists($client->image)){
                unlink($client->image); //delete previously uploaded image
            }

            $name = str_replace(' ','_',$request->client_name);

            $image = $request->file('image');

            $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

            $image->move('uploads/client', $filename);

            $input['image'] = 'uploads/client/'.$filename;
        }

        $client->update($input);

        Session::flash('flash_success', 'Client updated!');
        return redirect('admin/clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $client = Client::find($id);
        if(file_exists($client->image) && $client->image != 'uploads/client/avatar.jpg'){
            unlink($client->image); //delete previously uploaded image
        }

        $client->delete();

        if($request->has('from_index')){
            $message = "Client Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Client deleted!');

            return redirect('admin/users');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'client_name' => 'required',
            'user_id' => 'required',
            'phone' => 'required|numeric',
        ];

        if($request->has('image')){
            $rules['image'] = 'mimes:jpg,jpeg,png';

        }

        if($id == 0){
            $rules['email'] = ['required','email',function($attribute, $value, $fail) use($request) {
                            $user = Client::where('email',$request->email)->where('user_id',$request->user_id)->first();
                            if ($user) {
                                $fail('Client already exist');
                            }
                        }];
        }

        return $this->validate($request, $rules,[
            'user_id.required' => 'The user field is required'
        ]);
    }
}
