<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Client;
use App\Invoice;
use App\User;
use DB;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon as Carbon;

class InvoiceController extends Controller
{
    public function __construct(){
        view()->share('moduleName', 'Invoice');
        view()->share('route', 'invoice');
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */

    public function index(Request $request)
    {
        return view('admin.invoice.index');
    }

    public function datatable(Request $request)
    {
        // $invoice = Invoice::with(['user'])->get();
        $invoice = Invoice::select([
                            'users.name as user_name',
                            'client.client_name',
                            'invoice.*',
                    ])
                    ->leftJoin('client',function($join){
                        $join->on('client.id','=','invoice.client_id');
                    })
                    ->leftJoin('users',function($join){
                        $join->on('users.id','=','invoice.user_id');
                    })
                    ->orderBy('client.id','DESC')
                    ->get();

        return datatables()->of($invoice)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        $users = User::where('status','1')->pluck('name','id')->toArray();
        view()->share('users', $users);

        $client = [];
        if(old('user_id',null)){
            $client = $this->clientList($request,['user_id'=>old('user_id',null)]);
        }
        view()->share('client', $client);

        return view('admin.invoice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');

        $this->validateData($request);

        if($request->file('invoice_image')){
            $name = str_replace(' ','_',$request->subject);

            $image = $request->file('invoice_image');

            $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

            $image->move('uploads/invoice', $filename);

            $input['invoice_image'] = 'uploads/invoice/'.$filename;
        }
        $input['status'] = 'in_progress';

        $vat = round(($input['net_amount']*$input['vat'])/100,2);
        $input['total_amount'] = $vat+$input['net_amount'];

        if($input['date_time']){
            $input['date_time'] = Carbon::parse($input['date_time'])->format('Y-m-d H:i:s');
        }

        $invoice = Invoice::create($input);
        Invoice::where('id',$invoice->id)->update(['invoice_id'=>'#'.str_pad($invoice->id, 10, "0", STR_PAD_LEFT)]);

        Session::flash('flash_success', 'Invoice added!');
        return redirect('admin/invoice');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $invoice = Invoice::select(['users.name as user_name','client.*'])
                    ->leftJoin('users',function($join){
                        $join->on('users.id','=','client.user_id');
                    })
                    ->where('client.id',$id)
                    ->first();

        if ($invoice) {
            return view('admin.invoice.show', compact('client'));
        } else {
            Session::flash('flash_message', 'User is not exist!');
            return redirect('admin/invoice');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        $invoice = Invoice::where('id',$id)->first();

        if ($invoice) {
            $users = User::where('status','1')->pluck('name','id')->toArray();
            view()->share('users', $users);

            if(old('user_id',$invoice->user_id)){
                $client = $this->clientList(request(),['user_id'=>old('user_id',$invoice->user_id)]);
            }
            view()->share('client', $client);

            return view('admin.invoice.edit', compact('invoice'));
        } else {
            Session::flash('flash_warning', 'Invoice is not exist!');
            return redirect('admin/invoice');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $input = $request->except(['_token','user_id']);
        $input['status'] = (isset($request->status) && $request->status == '1')?'confirm':'in_progress';
        // echo '<pre>'; print_r($input); exit;
        $this->validateData($request,$id);

        $invoice = Invoice::where('id',$id)->first();

        if($request->file('invoice_image')){
            if(file_exists($invoice->invoice_image)){
                unlink($invoice->invoice_image); //delete previously uploaded image
            }

            $name = str_replace(' ','_',$request->subject);

            $image = $request->file('invoice_image');

            $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

            $image->move('uploads/invoice', $filename);

            $input['invoice_image'] = 'uploads/invoice/'.$filename;
        }
        $vat = round(($input['net_amount']*$input['vat'])/100,2);
        $input['total_amount'] = $vat+$input['net_amount'];

        if($input['date_time']){
            $input['date_time'] = Carbon::parse($input['date_time'])->format('Y-m-d H:i:s');
        }

        $invoice->update($input);

        Session::flash('flash_success', 'Invoice updated!');
        return redirect('admin/invoice');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $invoice = Invoice::find($id);

        if(file_exists($invoice->invoice_image) && $invoice->invoice_image != 'uploads/invoice/avatar.jpg'){
            unlink($invoice->invoice_image); //delete previously uploaded image
        }

        $invoice->delete();

        if($request->has('from_index')){
            $message = "Invoice Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Invoice deleted!');

            return redirect('admin/users');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'user_id' => 'required',
            'client_id' => 'required',
            'subject' => 'required',
            'net_amount' => 'required|numeric',
            'vat' => 'required|numeric',
            'date_time' => 'required|date|date_format:d-m-Y H:i:s',
            'invoice_image' => 'mimes:jpg,jpeg,png',
        ];

        return $this->validate($request, $rules,[
            'user_id.required' => 'The user field is required',
            'client_id.required' => 'The client field is required',
            'net_amount.required' => 'The amount field is required',
            'net_amount.numeric' => 'The amount field must be numeric',
        ]);
    }

    public function clientList(Request $request,$user_id=0)
    {
        return Client::where("status","1")->where('user_id',$request->get('user_id',$user_id))->pluck('client_name','id')->toArray();
    }
}
