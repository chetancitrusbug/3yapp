<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Country;
use App\Role;
use App\User;
use App\Certificate;
use App\Client;
use DB;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon as Carbon;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function __construct(){
        view()->share('moduleName', 'Users');
        view()->share('route', 'user');
    }

    public function index(Request $request)
    {
        return view('admin.users.index');
    }

    public function datatable(Request $request)
    {
        $users = User::all();

        return datatables()->of($users)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $countries = Country::pluck('name','id')->toArray();
        view()->share('countries', $countries);

        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rule = [
            'user_type' => 'required|in:user,guest',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'name' => 'required',
            'country' => 'required',
            // 'company_name' => 'required',
            'birth_date' => 'required|date',
            'contact_no' => 'required|numeric',
            'country'=> 'required|integer',
            'profile_pic'=> 'mimes:jpg,jpeg,png',
        ];

        $this->validate($request, $rule);

        $data = $request->except(['password','insurance_certificates']);

        if($data['birth_date']){
            $date1 = Carbon::now();
            $date2 = Carbon::parse($data['birth_date']);

            if($date1->diffInYears($date2) < 18){
                $data['is_eligible'] = 0;
            }else{
                $data['is_eligible'] = 1;
            }

            $data['birth_date'] = Carbon::parse($data['birth_date'])->format('Y-m-d');
        }

        $name = str_replace(' ','_',$request->name);
        if($request->file('profile_pic')){

            $image = $request->file('profile_pic');

            $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

            $image->move('uploads/user', $filename);

            $data['profile_pic'] = 'uploads/user/'.$filename;
        }else{
            $data['profile_pic'] = 'uploads/user/avatar.jpg';
        }

        $data['password'] = bcrypt($request->password);

        if($request->has('is_register')){
            $data['status'] = 0;
        }

        $data['referral_code'] = md5(uniqid());
        $data['activation_token'] = sha1(time() . uniqid() . $data['email']);
        $user = User::create($data);

        if($request->file('insurance_certificates')){
            foreach ($request->file('insurance_certificates') as $key => $value) {
                # code...
                $file = [];
                $filename = $name.'_'. uniqid(time()) . '.' . $value->getClientOriginalExtension();
                $value->move('uploads/user/certificates', $filename);
                $file['name'] = 'uploads/user/certificates/'.$filename;
                $file['status'] = 1;
                $file['user_id'] = $user->id;
                $certificate = Certificate::create($file);
            }
        }

        $user->assignRole('USER');

        Session::flash('flash_success', 'User added!');
        return redirect('admin/users');
    }

    public function afterRegister($user)
    {
        $this->sendEmail($user);
    }

    public function sendEmail(User $user)
    {

        return $user->notify(new  \App\Notifications\ActivationLink($user));
    }

    public function activateAccount($token)
    {

        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            Session::flash('flash_error', 'This link is expired.');
            return redirect('/');
        }
        elseif ($user->is_active == 1) {
            Session::flash('flash_success', 'Your account have already activated.Please login!');
            return redirect('/login');
        }

        $user->activation_token = null;
        $user->activation_time = null;
        $user->is_active = 1;
        $user->save();


        if (!$this->guard()->check()) {
           // $this->guard()->login($user);
        }
		// Auth::loginUsingId($user->id);
       return redirect('/login')->with('flash_success', 'Your account has been activated.');
       // return redirect()->intended($this->redirectPath())

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $user = User::where('id',$id)->first();

        if ($user) {
            return view('admin.users.show', compact('user'));
        } else {
            Session::flash('flash_message', 'User is not exist!');
            return redirect('admin/users');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$user = User::where('id',$id)->first();
        if ($user) {
            $countries = Country::pluck('name','id')->toArray();
            view()->share('countries', $countries);

            return view('admin.users.edit', compact('user'));
        } else {
            Session::flash('flash_warning', 'User is not exist!');
            return redirect('admin/users');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'user_type' => 'required|in:user,guest',
            'name' => 'required',
            'country' => 'required',
            'birth_date' => 'required|date',
            'contact_no' => 'required|numeric',
            'country'=> 'required|integer',
            'profile_pic'=> 'mimes:jpg,jpeg,png',
        ]);

        $requestData = $request->except(['email','insurance_certificates']);

        $user = User::where('id',$id)->first();

        $name = str_replace(' ','_',$request->name);
        if($request->file('profile_pic')){
            if(file_exists($user->profile_pic)){
                unlink($user->profile_pic); //delete previously uploaded image
            }

            $image = $request->file('profile_pic');

            $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

            $image->move('uploads/user', $filename);

            $requestData['profile_pic'] = 'uploads/user/'.$filename;
        }

        if($request->file('insurance_certificates')){

            $oldCertificate = Certificate::where('user_id',$id)->get();

            if(count($oldCertificate)){
                foreach ($oldCertificate as $key => $value) {
                    if(file_exists($value->name)){
                        unlink($value->name); //delete previously uploaded certificate
                    }
                }
            }

            Certificate::where('user_id',$id)->delete();

            foreach ($request->file('insurance_certificates') as $key => $value) {
                # code...
                $file = [];
                $filename = $name.'_'. uniqid(time()) . '.' . $value->getClientOriginalExtension();
                $value->move('uploads/user/certificates', $filename);
                $file['name'] = 'uploads/user/certificates/'.$filename;
                $file['status'] = 1;
                $file['user_id'] = $id;
                $certificate = Certificate::create($file);
            }
        }

        if($requestData['birth_date']){
            $date1 = Carbon::now();
            $date2 = Carbon::parse($requestData['birth_date']);

            if($date1->diffInYears($date2) < 18){
                $requestData['is_eligible'] = 0;
            }else{
                $requestData['is_eligible'] = 1;
            }

            $requestData['birth_date'] = Carbon::parse($requestData['birth_date'])->format('Y-m-d');
        }
        $requestData['status'] = isset($requestData['status'])?1:0;
        $user->update($requestData);

        if($request->has('is_register')){
            Session::flash('flash_success', 'Profile Updated Successfully !');
            return redirect()->route('my-account');
        }
        Session::flash('flash_success', 'User updated!');
        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $user = User::find($id);
        //delete profile pic
        if(file_exists($user->profile_pic) && $user->profile_pic != 'uploads/user/avatar.jpg'){
            unlink($user->profile_pic); //delete previously uploaded image
        }

        //delete certificates
        $oldCertificate = Certificate::where('user_id',$id)->get();

        if(count($oldCertificate)){
            foreach ($oldCertificate as $key => $value) {
                if(file_exists($value->name)){
                    unlink($value->name); //delete previously uploaded certificate
                }
            }
        }

        Certificate::where('user_id',$id)->delete();

        //delete clients
        Client::where('user_id',$id)->delete();

        $user->roles()->sync([]);

        $user->delete();

        if($request->has('from_index')){
            $message = "User Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'User deleted!');

            return redirect('admin/users');
        }

    }

    public function changeStatus($table,$status,$id)
    {
        $status = ($status == 0)?1:0;

        if($table == 'invoice'){
            $status = ($status == 0)?'in_progress':'confirm';
        }

        DB::table($table)->where('id',$id)->update(['status'=>$status]);

        return response()->json(['message' => 'Status Updated'],200);
    }
}
