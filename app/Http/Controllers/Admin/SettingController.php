<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;
use Session;
use App\Rules\Unique;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.setting.index');
    }

    public function datatable(Request $request)
    {
        $setting = Setting::get();

        return datatables()->of($setting)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.setting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $validate = $this->validateData($request);

        $input['key'] = strtolower($request->get('name'));

        $setting = Setting::create($input);
        Session::flash('flash_success', 'Setting added!');

        return redirect('admin/setting');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        return redirect('admin/setting');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$setting = Setting::where('id',$id)->first();
        if ($setting) {
            return view('admin.setting.edit', compact('setting'));
        } else {
            Session::flash('flash_warning', 'Setting is not exist!');
            return redirect('admin/setting');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $input = $request->except(['_token','_method']);
        $validate = $this->validateData($request,$id);

        $input['status'] = isset($input['status'])?$input['status']:0;

        $setting = Setting::where('id',$id)->first();

        if($input['status'] != $setting->status){
            CarModel::where('brand_id',$id)->update(['status'=>$input['status']]);
            CarDetail::where('brand_id',$id)->update(['status'=>$input['status']]);
        }

        $setting->update(array_except($input,'old_logo'));

        Session::flash('flash_success', 'Setting updated!');

        return redirect('admin/setting');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Setting::whereId($id)->delete();

        $message = "Setting Deleted !!";

        return response()->json(['message' => $message],200);
    }

    public function validateData($request, $id = 0)
    {
        $validation = [
            'name' => ["required",new Unique('settings',$id)],
            'value' => 'required'
        ];

        return $this->validate($request, $validation);
    }
}
