<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Session;
use Illuminate\Support\Str;
use App\ForgotPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
/*
	protected function rules()
	{
		return [
			
			'password' => [
            'required',
            'string',
            'min:6',             // must be at least 10 characters in length
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[@$!%*#?&]/', // must contain a special character
			],
		];
	}
	
	protected function getResetValidationMessages()
    {
        return [
          'password.regex' => 'Password must contain at least 1 capital letter, a number and symbol.'
        ];
    }
*
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'password' => 'required|confirmed|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^a-zA-Z0-9]).+$/',
		], [
			'password.regex' => 'Password must contain at least 1 lower-case and capital letter, a number and symbol.'
		]);
	} */
	protected function resetPassword($user, $password)
	{	
		$user->forceFill([
			'password' => bcrypt($password),
			'remember_token' => Str::random(60),
		])->save();

		return redirect('/');
	}

    protected function sendResetResponse($response)
    {
        return redirect($this->redirectPath())
                            ->with([
                                'status', trans($response),
                                'flash_success' => 'Password reset.'
                            ]);
    }
	/*
	public function showResetPasswordForm($token)
    {	
		$data = array();
		
		$tokenNew = password_hash($token, PASSWORD_BCRYPT, ['cost' => '10']);
		dd($tokenNew);
 
		$data = ForgotPassword::where('token',$tokenNew)->get();
		dd($data);
		/*foreach($datas as $value){
			var_dump((Hash::check($value->token, $token)));
			if (Hash::check($value->token, $token)) {
				$data = $value;
			}
		}*
		exit;
        return view('auth.passwords.reset', compact('token','data'));
    } */
	
	
	public function changePassword(Request $request){
        /*if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        } *
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }*/
        $validatedData = $request->validate([
            'password' => 'required|string|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^a-zA-Z0-9]).+$/|confirmed',
        ],[
			'password.regex' => 'Password must contain at least 1 lower-case and capital letter, a number and symbol.'
		]);
        //Change Password
        $user = User::where('email',$request->get('email'))->first();
        $user->password = bcrypt($request->get('password'));
        $user->save();
		Session::flash('flash_success', "Password has been changed");
        return redirect('/');
    }
}
