<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Hash;
use App\ActionLog;
use Illuminate\Http\Request;
use Auth;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request)
    {
        $credentials = array(
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'status' => 1
        );

        $user = User::where("email", $request->email)->with('role')->where('status', 1)->first();
		if ($user) {
			if($user->role[0]->name == "USER"){
				Session::flash('flash_error', "Please login from application");
				return redirect('/');
			}
            if (Hash::check($request->password, $user->password)) {
				
                if (Auth::attempt($credentials)) {

                    Session::flash('flash_success', "Login Success !!");
                    if (Auth::user()->hasRole('SU')) {
                        return redirect('/admin');
                    }else{
                        return redirect('/');
                    }
				} else {
                    Session::flash('flash_error', "Your Account Has Not Activated Yet");
                    Session::flash('is_active_error', 'Yes');
                }

            } else {
                Session::flash('flash_error', "Invalid Username or Password");
            }
        } else {
            Session::flash('flash_error', "Invalid Username or Password");
        }


        return redirect()->back()->withInput();

    }

	
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('login');
    }
}
