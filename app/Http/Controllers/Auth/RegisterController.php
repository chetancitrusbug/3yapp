<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Country;
use Carbon\Carbon as Carbon;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }



    // public function showRegistrationForm()
    // {
    //     $location = Location::all()->pluck('name', 'id')->Prepend('Select Location','');

    //     return view('auth.register', compact('location'));
    // }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_type' => 'required|in:user,guest',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'name' => 'required',
            'country' => 'required',
            'birth_date' => 'required|date',
            'contact_no' => 'required|numeric',
            'country'=> 'required|integer'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if($data['birth_date']){
            $date1 = Carbon::now();
            $date2 = Carbon::parse($data['birth_date']);

            if($date1->diffInYears($date2) < 18){
                $data['is_eligible'] = 0;
            }else{
                $data['is_eligible'] = 1;
            }

            $data['birth_date'] = Carbon::parse($data['birth_date'])->format('Y-m-d');
        }

        $user = User::create([
            'user_type' => $data['user_type'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'country' => $data['country'],
            'contact_no' => $data['contact_no'],
            'birth_date' => $data['birth_date'],
            'is_eligible' => $data['is_eligible'],
            'company_name' => $data['company_name'],
            'profile_pic' => 'uploads/user/avatar.jpg',
            'status' => '1',
            'activation_token' => sha1(time() . uniqid() . $data['email']),
        ]);

        if (!$user->hasRole('USER')) {
            $user->assignRole('USER');
        }

        return $user;
    }

    public function activateAccount($token)
    {

        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            Session::flash('flash_error', 'This link is expired.');
            return redirect('/');
        }
        elseif ($user->is_active == 1) {
            Session::flash('flash_success', 'Your account have already activated.Please login!');
            return redirect('/login');
        }

        $user->activation_token = null;
        $user->status = 1;
        $user->save();

        if (!$this->guard()->check()) {
           // $this->guard()->login($user);
        }

        return redirect('/')->with('flash_success', 'Thank you for activating your 3Y account');

    }

    public function showRegistrationForm()
    {
        return redirect('login');
        $countries = Country::pluck('name','id')->toArray();
        view()->share('countries', $countries);

        return view('auth.register');
    }
}
