<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenceItem extends Model
{
     //
    protected $table = 'expence_items';

    protected $fillable = [
        'expence_id', 'expence_item','unit', 'qty','item_price','item_sub_price'
    ];
}
