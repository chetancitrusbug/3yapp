<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RateVehical extends Model
{
    //
	protected $table = 'rate_vehicals';

    protected $fillable = [
        'vehical_type','rate', 'status'
    ];
	

}
