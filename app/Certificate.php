<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    //
    protected $table = 'certificate';

    protected $fillable = [
        'user_id','name', 'status'
    ];
}
