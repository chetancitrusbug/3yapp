<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //
    protected $table = 'invoice';

    protected $fillable = [
        'invoice_id', 'user_id', 'client_id','subject','description','net_amount','vat','total_amount','invoice_image','date_time','status','due_date','is_invoice','receipt_payment_mode','payment_terms_id'
    ];

    public function getDateTimeAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
        }
        return $value;
    }
	
	public function invoiceItem(){
		return $this->hasMany('App\InvoiceItem','invoice_id');
		//return $this->hasMany('App\Comment', 'foreign_key');
	}
	
	public function paymentTerm(){
		return $this->hasOne('App\PaymentTerms','id','payment_terms_id');
	}
}
