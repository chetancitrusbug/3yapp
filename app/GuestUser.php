<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class GuestUser extends Model
{
	use Notifiable;
    protected $table = 'guest_users';

    protected $fillable = [
        'parentId', 'child_email','childId'
    ];

    public function parentUsers(){
		return $this->hasOne('App\User','id','parentId');
	}
	
	public function childUser(){
		return $this->hasOne('App\User','email','child_email');
	}
}
