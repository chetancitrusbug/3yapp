<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $table = 'client';

    protected $fillable = [
        'user_id','email', 'first_name','last_name', 'address','phone','image','status','city','postcode','notes','company_name','mobile','address_line1','address_line2'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','id');
    }
	
	public function guestuser(){
		return $this->belongsTo('App\GuestUser','email','child_email');
	}
}
