<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    //
    protected $table = 'schedule';

    protected $fillable = [
        'user_id','client_id', 'title', 'location','date','start_time','end_time','priority','status','end_date','all_day'

    ];

    public function user()
    {
        return $this->belongsTo('App\User','id');
    }
}
