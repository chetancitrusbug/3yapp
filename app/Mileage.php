<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mileage extends Model
{
    protected $table = 'mileages';

    protected $fillable = [
        'job_title','user_id', 'client_name', 'client_id','purspose','date','time','start_point_lat','start_point_long','dest_point_lat','dest_point_long','vehical_type','ret_per_mile','start_point_name','end_point_name','is_start','is_perpose','total_distance'
    ];
}
