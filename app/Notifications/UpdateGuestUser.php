<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UpdateGuestUser extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {	$name = $notifiable->childUser->first_name.' '.$notifiable->childUser->last_name;
        return (new MailMessage)
		->subject('3Yapp Invitation')
        ->greeting('Hello, ' )
        ->line('Your invitation has been accepted by '.$name)
        //->action('Please click to download link', url('activate-account/' . $notifiable->activation_token))
        ->action('Please click to download app',url('http://13.127.235.254/dev/laravel/3yapp/public/'))
        ->line('Thanks for joining 3y!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
