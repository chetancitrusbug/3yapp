<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
     //
    protected $table = 'invoice_item';

    protected $fillable = [
        'invoice_id', 'invoice_item', 'qty','item_price','item_sub_price','unit'
    ];

    
}
