<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleTitle extends Model
{
    protected $table = 'schedule_titles';

    protected $fillable = [
        'user_id', 'title', 'status'
    ];
}
