<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment_Instraction extends Model
{
    protected $table = 'payment__instractions';
     protected $fillable = [
        'user_id', 'bank_name','account_number','ifsc_code','branch','save_for_feature'
    ];
}
