<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1.0/', 'namespace' => 'Api'], function () {
    Route::post('user/login', 'UsersController@login');
    Route::post('user/register', 'UsersController@register');
    Route::post('user/forgotpassword', 'UsersController@forgotPassword');
    Route::get('countrylist', 'CountriesController@list');
    Route::get('userexist', 'UsersController@checkUser');
	Route::post('user/socialmedialogin', 'UsersController@socialMediaLogin');
});

Route::group(['prefix'=>'v1.0/','middleware'=>['api_pass'],'namespace'=>'Api'],function(){
	
    Route::post('user/profileupdate', 'UsersController@profileUpdate');
    Route::post('user/changepassword', 'UsersController@changePassword');
	Route::get('user/detail','UsersController@detail');
	//guest user
    Route::post('user/guest-user/send-invitation','GuestUserController@sendInvitation');
    Route::post('user/guest-user/list','GuestUserController@getList');
    Route::post('user/guest-user/accept-user-list','GuestUserController@acceptUserList');
    Route::post('user/guest-user/notification-list','GuestUserController@notificationList');
	Route::post('user/accept-invitation','GuestUserController@updateInvitationStatus');

    //client route
    Route::get('clientlist','ClientController@list');
    Route::post('clientstore','ClientController@store');
    Route::get('client','ClientController@edit');
    Route::post('clientupdate','ClientController@update');
    Route::post('clientdetails','ClientController@details');
    Route::get('delete-client','ClientController@delete');
    Route::post('clientexists','ClientController@ClientExists');
	Route::resources([
    'payment-terms' => 'PaymentTermsController',
	]);

    //invoice route
    Route::get('invoicelist','InvoiceController@list');
    Route::post('invoicestore','InvoiceController@store');
    Route::get('invoice','InvoiceController@edit');
    Route::post('invoicedetails','InvoiceController@details');
    Route::post('invoiceupdate','InvoiceController@update');
    Route::post('payment_status','InvoiceController@updatePayment');
    Route::get('invoicedelete','InvoiceController@delete');
    Route::get('invoicepdf','InvoiceController@InvoicePdf');
	

    //expense route
    Route::get('expenselist','ExpenseController@list');
    Route::post('expensestore','ExpenseController@store');
    Route::get('expense','ExpenseController@edit');
    Route::post('expensedetails','ExpenseController@details');
	Route::get('expense/view','ExpenseController@viewPdf');
    Route::post('expenseupdate','ExpenseController@update');
    Route::get('expensedelete','ExpenseController@delete');

    //Schedule route
    Route::post('schedule/add','ScheduleController@store');
    Route::get('schedule/list','ScheduleController@getSchedule');
    Route::post('schedule/update','ScheduleController@updateSchedule');
    Route::get('schedule/delete','ScheduleController@deleteSchedule');
	
	Route::get('schedule/titlelist','ScheduleController@getScheduleTitleList');
    Route::post('schedule/addTitle','ScheduleController@storeScheduleTitle');
    Route::post('schedule/details','ScheduleController@getDetails');
	Route::post('schedule/check','ScheduleController@checkSchedule');

		
    //  PaymentInstruction routes
    Route::post('payment_instruction/add','PaymentInstructionController@store');
    Route::get('payment_instruction/edit','PaymentInstructionController@editPaymentInstruction');
    Route::get('payment_instruction/list','PaymentInstructionController@getPaymentInstruction');
    Route::post('payment_instruction/update','PaymentInstructionController@updatePaymentInstruction');
    Route::get('payment_instruction/delete','PaymentInstructionController@deletePaymentInstruction');

    // Category routes
    Route::post('category/add','CategoryController@store');
    Route::get('category/edit','CategoryController@edit');
    Route::get('category/list','CategoryController@list');
    Route::post('category/update','CategoryController@update');
    Route::get('category/delete','CategoryController@delete');

    //Expense Category routes
    Route::post('expensecategory/add','ExpenseCategoryController@store');
    Route::get('expensecategory/edit','ExpenseCategoryController@edit');
    Route::get('expensecategory/list','ExpenseCategoryController@list');
    Route::post('expensecategory/update','ExpenseCategoryController@update');
    Route::get('expensecategory/delete','ExpenseCategoryController@delete');

    //Mileage routes
    Route::post('mileage/add','MileageController@store');
    Route::get('mileage/edit','MileageController@edit');
    Route::get('mileage/list','MileageController@list');
    Route::post('mileage/update','MileageController@update');
    Route::get('mileage/delete','MileageController@delete');
	
	//Notes routes
    Route::post('notes/add', 'NoteController@store');
    Route::get('notes/edit', 'NoteController@edit');
    Route::get('notes/list', 'NoteController@list');
    Route::post('notes/update', 'NoteController@update');
    Route::get( 'notes/delete', 'NoteController@delete');
	
	//Rate For Vehical
	Route::get('rate-vehical','MileageController@vehicalRate');
    //Dashboard route
    Route::get('bussnessprofit','InvoiceController@bussnessprofit');
	
	
	//Feedback
	Route::post('feedback','UsersController@feedback');

});
