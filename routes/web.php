<?php
use App\CarBrand;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Auth::routes();
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetPasswordForm')->name('password.reset');
Route::post('login', 'Auth\LoginController@authenticate')->name('login');
Route::post('/changePassword','Auth\ResetPasswordController@changePassword')->name('changePassword');
Route::get('/', 'HomeController@index')->name('/');

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/about-us', 'HomeController@aboutUs')->name('about-us');
Route::any('/contact', 'HomeController@contact')->name('contact');
Route::get('/terms-and-conditions', 'HomeController@termsAndConditions')->name('terms-and-conditions');
Route::get('/privacy-policy', 'HomeController@privacyPolicy')->name('privacy-policy');
Route::any('/contact-submit', 'HomeController@contactSubmit')->name('contact-submit');
Route::any('/myaccount', 'HomeController@myAccount')->name('my-account');

//user authentication
Route::post('/userLogin', 'LoginController@login')->name('userLogin');
Route::get('/userRegister', 'LoginController@register')->name('userRegister');
Route::post('/userRegister', 'Admin\UsersController@store')->name('registerStore');
Route::get('/userProfile', 'LoginController@userProfile')->name('user-profile');
Route::post('/updateUserProfile/{id}', 'Admin\UsersController@update')->name('user-profile-update');

Route::get('/profile/change-password', 'LoginController@changePassword')->name('front-change-password');
Route::post('/profile/change-password', 'LoginController@updatePassword')->name('front-password-update');


/////////////////////////////////////////////////// 3Yapp routes start
Route::get('/terms-and-conditions', 'HomeController@termsAndConditions')->name('terms-and-conditions');
Route::get('/privacy-policy', 'HomeController@privacyPolicy')->name('privacy-policy');

Route::get('/activate-account/{token}', 'Auth\RegisterController@activateAccount');

Route::group(['middleware' => ['auth', 'admin']], function () {

    Route::get('/home', 'HomeController@redirect');

    Route::group(['prefix' => 'admin' ], function () {

        Route::get('/', 'Admin\AdminController@index');

        //Role & Permissions
        Route::get('/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
        Route::post('/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');
        Route::resource('/roles', 'Admin\RolesController');
        Route::resource('/permissions', 'Admin\PermissionsController');

        //Users
        Route::get('/users-data', 'Admin\UsersController@datatable');
        Route::resource('/users', 'Admin\UsersController');

        //Client
        Route::get('/clients-data', 'Admin\ClientController@datatable');
        Route::resource('/clients', 'Admin\ClientController');

        //Invoice
        Route::get('/invoice-data', 'Admin\InvoiceController@datatable');
        Route::resource('/invoice', 'Admin\InvoiceController');
        Route::get('/client-list', 'Admin\InvoiceController@clientList');

        //Contact
        Route::resource('/contact', 'Admin\ContactController');
        Route::get('/contact-data', 'Admin\ContactController@datatable');

        //setting
        Route::resource('/setting', 'Admin\SettingController');
        Route::get('/setting-data', 'Admin\SettingController@datatable');


        //status change
        Route::get('/change-status/{table}/{status}/{id}', 'Admin\UsersController@changeStatus');

        //Profile
        Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
        Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
        Route::patch('/profile/edit', 'Admin\ProfileController@update');
        Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
        Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');
    });
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/profile', 'ProfileController@index');
    Route::get('/profile/change_password', 'ProfileController@change_password');
    Route::get('/profile/change_location', 'ProfileController@change_location');
});

Route::get('/db',function(){
    try {
        DB::connection()->getPdo();
        if(DB::connection()->getDatabaseName()){
            echo "Yes! Successfully connected to the DB: " . DB::connection()->getDatabaseName();
        }
    } catch (\Exception $e) {
        die("Could not connect to the database.  Please check your configuration.");
    }
});




Route::get('/home', 'HomeController@index')->name('home');
