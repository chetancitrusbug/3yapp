<?php

return [

    'users'=>[
        'status'=>[
                '0'=>'Inactive',
                '1'=>'Active'
        ],
        'user_type'=>[
                'user'=>'User',
                'guest'=>'Guest'
            ]
        ],

    'car_brand'=>[
        'status'=>[
                '0'=>'Inactive',
                '1'=>'Active'
            ]
        ],

    'car_model'=>[
        'status'=>[
                '0'=>'Inactive',
                '1'=>'Active'
            ]
        ],

    'car_engine'=>[
        'status'=>[
                '0'=>'Inactive',
                '1'=>'Active'
            ]
        ],

    'state_coach'=>[
        'status'=>[
                '0'=>'Inactive',
                '1'=>'Active'
            ]
        ],

    'abgasnorm'=>[
        'status'=>[
                '0'=>'Inactive',
                '1'=>'Active'
            ]
        ],

    'transmition'=>[
        'status'=>[
                '0'=>'Inactive',
                '1'=>'Active'
            ]
        ],

    'feature'=>[
        'status'=>[
                '0'=>'Inactive',
                '1'=>'Active'
            ]
        ],

    'fuel_type'=>[
        'status'=>[
                '0'=>'Inactive',
                '1'=>'Active'
            ]
        ],

    'car_detail'=>[
        'status'=>[
                '0'=>'Inactive',
                '1'=>'Active'
            ],

        'bid_status'=>[
            '0'=>'Bid Inactive',
            '1'=>'Bid Active'
            ],

        'gears'=>[
            '4'=>'4',
            '5'=>'5',
            '6'=>'6',
            '7'=>'7',
            '8'=>'8',
            '9'=>'9',
            ],

        'year'=>[
            'start'=>'1990',
            ],
        ],

    'car_bids'=>[
        'status'=>[
                '0'=>'Inactive',
                '1'=>'Active'
            ]
        ],

    'car_detail_sort'=>[
        '0'=>'Sort by',
        '1'=>'Name',
        '2'=>'Price',
        '3'=>'Engine',
        '4'=>'Year',
        '5'=>'Model',
    ],

    'price_filter'=>[
        '1'=> '<= 30000',
        '2'=> 'BETWEEN 30001 AND 50000',
        '3'=> 'BETWEEN 50001 AND 70000',
        '4'=> 'BETWEEN 70001 AND 90000',
        '5'=> '> 90000',
    ],

    'price_option'=>[
        '1'=>'Less than 30k',
        '2'=>'31k-50k',
        '3'=>'51k-70k',
        '4'=>'71k-90k',
        '5'=>'Greater Than 90k',
    ],
];
