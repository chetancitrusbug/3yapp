<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="background-color: #36bfa6;">
                        <a href="{{ $url }}" class="button button-green" target="_blank" style="color: #FFFFFF;background-color: #36bfa6;">{{ $slot }}</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
