@extends('layouts.app')

@section('title','LOGIN')

@section('content')
<div class="panel panel-default panel-border-color panel-border-color-primary">
    <div class="panel-heading app-background remove-margin">
        <img src="{{asset('backend/img/logo.png')}}" alt="Avatar" height="50px"><span class="span-title">3Yapp<span><span class="splash-description">Please enter your user information.</span>
    </div>
    <div class="panel-body">
        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            @csrf
            <div class="login-form">
                <div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
                    <input id="email" type="text" placeholder="Email" autocomplete="off" class="form-control input-sm" name="email" value="{{ old('email') }}">
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
                    <input id="password" type="password" placeholder="Password" name="password" class="form-control input-sm">
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group row login-tools">
                    <div class="col-xs-6 login-remember">
                        <div class="be-checkbox">
                            <input type="checkbox" id="remember" name="remember" {{ old( 'remember') ? 'checked' : '' }}>
                            <label for="remember">Remember Me</label>
                        </div>
                    </div>
                    <div class="col-xs-6 login-forgot-password"><a href="{{ route('password.request') }}">Forgot Password?</a></div>
                </div>
                <div class="form-group row login-submit">
                    {{-- <div class="col-xs-6">
                        <a href="{{route('register')}}" class="btn btn-default btn-xl">Register</a>
                    </div> --}}
                    <div class="col-xs-12">
                        <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl app-green app-dark-green">Sign in</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection


