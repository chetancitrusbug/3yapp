@extends('layouts.app')

@section('title','Register')

@section('content')
<div class="panel panel-default panel-border-color panel-border-color-primary">
    <div class="panel-heading app-background remove-margin">
        <img src="{{asset('backend/img/logo.png')}}" alt="Avatar" height="50px"><span class="span-title">3Yapp<span><span class="splash-description">Please enter your user information.</span>
    </div>
    <div class="panel-body">
        <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
            @csrf
            <div class="login-form">
                <div class="form-group {{ $errors->has('user_type') ? ' has-error' : ''}}">
                    {!! Form::select('user_type', [''=>'-- Select user type --']+config('constants.users.user_type'), null ,['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('user_type', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : ''}}">
                    <input id="name" type="text" placeholder="Name" autocomplete="off" class="form-control input-sm" name="name" value="{{ old('name') }}">
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
                    <input id="email" type="text" placeholder="Email" autocomplete="off" class="form-control input-sm" name="email" value="{{ old('email') }}">
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
                    <input id="password" type="password" placeholder="Password" name="password" class="form-control input-sm">
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {{ $errors->has('contact_no') ? ' has-error' : ''}}">
                    <input id="contact_no" type="text" placeholder="Contact No" autocomplete="off" class="form-control input-sm" name="contact_no" value="{{ old('contact_no') }}">
                    {!! $errors->first('contact_no', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {{ $errors->has('country') ? ' has-error' : ''}}">
                    {!! Form::select('country', [''=>'-- Select Country --']+$countries, null ,['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : ''}}">
                    <div data-min-view="2"  class="input-group date datetimepicker">
                        <input size="16" type="text" value="{{old('birth_date',isset($user)?$user->birth_date:'')}}" class="form-control input-sm" data-date-format="yyyy-mm-dd" name="birth_date"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                    </div>
                    {!! $errors->first('birth_date', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {{ $errors->has('company_name') ? ' has-error' : ''}}">
                    <input id="company_name" type="text" placeholder="Company Name" autocomplete="off" class="form-control input-sm" name="company_name" value="{{ old('company_name') }}">
                    {!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group row login-submit">
                    <div class="col-xs-6">
                        <a href="{{route('login')}}" class="btn btn-default btn-xl">Back to login</a>
                    </div>
                    <div class="col-xs-6">
                        <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Register</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection