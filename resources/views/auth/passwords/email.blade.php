@extends('layouts.app')

@section('title','FORGOT PASSWORD')

@section('content')
    <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading app-background remove-margin">
            <img src="{{asset('backend/img/logo.png')}}" alt="Avatar" height="50px"><span class="span-title">3Yapp<span><span class="splash-description">Forgot your password?</span>
        </div>
        @if (session('status'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="panel-body">
            <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                @csrf
                <p>Don't worry, we'll send you an email to reset your password.</p>
                <div class="form-group xs-pt-20 {{ $errors->has('email') ? ' has-error' : ''}}">
                    <input type="email" name="email" required="" placeholder="Your Email" autocomplete="off" class="form-control input-sm" value="{{ old('email') }}">
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group xs-pt-5">
                    <button type="submit" class="btn btn-block btn-primary btn-xl app-green app-dark-green">Reset Password</button>
                </div>
            </form>
        </div>
    </div>
@endsection


