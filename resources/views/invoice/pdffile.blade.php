<html>
    <head>
        <meta charset="utf-8">
        <title>Invoice</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
       <style>
       /* reset */
@import url('https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800');
*, html, body{

    border: 0;
    box-sizing: content-box;
    color: inherit;
    font-family: inherit;
    font-size: inherit;
    font-style: inherit;
    font-weight: inherit;
    line-height: inherit;
    list-style: none;
    margin: 0;
    padding: 0;
    text-decoration: none;
    vertical-align: top;
    font-family: 'Poppins', sans-serif !important;
}
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: normal;
  src: local('Poppins Regular'), local('Poppins-Regular'), url(Poppins-Regular.ttf) format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

    

</style> 
    </head>
    <body style="color: #000; background: #FFF; width: 842px; margin: 0 auto;  font-family: 'Poppins', sans-serif;">
		
		@php 
			$title = ($invoice->is_invoice) ? 'Receipt' : 'Invoice';
			$gstVat = ($invoice->country) ? 'VAT' : 'GST'; 
		@endphp
	
	
		<table style="padding:0; margin:0; font-size: 100%; border-spacing: 0; clear:both; table-layout: fixed; width: 100%;font-family: 'Poppins', sans-serif;">
			<tr>
				<td width="50%" style="text-align: left;  border-bottom: 1px solid #000; padding:0 0 20px 0;"><img alt="" src="{{public_path('/').$invoice->company_logo}}" width="100px"></td>
				
				<td width="20%" style="text-align: right;  border-bottom: 1px solid #000; padding:0 0 20px 0; font-family: 'Poppins', sans-serif;">
					<p style="margin-bottom: 5px;font-size:13px; margin:0; padding:0; display:block; font-family: 'Poppins', sans-serif; ">{!! ($invoice->contact_no != null)? $invoice->contact_no: '' !!}</p>
                                            <p style="margin-bottom: 5px;font-size:13px; margin:0; padding:0; display:block; font-family: 'Poppins', sans-serif;">{!! ($invoice->email != null)? $invoice->email: ''!!}</p>
                                            <p style="margin-bottom: 5px;font-size:13px; margin:0; padding:0; display:block; font-family: 'Poppins', sans-serif; ">{!! ($invoice->website != null)? $invoice->website: ''!!}</p>
				</td>
				<td width="30%" style="text-align: right;  border-bottom: 1px solid #000; padding:0 0 20px 0; font-family: 'Poppins', sans-serif;">
					<p style=" font-weight:bold; font-size:18px; margin:0; padding:0; display:block; font-family: 'Poppins', sans-serif;">{!! ($invoice->first_name != null)?  $invoice->first_name.' '.$invoice->last_name : ''!!}</p>
					<p style=" font-weight:bold; font-size:18px; margin:0; padding:0; display:block; font-family: 'Poppins', sans-serif;">{!! ($invoice->company_name != null)? $invoice->company_name: '' !!}</p>
					<p style="margin-bottom: 5px;font-size:13px; margin:0; padding:0; display:block; font-family: 'Poppins', sans-serif;">{!! ($invoice->address != null)? $invoice->address: ''!!}</p>
					<!--<p style="margin-bottom: 5px;font-size:13px; margin:0; padding:0; display:block; font-family: 'Poppins', sans-serif;">{!! ($invoice->city != null)? $invoice->city: ''!!}  - {!! ($invoice->postcode != null)? $invoice->postcode: ''!!}</p> -->
					<p style="margin-bottom: 5px;font-size:13px; margin:0; padding:0; display:block; font-family: 'Poppins', sans-serif;">{{$gstVat}} Number: {!! ($invoice->company_vat_no != null)? $invoice->company_vat_no: ''!!}</p>
				</td>
			</tr>
		</table>
		
		<table style="padding:0; margin:0; font-size: 100%; border-spacing: 0; clear:both; table-layout: fixed; width: 100%;font-family: 'Poppins', sans-serif;">
			<tr>
				<td style="height:10px;"></td>
			</tr>
			<tr>
				<td style="font-size: 18px; font-weight: bold;font-family: 'Poppins', sans-serif; display: block;text-align:center;height:20px;">{{$invoice->subject}}</td>
			</tr>
			<tr>
				<td style="height:20px;"></td>
			</tr>
		</table>
		
		
        <div style="padding: 0; margin: 0 0 20px 0; clear: both;font-family: 'Poppins', sans-serif; ">

                <table style="padding: 0px 0; font-size: 100%; table-layout: fixed; width: 100%;font-family: 'Poppins', sans-serif;">
                    <thead>
                        <tr>
                            <th style="width:33%; text-align: left; ">
							<p style="text-align: left; margin:0; padding:0;font-family: 'Poppins', sans-serif;">
							<span style="font-size: 18px; font-weight: 600;font-family: 'Poppins', sans-serif; display: block;">{{$title}} Date</span>
							<br><span style="font-size: 12px; padding-top: 5px; font-weight: normal; display: block; font-family: 'Poppins', sans-serif;">{!! ($invoice->date_time != null)? date('d-m-Y',strtotime($invoice->date_time)): 'N/A'!!}</span>
							</p></th>
							@if($invoice->is_invoice == 0)
							<th style="width:33%; text-align: left; ">
							<p style="text-align: left; margin:0; padding:0;font-family: 'Poppins', sans-serif;">
							<span style="font-size: 18px; font-weight: 600;font-family: 'Poppins', sans-serif; display: block;">Due Date</span>
							<br><span style="font-size: 12px; padding-top: 5px; font-weight: normal; display: block; font-family: 'Poppins', sans-serif;">{!! ($invoice->due_date != '0000-00-00')? date('d-m-Y',strtotime($invoice->due_date)): 'N/A'!!}</span>
							</p></th> 
							@endif
                            <th style="width:33%; text-align: right; font-family: 'Poppins', sans-serif;">
							<p style="text-align: left; font-size: 18px; font-weight: 600;font-family: 'Poppins', sans-serif; margin:0; padding:0; display:block;">{{$title}} Number</p>
							<p style="font-size: 18px; font-weight: 700; font-family: 'Poppins', sans-serif;display:block; margin:0; padding:0;">{!! ($invoice->invoice_id != null)? $invoice->invoice_id: ''!!}</p></th>
                        </tr>
                    </thead>
                </table>
            </div>
			
			
			<div style=" clear:both;background-color: #cee7ee; margin:0 0 20px 0; padding:0;">
				
				@if($invoice->invoice_status == "Draft")
                     <div style="padding:0; margin: 0 0 20px 0; background-color: #cee7ee; clear: both;">
                @elseif($invoice->invoice_status == "Paid")
                     <div style="padding: 0px 0 0 0; margin: 0 0 20px 0;  background-image: url({{url('/images/paid.png')}}); background-repeat: no-repeat;  background-size: 25%; background-position: center 10px; background-color: #cee7ee; clear: both;">
                @elseif($invoice->invoice_status == "Cancel")
                   <div style="padding: 0px 0 0 0; margin: 0 0 20px 0;  background-image: url({{url('/images/cancelled.png')}}); background-repeat: no-repeat; background-size: 25%; background-position: center 10px; background-color: #cee7ee; clear: both;">
                @elseif($invoice->invoice_status == "Sent")
                     <div style="padding: 0; margin: 0 0 20px 0; background-color: #cee7ee; clear: both;">
            @endif
				
					
				<table style="padding: 20px 20px 0 20px; margin:0; font-size: 100%; table-layout: fixed; width: 100%; position:relative; ">
                <thead>
                    <tr>
                        <th style="width:50%; text-align: left; font-size: 18px; font-weight: 600; font-family: 'Poppins', sans-serif;"><h4>Bill To:</h4></th>
                        <th style="width:50%; text-align: right; font-size: 18px; font-weight: 600; font-family: 'Poppins', sans-serif;"><h4>Payment Information:</h4></th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td style="width:50%; font-size: 14px; text-align: left; ">
                            <h3 style="font-size: 16px; font-weight: 500;">{!! ($invoice->client_fname != null)? $invoice->client_fname: ''!!}  {!! ($invoice->client_lname != null)? $invoice->client_lname: ''!!}</h3>
                            <p style="margin-bottom: 5px;font-size: 13px;">{!! ($invoice->client_address != null)? $invoice->client_address.',': ''!!}</p>
                            <p style="margin-bottom: 5px;font-size: 13px;">{!! ($invoice->client_address_line2 != null)? $invoice->client_address_line2.',': ''!!}</p>
                            <p style="margin-bottom: 5px;font-size: 13px;">{!! ($invoice->client_city != null)? $invoice->client_city: ''!!}-{!! ($invoice->client_postcode != null)? $invoice->client_postcode: ''!!}</p>
                            <p style="margin-bottom: 5px;font-size: 13px;">{!! ($invoice->client_phone != null)? $invoice->client_phone: ''!!}</p>
                            <p style="margin-bottom: 5px;font-size: 13px;">{!! ($invoice->client_email != null)? $invoice->client_email: ''!!}</p>
                        </td>

                        <td style="width:50%; font-size: 14px; text-align: right;">
						@if($invoice->is_invoice == 0)
                            @if($invoice->payment_mode == 0)
                            <p style="margin-bottom: 5px; font-size: 14px;">Payment Method: <strong style="font-weight: 700;font-size: 14px;">cash</strong></p>
                            @else
                                <p style="margin-bottom: 5px;font-size: 13px;">Payment Method: <strong style="font-weight: 700;">Bank</strong></p>
                                <p style="margin-bottom: 5px;font-size: 13px;">Bank Name: {!! ($invoice->bank_name != null)? $invoice->bank_name: '-'!!}</p>
                                <p style="margin-bottom: 5px;font-size: 13px;">Account Number: {!! ($invoice->account_number != null)? $invoice->account_number: '-'!!}</p>
                                <p style="margin-bottom: 5px;font-size: 13px;">Sort Code: {!! ($invoice->ifsc_code != null)? $invoice->ifsc_code: '-'!!}</p>
                            @endif  
						@else
							 <p style="margin-bottom: 5px; font-size: 14px;">Payment Method: <strong style="font-weight: 700;font-size: 14px;">{{$invoice->receipt_payment_mode}}</strong></p>
						@endif
                        </td>
                        
                    </tr>
                </tbody>
            </table>
				
			</div>
            
            
        </div>
           @php $currency_code = '$'; 
                            if($invoice->currency_code == 'GBP')
                                $currency_code = '£';
                      
                $vat=0;
                $vat=($invoice->vat * $invoice->net_amount)/100;
        @endphp
        <div style="clear: both;"> 
            <table style="margin: 0 0 10px 0;  Font-size: 13px;  width: 100%; border-spacing: 0px;table-layout: fixed; ">
                <thead style=" clear: both;">
                    <tr style="background-color: #d7d7d7; font-weight: normal; font-size: 13px; text-align: right;">
                        <th colspan="3" style="padding: 10px; text-align: left; font-size: 13px; width:40%; "><span >Items</span></th>
                        <th style="padding: 10px;font-size: 13px; text-align:right; width:30%;"><span >Unit Price ({!! ($invoice->currency_code != null)? $invoice->currency_code: ''!!})</span></th>
                        <th style="padding: 10px; text-align: center;font-size: 13px; width:10%;"><span >Qty.</span></th>
                        <th style="padding: 10px; text-align: center;font-size: 13px; width:10%;"><span >Unit</span></th>
                        <th style="padding: 10px; text-align:right;font-size: 13px; width:20%;"><span >Amount ({!! ($invoice->currency_code != null)? $invoice->currency_code: ''!!})</span></th>
                    </tr>
                </thead>
                <tbody style=" text-align: right;">
                     @foreach($invoice->invoiceItem as $item)
                    <tr style="">
                        <td colspan="3" style="padding: 10px; text-align: left;font-size: 13px;  border-bottom:1px solid #d7d7d7; "><span>{{$item->invoice_item}}</span></td>
                        <td style="padding: 10px; border-bottom:1px solid #d7d7d7;font-size: 13px; text-align: right"><span><span>{{$currency_code}} </span> {{number_format($item->item_sub_price,2)}}</span></td>
                        <td style="padding: 10px;border-bottom:1px solid #d7d7d7;font-size: 13px;  text-align: center"><span>{{$item->qty}}</span></td>
                        <td style="padding: 10px;border-bottom:1px solid #d7d7d7;font-size: 13px;  text-align: center"><span>{{($item->unit != '')? $item->unit : '-'}}</span></td>
                        <td style="padding: 10px;border-bottom:1px solid #d7d7d7;font-size: 13px; text-align:right;"><span>{{$currency_code}} </span><span>{{number_format($item->item_price,2)}}</span></td>
                    </tr>
                     @endforeach
                  
                </tbody>
            </table>
            
        </div>
        <div style="padding: 0; margin: 0 0 20px 0; background-color: #fff; clear: both;">

                <table style="font-size: 100%; table-layout: fixed; width: 100%;  ">
                  <tbody>
                        <tr>
							<td style="font-size: 16px; padding: 5px; font-weight: bold; text-align: right; width:45%;">&nbsp;</td>
							<td style="font-size: 16px; padding: 5px; font-weight: bold; text-align: right; width:30%;">Subtotal:</td>
							<td style="font-size: 16px; padding: 5px; font-weight: bold; text-align: right;width:25%;"><span>{{$currency_code}} </span>{{number_format($invoice->net_amount,2)}}</td>
                        </tr>
						<tr>
							<td style="font-size: 16px; padding: 5px; font-weight: bold; text-align: right; width:45%;">&nbsp;</td>
							<td style="font-size: 16px; padding: 5px; font-weight: bold; text-align: right; width:30%;">{{$gstVat}} ({{$invoice->vat}}%):</td>
							<td style="font-size: 16px; padding: 5px; font-weight: bold; text-align: right;width:25%;"><span>{{$currency_code}} </span>
							{{number_format($vat,2)}}</span></td>
						</tr>
                    </tbody>
                </table>
    
        </div> 
        <div style="padding: 0; margin: 0 0 20px 0; background-color: #cee7ee; clear: both;">

                    <table style=" font-size: 100%; table-layout: fixed; width: 100%; ">
                      <tbody>
						<tr>
							<td style="font-size: 16px; padding: 5px; font-weight: bold; text-align: right; width:45%;">&nbsp;</td>
							<td style="font-size: 16px; padding: 5px; font-weight: bold; text-align: right; width:30%;">Total:</td>
							<td style="font-size: 16px; padding: 5px; font-weight: bold; text-align: right;width:25%;"><span>{{$currency_code}} </span>{{number_format($invoice->total_amount,2)}}</span></td>
						</tr>
                        </tbody>
                    </table>
        
        </div>    
		@if($invoice->is_invoice == 0)
		<aside style="padding: 0 0 10px 0; font-size: 13px;">
                <div>
                    <h4 style=" font-weight: 600; font-size: 16px; margin:0; padding:0;">Payment Terms: {{$invoice->paymentTerm->title}} Days</h4>
                    <p style=" font-weight: 400; font-size: 12px; margin:0; padding:0;"></p>
                </div>
        </aside>
		<aside style="padding: 0 0 0 0; font-size: 13px;">
                <div>
                   <h4 style=" font-weight: 600; font-size: 14px; margin:0; padding:0;">Please quote your reference or Invoice number when making payment</h4>
                </div>
        </aside>
		<aside style="padding: 0 0 10px 0; font-size: 13px;">
                <div>
                    <p style=" font-weight: 400; font-size: 14px; margin:0; padding:0;">All goods remain the property of <strong>{!! ($invoice->company_name != null)? $invoice->company_name: $invoice->first_name.' '.$invoice->last_name !!}</strong> until full payment is received.</p>
                </div>
        </aside>
		@endif
    </body>
</html>