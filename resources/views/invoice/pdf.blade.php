<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<meta charset="utf-8">
<!------ Include the above in your HEAD tag ---------->
<style>
.invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
    			<h2>Invoice</h2><h3 class="pull-right">Invoice {{$invoice->invoice_id}}</h3>
    		</div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>Billed To:</strong><br>
						{{$invoice->first_name.' '.$invoice->last_name}} <br>
						{{$invoice->company_name}}
						{{$invoice->address}}<br>
    					{{$invoice->city}}<br>
    					{{$invoice->postcode}}
    				</address>
    			</div>
    			
    		</div>
    		<div class="row">
    			
    			<div class="col-xs-6">
    				<address>
    					<strong>Date:</strong><br>
    					{{date('d-m-Y',strtotime($invoice->date_time))}}<br><br>
    				</address>
					<address>
						<strong>{{$invoice->subject}}</strong><br>
    					{{$invoice->description}}<br><br>
    				</address>
    			</div>
    		</div>
			<div class="row">
    			
    			<div class="col-xs-6">
    				<address>
    					<strong>Payment Mode:</strong>
    					@if($invoice->payment_mode == 0)
							Cash<br>
						@else
							Bank<br>
							Bank Name:{{$invoice->bank_name}}<br>
							Account Number:{{$invoice->account_number}}<br>
							Sort Code:{{$invoice->ifsc_code}}<br>
							Branch:{{$invoice->branch}}<br>
						@endif
						<br><br>
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Invoice summary</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Item</strong></td>
        							<td class="text-center"><strong>Item Price</strong></td>
        							<td class="text-center"><strong>Qty</strong></td>
        							<td class="text-right"><strong>Totals</strong></td>
                                </tr>
    						</thead> 
    						<tbody>
							@php $currency_code = '$'; 
							if($invoice->currency_code == 'GBP')
								$currency_code = '£';
							@endphp
    							@foreach($invoice->invoiceItem as $item)
    							<tr>
    								<td>{{$item->invoice_item}}</td>
    								<td class="text-center">{{$currency_code}} {{$item->item_sub_price}}</td>
    								<td class="text-center">{{$item->qty}}</td>
    								<td class="text-right">{{$currency_code}} {{$item->item_price}}</td>
    							</tr>
                                
								@endforeach
							
								<tr>
    								<td class="thick-line" style="border-top: 2px solid;"></td>
									<td class="thick-line" style="border-top: 2px solid;"></td>
    								<td class="thick-line text-center" style="border-top: 2px solid;"><strong>Subtotal</strong></td>
    								<td class="thick-line text-right" style="border-top: 2px solid;">{{$currency_code}} {{$invoice->net_amount}}</td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
									<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Vat</strong></td>
    								<td class="no-line text-right">{{$invoice->vat}}%</td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
									<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right">{{$currency_code}} {{$invoice->total_amount}}</td>
    							</tr>
							</tbody>
    					</table>
					
    				</div>
				
    			</div>
    		</div>
			<div class="pull-right">
				@if($invoice->invoice_status == "Draft")
					<img src="{{public_path('frontend/stamps/draft_stamp_tn.jpg')}}" class="pull-right"/>
				@elseif($invoice->invoice_status == "Paid")
					<img src="{{public_path('frontend/stamps/paid_stamp_tn.jpg')}}" class="pull-right"/>
				@elseif($invoice->invoice_status == "Cancel")
					<img src="{{public_path('frontend/stamps/cancel_stamp_tn.jpg')}}" class="pull-right"/>
				@elseif($invoice->invoice_status == "Sent")
					<img src="{{public_path('frontend/stamps/sent_stamp_tn.jpg')}}" class="pull-right"/>
				@endif
			</div>
    	</div>
    </div>

					
    				
    		