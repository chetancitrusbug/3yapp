@extends('layouts.frontend')

@section('page-title','User Profile')

@section('title','User Profile')

@section('content')
    <div class="user-profile-div">
        <div class="container">
            <!-- Main content -->
            <section class="content">
                <div class="details-div">
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <p class="pl-50">First Name:</p>
                        </div>  
                        <div class="col-md-9 col-sm-8">
                            {{ $user->first_name }}
                        </div>  
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <p class="pl-50">Last Name:</p>
                        </div>  
                        <div class="col-md-9 col-sm-8">
                            {{ $user->last_name }}
                        </div>  
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <p class="pl-50">User Name:</p>
                        </div>  
                        <div class="col-md-9 col-sm-8">
                            {{ $user->user_name }}
                        </div>  
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <p class="pl-50">Email Address:</p>
                        </div>	
                        <div class="col-md-9 col-sm-8">
                            {{ $user->email }}
                        </div>	
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <p class="pl-50">Password:</p>
                        </div>	
                        <div class="col-md-9 col-sm-8">
                            <p>**********</p>
                            <a href="#" class="" id="pwd_modal" ><i class="fa fa-key"></i> change password</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <p class="pl-50">Mobile No:</p>
                        </div>  
                        <div class="col-md-9 col-sm-8">
                            {{ $user->mobile }}
                        </div>  
                    </div>
                </div>
            </section><!-- end of content section -->
        </div>	<!-- end of container -->
    </div>	<!-- end of user-profile-div -->
    <div class="mt-30 clearfix text-center"><a href="{{ url('/') }}" class=""><i class="fa fa-angle-left"></i> Back to Home page</a></div>

@include('profile.changepassword')

@endsection

@push('js')
<script>
    $(document).on('click', '#pwd_modal', function (e) {
        $('#myModal').modal('show');
    });

    function resetupdateform(formid) {
        $(formid)[0].reset();
        $('#opwd_error').html("");
        $('#npwd_error').html("");
        $('#cpwd_error').html("");
    }

    $("#pass_form").validate({
    
    submitHandler: function (form) {
        var old_password = $('#old_password').val();
        var new_password = $('#new_password').val();
        var confirm_password = $('#confirm_password').val();
        if(old_password == ""){
            document.getElementById("opwd_error").innerHTML = "Password field cannot be blank ";
            return false;
        }
        if(new_password == ""){
            document.getElementById("npwd_error").innerHTML = "New Password field cannot be blank ";
            return false;
        }
        if(new_password.length < 6 ){
            document.getElementById("npwd_error").innerHTML = "Password Must be Atleast Six Charachters Long ";
            return false;
        }
        if(confirm_password == ""){
            document.getElementById("cpwd_error").innerHTML = "Confirm Password field cannot be blank ";
            return false;
        }
        if(new_password != confirm_password){
            document.getElementById("npwd_error").innerHTML = "Password Confirm does not Match ";
            return false;
        }

        var url = "{{url('profile/change_password')}}";
        var method = "get";
        $.ajax({
            type: method,
            url: url,
            data: $(form).serialize(),
            beforeSend: function () {
            },
            success: function (result)
            {   
                toastr.success(result.message);
                $('#myModal').modal('hide');
                resetupdateform('#pass_form');
            },
            error: function (error) {
                toastr.error(error.responseJSON.message);
                $('#myModal').modal('hide');
                resetupdateform('#pass_form');
            }
        }); 
        return false;
    }
});

  $(document).on('click', '#loc_modal', function (e) {
        $('#locModal').modal('show');
    });


    $("#loc_form").validate({
    
    submitHandler: function (form) {
        
        var url = "{{url('profile/change_location')}}";
        var method = "get";
        $.ajax({
            type: method,
            url: url,
            data: $(form).serialize(),
            beforeSend: function () {
            },
            success: function (result)
            {   
                toastr.success(result.message);
                $('#locModal').modal('hide');
                resetupdateform('#loc_form');
                setTimeout("location.reload(true);", 2000);
            },
            error: function (error) {
                toastr.error(error.responseJSON.message);
                $('#locModal').modal('hide');
                resetupdateform('#loc_form');
            }
            
        }); 
        return false;
    }
});

</script>
@endpush