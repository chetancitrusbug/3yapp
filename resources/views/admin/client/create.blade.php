@extends('layouts.admin')
@section('title',"Create Clients")

@section('content')
<div class="col-md-12">
    <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Create New Client
            <span class="panel-subtitle">
                <a href="{{ url('/admin/clients') }}" title="Back">
                    <button class="btn btn-space btn-warning">Back</button>
                </a>
            </span>
        </div>
        <div class="panel-body">
            {!! Form::open(['url' => '/admin/clients', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                @include ('admin.client.form')

                <div class="form-group col-sm-4">
                    {!! Form::submit('Create', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection