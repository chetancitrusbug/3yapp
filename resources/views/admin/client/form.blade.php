<div class="form-group{{ $errors->has('user_id') ? ' has-error' : ''}}">
    {!! Form::label('user_id', '* Select User: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('user_id', [''=>'-- Select user --']+$users, old('user_id') ,['class' => 'form-control input-sm',isset($client)?'disabled':'']) !!}
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('client_name') ? ' has-error' : ''}}">
    {!! Form::label('client_name', '* Client Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('client_name', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('client_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(isset($client->email))
    <input type="hidden" name="user_id" value="{{$client->user_id}}">
    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
        {!! Form::label('email', '* Email: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('email', old('email',$client->email), ['class' => 'form-control input-sm','disabled' => 'disabled']) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@else
    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
        {!! Form::label('email', '* Email: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('email', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif
<div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}}">
    {!! Form::label('phone', '* Phone No: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('phone', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group logo {{ $errors->has('image') ? ' has-error' : ''}}">
    {!! Form::label('image', 'Image: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('image', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($client))
        <div class="logo-image"><img src="{{ asset($client->image) }}" class="changeImage"></div>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
    {!! Form::label('address', 'Address: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('address', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::checkbox('status',1, isset($client->status)?$client->status:1, ['class'=>['status']]) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@push('js')
    <script>
        $('#profile_pic').on('change',function(){
            $('.changeImage').hide();
        });
    </script>
@endpush