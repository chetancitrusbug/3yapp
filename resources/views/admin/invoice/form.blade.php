<div class="form-group{{ $errors->has('user_id') ? ' has-error' : ''}}">
    {!! Form::label('user_id', '* Select User: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('user_id', [''=>'-- Select user --']+$users, old('user_id') ,['class' => 'form-control input-sm',isset($invoice)?'disabled':'']) !!}
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('client_id') ? ' has-error' : ''}}">
    {!! Form::label('client_id', '* Select Client: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('client_id', [''=>'-- Select client --']+$client, old('client_id') ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('client_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('subject') ? ' has-error' : ''}}">
    {!! Form::label('subject', '* Subject: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('subject', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(isset($invoice))
    <input type="hidden" name="user_id" value="{{$invoice->user_id}}">
@endif

<div class="form-group{{ $errors->has('date_time') ? ' has-error' : ''}}">
    {!! Form::label('date_time', '* Date Time: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('date_time', old('date_time',isset($invoice)?$invoice->date_time:''), ['class' => 'form-control input-sm form_datetime']) !!}
        {!! $errors->first('date_time', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('net_amount') ? ' has-error' : ''}}">
    {!! Form::label('net_amount', '* Amount: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('net_amount', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('net_amount', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('vat') ? ' has-error' : ''}}">
    {!! Form::label('vat', '* Vat (%): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('vat', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('vat', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
    {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('description', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group logo {{ $errors->has('invoice_image') ? ' has-error' : ''}}">
    {!! Form::label('invoice_image', 'Invoice Image: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('invoice_image', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('invoice_image', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($invoice))
            <div class="logo-image"><img src="{{ asset($invoice->invoice_image) }}" class="changeImage"></div>
        @endif
    </div>
</div>

@if (isset($invoice->status))
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::checkbox('status',1, (isset($invoice->status) && $invoice->status == 'in_progress')?0:1, ['class'=>['status_invoice']]) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif

@push('js')
    <script>
        $('#profile_pic').on('change',function(){
            $('.changeImage').hide();
        });

        //model dependency on brand change
        $('select[name="user_id"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/admin/client-list') }}",
                data: {user_id: $(this).val()},
            })
            .done(function(data) {
                $('select[name="client_id"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="client_id"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
            });
        });
    </script>
@endpush