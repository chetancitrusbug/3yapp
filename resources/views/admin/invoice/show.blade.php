@extends('layouts.admin')
@section('title',"View Client")
@section('content')

<div class="row">
    <div class="user-info-list panel panel-default">
        <div class="panel-heading panel-heading-divider">Client # {{ $client->client_name }}
            <span class="panel-subtitle">
                <a href="{{ url('/admin/clients') }}" title="Back">
                    <button class="btn btn-space btn-warning">Back</button>
                </a>
            </span>
        </div>
        <div class="panel-body">
            <table class="no-border no-strip skills">
                <tbody class="no-border-x no-border-y">
                    <tr>
                        <td class="item">User Name</td>
                        <td>{{ $client->user_name }}</td>
                    </tr>
                    <tr>
                        <td class="item">Client Name</td>
                        <td>{{ $client->client_name }}</td>
                    </tr>
                    <tr>
                        <td class="item">Email</td>
                        <td>{{ $client->email }}</td>
                    </tr>
                    <tr>
                        <td class="item">Phone No</td>
                        <td>{{ $client->phone }}</td>
                    </tr>
                    <tr>
                        <td class="item">Address</td>
                        <td>{{ $client->address }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection