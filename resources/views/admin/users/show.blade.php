@extends('layouts.admin')
@section('title',"View User")
@section('content')

<div class="row">
    <div class="user-info-list panel panel-default">
        <div class="panel-heading panel-heading-divider">User # {{ $user->name }}
            <span class="panel-subtitle">
                <a href="{{ url('/admin/users') }}" title="Back">
                    <button class="btn btn-space btn-warning">Back</button>
                </a>
            </span>
        </div>
        <div class="panel-body">
            <table class="no-border no-strip skills">
                <tbody class="no-border-x no-border-y">
                    <tr>
                        <td class="item">Name</td>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <td class="item">Email</td>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td class="item">Contact</td>
                        <td>{{ $user->contact_no }}</td>
                    </tr>
                    <tr>
                        <td class="item">Birthdate</td>
                        <td>{{ $user->birth_date }}</td>
                    </tr>
                    <tr>
                        <td class="item">Company Name</td>
                        <td>{{ $user->company_name }}</td>
                    </tr>
                    <tr>
                        <td class="item">Company Tax No</td>
                        <td>{{ $user->company_tax_no }}</td>
                    </tr>
                    <tr>
                        <td class="item">Company Vat No</td>
                        <td>{{ $user->company_vat_no }}</td>
                    </tr>
                    <tr>
                        <td class="item">Website</td>
                        <td>{{ $user->website }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection