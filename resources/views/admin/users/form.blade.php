@push('css')
    <style>
        .btn-primary,.btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus {
                color: #fff !important;
                background-color: #2572f2 !important;
                border-color: transparent;
                border-top-color: #0c57d3 !important;
                box-shadow: inset 0 2px 0 #1266f1 !important;
            }
    </style>
@endpush
<div class="form-group{{ $errors->has('user_type') ? ' has-error' : ''}}">
    {!! Form::label('user_type', '* Select Usertype: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('user_type', [''=>'-- Select user type --']+config('constants.users.user_type'), null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('user_type', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', '* Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(isset($user->email))
    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
        {!! Form::label('email', '* Email: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('email', old('email',$user->email), ['class' => 'form-control input-sm','disabled' => 'disabled']) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@else
    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
        {!! Form::label('email', '* Email: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('email', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
        {!! Form::label('password', '* Password: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::password('password',  ['class' => 'form-control input-sm', 'required' => 'required']) !!}
            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif
<div class="form-group{{ $errors->has('contact_no') ? ' has-error' : ''}}">
    {!! Form::label('contact_no', '* Contact No: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('contact_no', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('contact_no', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group logo {{ $errors->has('profile_pic') ? ' has-error' : ''}}">
    {!! Form::label('profile_pic', 'Profile Pic: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('profile_pic', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('profile_pic', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($user))
        <div class="logo-image"><img src="{{ asset($user->profile_pic) }}" class="changeImage"></div>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('insurance_certificates') ? ' has-error' : ''}}">
    {!! Form::label('insurance_certificates', 'Insurance Certificates: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        <input type="file" name="insurance_certificates[]" multiple class="">
        {!! $errors->first('insurance_certificates','<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('birth_date') ? ' has-error' : ''}}">
    {!! Form::label('birth_date', '* Birth Date: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6 col-xs-12">
        <div data-min-view="2"  class="input-group date datetimepicker1">
            <input size="16" type="text" value="{{old('birth_date',isset($user)?$user->birth_date:null)}}" class="form-control input-sm" data-date-format="dd-mm-yyyy H:i" name="birth_date"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
        </div>
        {!! $errors->first('birth_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('country') ? ' has-error' : ''}}">
    {!! Form::label('country', '* Select Country: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('country', [''=>'-- Select Country --']+$countries, null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('company_name') ? ' has-error' : ''}}">
    {!! Form::label('company_name', 'Company Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('company_name', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('company_tax_no') ? ' has-error' : ''}}">
    {!! Form::label('company_tax_no', 'Company Tax No: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('company_tax_no', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('company_tax_no', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('company_vat_no') ? ' has-error' : ''}}">
    {!! Form::label('company_vat_no', 'Company Vat No: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('company_vat_no', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('company_vat_no', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('website') ? ' has-error' : ''}}">
    {!! Form::label('website', 'Website: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('website', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('website', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::checkbox('status',1, isset($user->status)?$user->status:1, ['class'=>['status']]) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@push('js')
    <script>
        $('#profile_pic').on('change',function(){
            $('.changeImage').hide();
        });
    </script>
@endpush