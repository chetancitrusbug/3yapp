@extends('layouts.admin')
@section('title',"Edit Users")
@push('css')
<style type="text/css">
    .logo {
        position: relative;
    }

    .logo-image {
        position: absolute;
        left: 65px;
        top: 0px;
    }

    img {
        height: 50px;
        width: 50px;
    }
</style>

@endpush
@section('content')
<div class="col-md-12">
    <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Edit User # <strong>{{$user->name}}</strong>
            <span class="panel-subtitle">
                <a href="{{ url('/admin/users') }}" title="Back">
                    <button class="btn btn-space btn-warning">Back</button>
                </a>
            </span>
        </div>
        <div class="panel-body">
           {!! Form::model($user, ['method' => 'PATCH','url' => ['/admin/users', $user->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                @include ('admin.users.form')

                <div class="form-group col-sm-4">
                    {!! Form::submit('Update', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection