<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', '* Name: ', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('value') ? ' has-error' : ''}}">
    {!! Form::label('value', '* value: ', ['class' => 'control-label']) !!}
    {!! Form::text('value', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('value', '<p class="help-block">:message</p>') !!}
</div>