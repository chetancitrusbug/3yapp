<nav class="navbar navbar-default navbar-fixed-top be-top-header">
    <div class="container-fluid">
        <div class="navbar-header"><a href="{{url('admin/')}}" class="navbar-brand"><img src="{{asset('backend/img/logo.png')}}" alt="Avatar" height="50px"><span class="span-title">3Yapp<span></a></div>
        <div class="be-right-navbar">
            <ul class="nav navbar-nav navbar-right be-user-nav">
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
                        @if (auth()->user()->profile_pic != '')
                            <img src="{{ asset(auth()->user()->profile_pic)}}" alt="{{auth()->user()->name}}">
                        @else
                            <img src="{{ asset('uploads/user/avatar.jpg')}}" alt="{{auth()->user()->name}}">
                        @endif
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li>
                            <div class="user-info">
                                <div class="user-name">{{auth()->user()->name}}</div>
                                <div class="user-position online">Available</div>
                            </div>
                        </li>
                        <li><a href="#"><span class="icon mdi mdi-face"></span> Account</a></li>
                        <li><a href="#"><span class="icon mdi mdi-settings"></span> Settings</a></li>
                        <li><a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="icon mdi mdi-power"></span> Logout</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </li>
            </ul>
            <div class="page-title"><span>{{ $moduleName or 'Dashboard'}}</span></div>
            <ul class="nav navbar-nav navbar-right be-icons-nav">
                {{-- setting icon --}}
                <li class="dropdown"><a href="#" role="button" aria-expanded="false"><span class="icon mdi mdi-settings"></span></a></li>

                {{-- notification bell icon --}}
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-notifications"></span><span class="indicator"></span></a>
                    <ul class="dropdown-menu be-notifications">
                        <li>
                            <div class="title">Notifications<span class="badge">3</span></div>
                            <div class="list">
                                <div class="be-scroller">
                                    <div class="content">
                                        <ul>
                                            <li class="notification notification-unread">
                                                <a href="#">
                                                    <div class="image"><img src="{{asset('backend/img/avatar2.png')}}" alt="Avatar"></div>
                                                    <div class="notification-info">
                                                        <div class="text"><span class="user-name">Jessica Caruso</span> accepted your invitation
                                                            to join the team.</div><span class="date">2 min ago</span>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="footer"> <a href="#">View all notifications</a></div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>