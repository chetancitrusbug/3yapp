<!--Default Alerts-->
@if (Session::has('flash_success'))
    <div role="alert" class="alert alert-success alert-dismissible">
        <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
        <span class="icon mdi mdi-check"></span>{{ Session::get('flash_success') }}
    </div>
@endif

@if (Session::has('flash_message'))
    <div role="alert" class="alert alert-primary alert-dismissible">
        <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
        <span class="icon mdi mdi-info-outline"></span>{{ Session::get('flash_message') }}
    </div>
@endif

@if (Session::has('flash_warning'))
    <div role="alert" class="alert alert-warning alert-dismissible">
        <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
        <span class="icon mdi mdi-alert-triangle"></span>{{ Session::get('flash_warning') }}
    </div>
@endif

@if (Session::has('flash_error'))
    <div role="alert" class="alert alert-danger alert-dismissible">
        <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
        <span class="icon mdi mdi-close-circle-o"></span>{{ Session::get('flash_error') }}
    </div>
@endif