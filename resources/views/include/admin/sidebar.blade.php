<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Data Tables</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">Menu</li>
                        <li class="{{($route == 'user')?'active':''}}"><a href="{{ url('admin/users') }}"><i class="icon mdi mdi-account"></i><span>Users</span></a></li>
                        <li class="{{($route == 'client')?'active':''}}"><a href="{{ url('admin/clients') }}"><i class="icon mdi mdi-account"></i><span>Client</span></a></li>
                        <li class="{{($route == 'invoice')?'active':''}}"><a href="{{ url('admin/invoice') }}"><i class="icon mdi mdi-account"></i><span>Invoice</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>