<div class="footer-wrapper scrollspy-footer">
	<footer class="secondary-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<p class="copy-right">&#169; Copyright 2018 Autosquare.</p>
				</div>
				<div class="col-sm-6">
					<ul class="secondary-footer-menu clearfix">
                        @if(auth()->check())
                            <li><a href="{{route('my-account')}}">My Account</a></li>
                        @else
                            <li><a href="#login" data-toggle="modal" data-target="#login">Sign-in</a></li>
                            <li><a href="{{route('userRegister')}}">Sign-up</a></li>
                        @endif
					</ul>
				</div>
			</div>
		</div>
	</footer>
</div>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	{{ csrf_field() }}
</form>