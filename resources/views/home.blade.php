<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>3Yapp</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Poppins', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .subtile {
                font-size: 25px;
                color:#fff;
            }
            .title {
                font-size: 50px;
                margin:0 auto;
                text-align: center;
                color: #fff;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            body{
                background-color:#36bfa6;
            }
            .border_bottom{ width: 250px;height: 3px; background-color: #fff;padding:0; text-align: center;margin: 20px auto;display: block; }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div style=" padding-bottom:50px;" class="panel-heading app-background remove-margin">
                    <img src="{{asset('backend/img/logo.png')}}" alt="Avatar" height="200px">
                </div>
                <div class="m-b-md subtile">
                    @if(Session::has('flash_success'))
						<div class="title m-b-md">
							{{(Session::get('flash_success')) }}
							<div class="border_bottom"></div>
						</div>
                       
                    @elseif(Session::has('flash_error'))
                         {{(Session::get('flash_error')) }}
                    @else
						Thanks For using application!
                    @endif
                </div>
                
            </div>
        </div>
    </body>
</html>
