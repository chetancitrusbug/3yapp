<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="assets/img/logo-fav.png">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- title --}}
        <title>@yield('title') {{ config('app.name') }}</title>

        <link rel="stylesheet" type="text/css" href="{{asset('backend/lib/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('backend/lib/material-design-icons/css/material-design-iconic-font.min.css')}}"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="{{asset('backend/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('backend/lib/jqvmap/jqvmap.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('backend/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('backend/css/style.css')}}" type="text/css"/>
        <link rel="stylesheet" href="{{asset('backend/css/your-style.css')}}" type="text/css"/>

        <link rel="stylesheet" type="text/css" href="{{asset('backend/lib/datatables/css/dataTables.bootstrap.min.css')}}" />
        @stack('css')
    </head>

    <body>
        <div id="loading">
            <img id="loading-image" src="{{asset('backend/loading.gif')}}" alt="Loading..." />
        </div>
        <div class="be-wrapper be-fixed-sidebar">

            @include('include.admin.top-nav-bar')

            @include('include.admin.sidebar')

            <div class="be-content">
                @include('include.admin.alerts')
                @include('include.admin.module_header')

                <div class="main-content container-fluid">
                    @yield('content')
                </div>
            </div>
        </div>

        @include('include.admin.models')

        <script src="{{asset('backend/lib/jquery/jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/js/main.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/lib/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/lib/datatables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/lib/datatables/js/dataTables.bootstrap.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/lib/datatables/plugins/buttons/js/dataTables.buttons.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/lib/datatables/plugins/buttons/js/buttons.html5.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/lib/datatables/plugins/buttons/js/buttons.flash.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/lib/datatables/plugins/buttons/js/buttons.print.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/lib/datatables/plugins/buttons/js/buttons.colVis.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/lib/datatables/plugins/buttons/js/buttons.bootstrap.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/js/app-tables-datatables.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/js/bootstrap-checkbox.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/js/app-form-elements.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/lib/jquery.niftymodals/dist/jquery.niftymodals.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/js/custom.js')}}" type="text/javascript"></script>

        <script type="text/javascript">
        $(".form_datetime").datetimepicker({
            autoclose: true,
            format: 'dd-mm-yyyy hh:ii:ss',
        });
        $(document).ready(function(){
            //initialize the javascript
            // App.init();
            // App.dashboard();
            setTimeout(function(){
                jQuery('.alert-dismissible').hide();
            }, 3000);

            $(".datetimepicker1").datetimepicker({
                autoclose: true,
                format: 'dd-mm-yyyy',
                setEndDate: '2018-09-08',
                componentIcon: '.mdi.mdi-calendar',
                navIcons:{
                    rightIcon: 'mdi mdi-chevron-right',
                    leftIcon: 'mdi mdi-chevron-left'
                }
            });

            $("#loading").hide();
        });


        </script>
        @stack('js')
    </body>
</html>