<!doctype html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="minimal-ui, width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="images/favicon.ico">
<title>3Y - Make Life Easy - Privacy Policy</title>


<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

<style>
html,body {margin:0 auto; padding:0; height:100%; }

/**/
html {
    text-rendering: optimizeLegibility;
    -webkit-text-size-adjust: 100%;
    -moz-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    text-size-adjust: 100%;
}

* {
    -webkit-overflow-scrolling: touch;
}
body {
/*	background:#fff;*/
	font-size:12px;
	color: #222;
	-webkit-font-smoothing: antialiased; /* Fix for webkit rendering */
	-webkit-text-size-adjust: 100%;
	/*-webkit-overflow-scrolling: touch;*/
	font-family: 'Raleway', sans-serif;
}
h2 { margin:0 0 15px 0; padding:0; font-size:20px; font-weight:bold; color:#222; line-height:18px;}
p { margin:0 0 15px 0; padding:0; font-size:14px; color:#222; line-height:18px; position:relative;}

ol{ margin:0 0 15px 15px; padding:0; font-size:14px; color:#222; line-height:18px; position:relative; }
ol li{ margin:0 0 15px 0; padding:0 0 0 15px; font-size:14px; color:#222; line-height:18px; position:relative; list-style:decimal ;}
ol li ol{ margin:0 0 15px 20px; padding:0; font-size:14px; color:#222; line-height:18px; position:relative; }

ul li { list-style:disc; }

.p-heading{ font-weight: 400!important;padding: 0px 0;line-height: 1.2; }
.p-heading:first-child{ padding: 15px 0 0 0; }


</style>

</head>
<body>

<div style=" margin:0; padding:40px 20px; position:relative;">

<h2>Privacy Notice</h2>
<p>This is the privacy notice of Tria Limited, the trading name for which is 3Y and Three-Y.   In this document, "we", "our", or "us" refer toTria Limited, and its trading names, 3Y and Three-Y</p>
<p>We are company number 11206576 registered in England and Wales.</p>
<p>Our registered office is at 10d Wincombe Business Park, Shaftesbury, SP7 9QJ, United Kingdom</p>
<p style="font-size:18px;font-weight:700;padding:10px 0 0 0;">Introduction</p>
<p style="line-height:1.4;">This privacy notice aims to inform you about how we collect and process any information that we collect from you, or that you provide to us. It covers information that could identify you (“personal information”) and information that could not. In the context of the law and this notice, “process” means collect, store, transfer, use or otherwise act on information. It tells you about your privacy rights and how the law protects you.</p>
<p style="line-height:1.4;">We are committed to protecting your privacy and the confidentiality of your personal information. Our policy is not just an exercise in complying with the law, but a continuation of our respect for you and your personal information.</p>
<p style="line-height:1.4;">We undertake to preserve the confidentiality of all information you provide to us and hope that you reciprocate.</p>
<p style="line-height:1.4;">Our policy complies with UK law accordingly implemented, including that required by the EU General Data Protection Regulation (GDPR).</p>
<p style="line-height:1.4;">Except as set out below, we do not share, or sell, or disclose to a third party, any information collected through our App.</p>

<ol>
	<li style="font-weight:bold;">Data we process</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:20px">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;">We may collect, use, store and transfer different kinds of personal data about you. We have collated these into groups as follows:</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;">Your identity includes information such as first name, last name, title, date of birth, and other identifiers that you may have provided at some time.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;">Your contact information includes information such as billing address, delivery address, email address, telephone numbers and any other information you have given to us for the purpose of communication or meeting.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;">Transaction data includes details about payments or communications to and from you and information about products and services you have purchased from us.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;">Technical data includes your internet protocol (IP) address, browser type and version, time zone setting and location, browser plug-in types and versions, operating system and platform and other technology on the devices you use to access this App.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;">Marketing data includes your preferences in receiving marketing from us; communication preferences; responses and actions in relation to your use of our services.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;">We may aggregate anonymous data such as statistical or demographic data for any purpose. Anonymous data is data thatdoes notidentify you as an individual. Aggregated data may be derived from your personal data but is not considered personal information in law because it does not reveal your identity.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;">For example, we may aggregate profile data to assess interest in a product or service.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;">However, if we combine or connect aggregated data with your personal information so that it can identify you in any way, we treat the combined data as personal information and it will be used in accordance with this privacy notice.</span>
		</p>
    </div>

    <li style="font-weight:bold;">Special personal information</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:20px">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Special personal information is data about your race or ethnicity, religious or philosophical beliefs, sex life, sexual orientation, political opinions, trade union membership, information about your health and genetic and biometric data.We may collect special personal information about youif there is a lawful basis on which to do so.</span>
		</p>
    </div>

    <li style="font-weight:bold;">If you do not provide personal information we need</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Where we need to collect personal data by law, or under the terms of a contract we have with you, and you fail to provide that data when requested, we may not be able to perform that contract. </span>
        </p>
        <h4 style="margin:0px 0 15px 0;">The bases on which we process information about you </h4>
        <p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">The law requires us to determine under which of six defined bases we process different categories of your personal information, and to notify you of the basis for each category. </span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">If a basis on which we process your personal information is no longer relevant, then we shall immediately stop processing your data. </span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">If the basis changes then if required by law, we shall notify you of the change and of any new basis under which we have determined that we can continue to process your information.</span>
        </p>
	</div>

    <li style="font-weight:bold;">Information we process because we have a contractual obligation with you</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">When you create an account on our App, buy a product or service from us, or otherwise agree to our terms and conditions, a contract is formed between you and us. </span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">In order to carry out our obligations under that contract we must process the information you give us. Some of this information may be personal information.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We may use it in order to:</span>
            <ul style="list-style-type:disc;">
                <li>verify your identity for security purposes</li>
                <li>sell products to you</li>
                <li>provide you with our services</li>
                <li>provide you with suggestions and advice on products, services and how to obtain the most from using our App</li>
            </ul>
        </p>
        <p style="margin-top:20px;">
            We shall continue to process this information until the contract between us ends or is terminated by either party under the terms of the contract.
        </p>
    </div>

    <li style="font-weight:bold;">Information we process with your consent</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Through certain actions when otherwise there is no contractual relationship between us, such as when you browse our App or ask us to provide you more information about our business, including our products and services, you provide your consent to us to process information that may be personal information. </span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">we may from time to time pass your name and contact information to selected associates whom we consider may provide services or products you would find useful.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">You may withdraw your consent at any time by instructing us via the App. However, if you do so, you may not be able to use our App or our services further.</span>
        </p>
    </div>

    <li style="font-weight:bold;">Information we process for the purposes of legitimate interests</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We may process information on the basis there is a legitimate interest, either to you or to us, of doing so. </span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Where we process your information on this basis, we do, after having given careful considerationto:</span>
            <ul style="list-style-type:disc;">
                <li>whether the same objective could be achieved through other means</li>
                <li>whether processing (or not processing) might cause you harm</li>
                <li>whether you would expect us to process your data, and whether you would, in the round, consider it reasonable to do so</li>
            </ul>

            <span style="position:relative; display:block; padding:10px 0 10px 0px;line-height:1.4;">For example, we may process your data on this basis for the purposes of:</span>
            <ul style="list-style-type:disc;">
                <li>record-keeping for the proper and necessary administration of our business.</li>
                <li>responding to unsolicited communication from you to which we believe you would expect a response</li>
                <li>protecting and asserting the legal rights of any party</li>
                <li>insuring against or obtaining professional advice that is required to manage businessrisk</li>
                <li>protecting your interests where we believe we have a duty to do so</li>
            </ul>
        </p>
    </div>

    <li style="font-weight:bold;">Information we process because we have a legal obligation</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Sometimes, we must process your information in order to comply with a statutory obligation. </span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">For example, we may be required to give information to legal authorities if they so request or if they have the proper authorisation such as a search warrant or court order.</span>
            <span style="position:relative; display:block; padding:0px 0 0px 0px;line-height:1.4;">This may include your personal information.</span>
            <h4 style="padding:0;margin:0;">Specific uses of information you provide to us</h4>
        </p>
    </div>

    <li style="font-weight:bold;">Information provided on the understanding that it will be shared with a third party</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Our App allows you to post information with a view to that information being read, copied, downloaded, or used by other people.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Examples include:</span>
            <ul style="list-style-type:disc;">
                <li>posting a message our forum</li>
                <li>posting a message to one of your clients</li>
                <li>tagging an image</li>
                <li>clicking on an icon next to another visitor’s message to convey your agreement, disagreement or thanks</li>
            </ul>
            <span style="position:relative; display:block; padding:10px 0 10px 0px;line-height:1.4;">We do not specifically use this information except to allow it to be displayed or shared as part of the App service.</span>
            <span style="position:relative; display:block; padding:10px 0 10px 0px;line-height:1.4;">We do store it, and we reserve a right to use it in the future in any way we decide.</span>
            <span style="position:relative; display:block; padding:10px 0 10px 0px;line-height:1.4;">Once your information enters the public domain, we have no control over what any individual third party may do with it. We accept no responsibility for their actions at any time.</span>
            <span style="position:relative; display:block; padding:10px 0 10px 0px;line-height:1.4;">Provided your request is reasonable and there is no legal basis for us to retain it.</span>
        </p>
    </div>

    <li style="font-weight:bold;">Complaints regarding content on our App</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Our App is a publishing medium. Anyone may register and then publish information about himself, herself or some other person.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We do not moderate or control what is posted.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">If we feel it is justified or if we believe the law requires us to do so, we shall remove the content while we investigate.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Free speech is a fundamental right, so we have to make a judgment as to whose right will be obstructed: yours, or that of the person who posted the content that offends you.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">If we think your complaint is vexatious or without any basis, we shall not correspond with you about it.</span>
        </p>
    </div>

    <li style="font-weight:bold;">Information relating to your method of payment</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Payment information is never taken by us or transferred to us either through our App or otherwise. Our employees and contractors never have access to it.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">At the point of payment, you are transferred to a secure page on the website of (payment provider to be confirmed) or some other reputable payment service provider. It is not controlled by us</span>
        </p>
    </div>

    <li style="font-weight:bold;">Communicating with us</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">When you contact us via our App or by e-mail, we collect the data you have given to us in order to reply with the information you need.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We record your request and our reply in order to increase the efficiency of our business.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We keep personally identifiable information associated with your message, such as your name and email address so as to be able to track our communications with you to provide a high quality service.</span>
        </p>
    </div>

    <li style="font-weight:bold;">Complaining</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">When we receive a complaint, we record all the information you have given to us.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We use that information to resolve your complaint.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">If your complaint reasonably requires us to contact some other person, we may decide to give to that other person some of the information contained in your complaint. We do this as infrequently as possible, but it is a matter for our sole discretion as to whether we do give information, and if we do, what that information is.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We may also compile statistics showing information obtained from this source to assess the level of service we provide, but not in a way that could identify you or any other person.</span>
        </p>
    </div>

    <li style="font-weight:bold;">Affiliate and business partner information</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">This is information given to us by you in your capacity as an affiliate of us or as a business partner.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Where applicable, it allows us to recognise App users that you have referred to us, and to credit to you commission due for such referrals. It also includes information that allows us to transfer commission to you.  This is at our absolute discretion.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">The information is not used for any other purpose.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We undertake to preserve the confidentiality of the information and of the terms of our relationship. </span>
            <span style="position:relative; display:block; padding:0px 0 0px 0px;line-height:1.4;">We expect any affiliate or partner to agree to reciprocate this policy. </span>
            <h4 style="padding:0;margin:0;">Use of information we collect through automated systems when you visit our App.</h4>
        </p>
    </div>

    <li style="font-weight:bold;">Cookies and tracking tools</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We may use cookies, web beacons, tracking pixels, and other tracking technologies on our App to help us and improve your experience. Your web browser should allow you to delete any you choose. It also should allow you to prevent or limit their use.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Our App uses cookies. They are placed by software that operates on our servers, and by software operated by third parties whose services we use.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">If you choose not to use cookies or you prevent their use through your browser settings, you will not be able to use all the functionality of our App.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We use cookies in the following ways:</span>

            <ul style="list-style-type:disc;">
                <li>to track how you use our app</li>
                <li>to record whether you have seen specific messages we display on our app</li>
                <li>to keep you signed in our app</li>
                <li>to record your answers to surveys and questionnaires on our appwhile you complete them</li>
                <li>to record the conversation thread during a live chat with our support team</li>
            </ul>
            <span style="position:relative; display:block; padding:10px 0 10px 0px;line-height:1.4;">We do not specifically use this information except to allow it to be displayed or shared as part of the App service.</span>
            <span style="position:relative; display:block; padding:10px 0 10px 0px;line-height:1.4;">We do store it, and we reserve a right to use it in the future in any way we decide.</span>
            <span style="position:relative; display:block; padding:10px 0 10px 0px;line-height:1.4;">Once your information enters the public domain, we have no control over what any individual third party may do with it. We accept no responsibility for their actions at any time.</span>
            <span style="position:relative; display:block; padding:10px 0 10px 0px;line-height:1.4;">Provided your request is reasonable and there is no legal basis for us to retain it.</span>
        </p>
    </div>

    <li style="font-weight:bold;">Our use of re-marketing</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Re-marketing involves placing a cookie on your device when you download our app in order to be able to serve to you an advert for our products or services when you visit some other web pages.</span>
            <span style="position:relative; display:block; padding:0px 0 0px 0px;line-height:1.4;">We may use a third party to provide us with re-marketing services from time to time. If so, then if you have consented to our use of cookies, you may see advertisements for our products and services on other web pages.</span>
            <h4 style="padding:0;margin:0;">Disclosure and sharing of your information</h4>
        </p>
    </div>

    <li style="font-weight:bold;">Information we obtain from third parties</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Although we do not disclose your personal information to any third party (except as set out in this notice), we sometimes receive data that is indirectly made up from your personal information from third parties whose services we use.</span>
        </p>
    </div>

    <li style="font-weight:bold;">Credit reference</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">To assist in combating fraud, we share information with credit reference agencies, so far as it relates to clients or customers who instruct their credit card issuer to cancel payment to us without having first provided an acceptable reason to us and given us the opportunity to refund their money.</span>
        </p>
    </div>

    <li style="font-weight:bold;">Data may be processed outside the European Union</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We may also use outsourced services in countries outside the European Union from time to time in other aspects of our business.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Accordingly, data obtained within the UK or any other country could be processed outside the European Union.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We use the following safeguards with respect to data transferred outside the European Union:</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">the processor is within the same corporate group as our business or organisation and abides by the same binding corporate rules regarding data processing.Control over your own information</span>
        </p>
    </div>

    <li style="font-weight:bold;">Your duty to inform us of changes</li>

    <li style="font-weight:bold;">Access to your personal information</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">At any time, you may review or update personally identifiable information that we hold about you, by signing in to your account on our App.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">To obtain a copy of any information that is not provided on App you shouldcontact us via the App to make that request.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">After receiving the request, we will tell you when we expect to provide you with the information, and whether we require any fee for providing it to you.</span>
        </p>
    </div>

    <li style="font-weight:bold;">Removal of your information</li>

    <li style="font-weight:bold;">Verification of your information</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 0px 0px;line-height:1.4;">When we receive any request to access, edit or delete personal identifiable information we shall first take reasonable steps to verify your identity before granting you access or otherwise taking any action. This is important to safeguard your information.</span>
            <h4 style="padding:0;margin:0;">Other matters</h4>
        </p>
    </div>

    <li style="font-weight:bold;">Use of site by children</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We do not sell products or provide services for purchase by children, nor do we market to children</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We collect data about all users of and visitors to these areas regardless of age, and we anticipate that some of those users and visitors will be children.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Such child users and visitors will inevitably visit other parts of the site and will be subject to whatever on-site marketing they find, wherever they visit.</span>
        </p>
    </div>

    <li style="font-weight:bold;">Encryption of data sent between us</li>


    <li style="font-weight:bold;">How you can complain</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">If you are not happy with our privacy policy or if you have any complaint, then you should tell us via our App.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">If a dispute is not settled, then we hope you will agree to attempt to resolve it by engaging in good faith with us in a process of mediation or arbitration. </span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">If you are in any way dissatisfied about how we process your personal information, you have a right to lodge a complaint with the Information Commissioner's Office (ICO).</span>
        </p>
    </div>

    <li style="font-weight:bold;">Retention period for personal data</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Except as otherwise mentioned in this privacy notice, we keep your personal information only for as long as required by us:</span>

            <ul style="list-style-type:disc;">
                <li>to provide you with the services you have requested;</li>
                <li>to comply with other law, including for the period demanded by our tax authorities;</li>
                <li>to support a claim or defence in court.	</li>
            </ul>
        </p>
    </div>

    <li style="font-weight:bold;">Compliance with the law</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">Our privacy policy has been compiled so as to comply with the law of every country or legal jurisdiction in which we aim to do business. If you think it fails to satisfy the law of your jurisdiction, we should like to hear from you.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">However, ultimately it is your choice as to whether you wish to use our App.</span>
        </p>
    </div>

    <li style="font-weight:bold;">Review of this privacy policy</li>
	<div style="list-style:none;padding-left:15px;">
		<p style="margin-top:0px;">
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">We may update this privacy notice from time to time as necessary. The terms that apply to you are those posted here on our App on the day you use our App. We advise you to print a copy for your records.</span>
            <span style="position:relative; display:block; padding:0px 0 10px 0px;line-height:1.4;">If you have any question regarding our privacy policy, please contact us.</span>
        </p>
    </div>












</ol>
</div>

</body>
</html>