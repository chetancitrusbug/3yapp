jQuery(document).ready(function ($) {
    statusChange();
});

function statusChange() {
    $('.status').checkboxpicker({
        offLabel: 'Inactive',
        onLabel: 'Active',
    });
    $('.status1').checkboxpicker();

    $('.status_invoice').checkboxpicker({
        offLabel: 'Inprogress',
        onLabel: 'Confirm',
    });
}
$('.status_invoice').checkboxpicker({
    offLabel: 'Inprogress',
    onLabel: 'Confirm',
});


function changeImage(id) {
    $('.changeImage' + id).hide();
}

$.fn.niftyModal('setDefaults', {
    overlaySelector: '.modal-overlay',
    closeSelector: '.modal-close',
    classAddAfterOpen: 'modal-show',
});

var id;
var url;

$(document).on('click', '.del-item', function (e) {
    $("#loading").show();
    id = $(this).data('id');
    url = $(this).data('url');
    msg = $(this).data('msg');
    url = url + "/" + id;

    jQuery('.delete-confirm-text').html("Are you sure you want to Delete " + msg +" ?");
    $('#danger').niftyModal();
});

/* $('#loading').bind('ajaxStart', function () {
    $(this).show();
}).bind('ajaxStop', function () {
    $(this).hide();
}); */

$('.modal-close').on('click',function(){
    $("#loading").hide();
    location.reload();
});

$(".delete-confirm").on('click',function(){
    $("#loading").show();

    $.ajax({
        type: "delete",
        url: url,
        data: { from_index: true },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            $("#loading").hide();
            $('.success-text').html(data.message);
            $('#danger').niftyModal('hide');
            $('#success').niftyModal();
            datatable.draw();
            id = url = null;
            setTimeout(function () {
                $('#success').niftyModal('hide');
            }, 3000);
        },
        error: function (xhr, status, error) {
            jQuery('.delete-confirm-text').html("");
            jQuery('.delete-title').html("Action not proceed!");
            $('#danger').niftyModal();
        }
    });
});

$(document).on('change', '.status-change', function (e) {
    $("#loading").show();
    var id = $(this).data('id');
    var status = $(this).data('status');
    var table = $(this).data('table');
    var url = $(this).data('url');

    url = url+ "/" + table + '/' + status + '/' + id;

    $.ajax({
        url: url,
        success: function (data) {
            $("#loading").hide();
            $('.success-text').html(data.message);
            $('#danger').niftyModal('hide');
            $('#success').niftyModal();
            datatable.draw();

            setTimeout(function () {
                $('#success').niftyModal('hide');
            }, 1000);
        },
        error: function (xhr, status, error) {
            jQuery('.delete-confirm-text').html("");
            jQuery('.delete-title').html("Action not proceed!");
            $('#danger').niftyModal();
        }
    });
});